/* global document, $, alert */
var venus = 9,
    mars = 9,
    jupiter = 9,
    mercury = 9,
    v = 0,
    m = 0,
    j = 0,
    mer = 0,
    i,
    ch1 = document.getElementsByName('class1'),
    ch2 = document.getElementsByName('class2'),
    ch3 = document.getElementsByName('class3'),
    ch4 = document.getElementsByName('class4'),
    val;

$('document').ready(function () {
    'use strict';
    
    $('#classesp1_2').hide();
    $('#classesp1_3').hide();
    $('#classesp2_2').hide();
    $('#classesp2_3').hide();
    $('#classesp2_4').hide();
    $('#classesp3_2').hide();
    $('#classesp3_3').hide();
    $('#classesp4_2').hide();
    $('#classesp4_3').hide();
    $('#classesp4_4').hide();
    
    //look for a change of slot 1's character
    $('#chars select[name="slot1"]').on('change', function () {
        var value = $('option:selected', 'select[name="slot1"]').val();
        if (value === 'isaac') {
            $('.one').css('background-image', 'url("static/isaac.jpg")');
            $('#classesp1_1').show();
            $('#classesp1_2').hide();
            $('#classesp1_3').hide();
        } else if (value === 'felix') {
            $('.one').css('background-image', 'url("static/felix.jpg")');
        } else if (value === 'matthew') {
            $('.one').css('background-image', 'url("static/matthew.jpg")');
            $('#classesp1_1').hide();
            $('#classesp1_2').show();
            $('#classesp1_3').hide();
        } else if (value === 'himi') {
            $('.one').css('background-image', 'url("static/himi.jpg")');
            $('#classesp1_1').hide();
            $('#classesp1_2').hide();
            $('#classesp1_3').show();
        }
        
        /*var value = $('input[name=game]:checked', '#radios').val(); THIS IS FOR REFERENCE -- PLEASE IGNORE
        if (value === 'tbs') {
            $('.one').css('background-image', 'url("static/isaac.jpg")').text('Isaac');
            $('.two').css('background-image', 'url("static/garet.jpg")').text('Garet');
            $('.three').css('background-image', 'url("static/ivan.jpg")').text('Ivan');
            $('.four').css('background-image', 'url("static/mia.jpg")').text('Mia');
        }*/
    });
    
    //look for a change of slot 2's character
    $('#chars select[name="slot2"]').on('change', function () {
        var value = $('option:selected', 'select[name="slot2"]').val();
        if (value === 'garet') {
            $('.two').css('background-image', 'url("static/garet.jpg")');
            $('#classesp2_1').show();
            $('#classesp2_2').hide();
            $('#classesp2_3').hide();
            $('#classesp2_4').hide();
        } else if (value === 'jenna') {
            $('.two').css('background-image', 'url("static/jenna.jpg")');
            $('#classesp2_1').hide();
            $('#classesp2_2').show();
            $('#classesp2_3').hide();
            $('#classesp2_4').hide();
        } else if (value === 'tyrell') {
            $('.two').css('background-image', 'url("static/tyrell.jpg")');
            $('#classesp2_1').hide();
            $('#classesp2_2').hide();
            $('#classesp2_3').show();
            $('#classesp2_4').hide();
        } else if (value === 'eoleo') {
            $('.two').css('background-image', 'url("static/eoleo.jpg")');
            $('#classesp2_1').hide();
            $('#classesp2_2').hide();
            $('#classesp2_3').hide();
            $('#classesp2_4').show();
        }
    });
    
    //look for a change of slot 3's character
    $('#chars select[name="slot3"]').on('change', function () {
        var value = $('option:selected', 'select[name="slot3"]').val();
        if (value === 'ivan') {
            $('.three').css('background-image', 'url("static/ivan.jpg")');
            $('#classesp3_1').show();
            $('#classesp3_2').hide();
            $('#classesp3_3').hide();
        } else if (value === 'sheba') {
            $('.three').css('background-image', 'url("static/sheba.jpg")');
            $('#classesp3_1').show();
            $('#classesp3_2').hide();
            $('#classesp3_3').hide();
        } else if (value === 'karis') {
            $('.three').css('background-image', 'url("static/karis.jpg")');
            $('#classesp3_1').hide();
            $('#classesp3_2').show();
            $('#classesp3_3').hide();
        } else if (value === 'sveta') {
            $('.three').css('background-image', 'url("static/sveta.png")');
            $('#classesp3_1').hide();
            $('#classesp3_2').hide();
            $('#classesp3_3').show();
        }
    });
    
    //look for a change of slot 4's character
    $('#chars select[name="slot4"]').on('change', function () {
        var value = $('option:selected', 'select[name="slot4"]').val();
        if (value === 'mia') {
            $('.four').css('background-image', 'url("static/mia.jpg")');
            $('#classesp4_1').show();
            $('#classesp4_2').hide();
            $('#classesp4_3').hide();
            $('#classesp4_4').hide();
        } else if (value === 'piers') {
            $('.four').css('background-image', 'url("static/piers.jpg")');
            $('#classesp4_1').hide();
            $('#classesp4_2').show();
            $('#classesp4_3').hide();
            $('#classesp4_4').hide();
        } else if (value === 'rief') {
            $('.four').css('background-image', 'url("static/rief.jpg")');
            $('#classesp4_1').hide();
            $('#classesp4_2').hide();
            $('#classesp4_3').show();
            $('#classesp4_4').hide();
        } else if (value === 'amiti') {
            $('.four').css('background-image', 'url("static/amiti.png")');
            $('#classesp4_1').hide();
            $('#classesp4_2').hide();
            $('#classesp4_3').hide();
            $('#classesp4_4').show();
        }
    });
    
    //used to replace words with pictures to clean up html
    $('label:contains("mercury")').html(function (_, html) {
        return html.replace(/mercury/g, "<img src='static/mercury.gif' />");
    });
    $('label:contains("venus")').html(function (_, html) {
        return html.replace(/venus/g, "<img src='static/venus.gif' />");
    });
    $('label:contains("mars")').html(function (_, html) {
        return html.replace(/mars/g, "<img src='static/mars.gif' />");
    });
    $('label:contains("jupiter")').html(function (_, html) {
        return html.replace(/jupiter/g, "<img src='static/jupiter.gif' />");
    });
    
    for (i = ch1.length; i--;) {
        ch1[i].onchange = function() {
            //reset elements in case of switch in same column
            venus += v;
            mars += m;
            jupiter += j;
            mercury += mer;
            
            val = this.value;
            //get value of each element
            v = parseInt(val.slice(0, 1));
            m = parseInt(val.slice(1,2));
            j = parseInt(val.slice(2,3));
            mer = parseInt(val.slice(3,4));
            
            venus -= v;
            mars -= m;
            jupiter -= j;
            mercury -= mer;
            //alert(venus + ''+ mars +''+ jupiter +''+ mercury);
            
            var ven = 0,
                mar = 0,
                jup = 0,
                merc = 0;
            $('[name="class2"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class3"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class4"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
        }
    }
        
     for (i = ch2.length; i--;) {
        ch2[i].onchange = function() {
            //reset elements in case of switch in same column
            venus += v;
            mars += m;
            jupiter += j;
            mercury += mer;
            
            val = this.value;
            //get value of each element
            v = parseInt(val.slice(0, 1));
            m = parseInt(val.slice(1,2));
            j = parseInt(val.slice(2,3));
            mer = parseInt(val.slice(3,4));
            
            venus -= v;
            mars -= m;
            jupiter -= j;
            mercury -= mer;
            //alert(venus + ''+ mars +''+ jupiter +''+ mercury);
            
            var ven = 0,
                mar = 0,
                jup = 0,
                merc = 0;
            $('[name="class1"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class3"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class4"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
        }
    }
    
    for (i = ch3.length; i--;) {
        ch3[i].onchange = function() {
            //reset elements in case of switch in same column
            venus += v;
            mars += m;
            jupiter += j;
            mercury += mer;
            
            val = this.value;
            //get value of each element
            v = parseInt(val.slice(0, 1));
            m = parseInt(val.slice(1,2));
            j = parseInt(val.slice(2,3));
            mer = parseInt(val.slice(3,4));
            
            venus -= v;
            mars -= m;
            jupiter -= j;
            mercury -= mer;
            //alert(venus + ''+ mars +''+ jupiter +''+ mercury);
            
            var ven = 0,
                mar = 0,
                jup = 0,
                merc = 0;
            $('[name="class1"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class2"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class4"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
        }
    }
    
    for (i = ch4.length; i--;) {
        ch4[i].onchange = function() {
            //reset elements in case of switch in same column
            venus += v;
            mars += m;
            jupiter += j;
            mercury += mer;
            
            val = this.value;
            //get value of each element
            v = parseInt(val.slice(0, 1));
            m = parseInt(val.slice(1,2));
            j = parseInt(val.slice(2,3));
            mer = parseInt(val.slice(3,4));
            
            venus -= v;
            mars -= m;
            jupiter -= j;
            mercury -= mer;
            //alert(venus + ''+ mars +''+ jupiter +''+ mercury);
            
            var ven = 0,
                mar = 0,
                jup = 0,
                merc = 0;
            $('[name="class1"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class2"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
            $('[name="class3"]').each(function () {
                ven = parseInt(this.value.slice(0,1));
                mar = parseInt(this.value.slice(1,2));
                jup = parseInt(this.value.slice(2,3));
                merc = parseInt(this.value.slice(3,4));
                if ((ven > venus) || (mar > mars) || (jup > jupiter) || (merc > mercury)) {
                    $(this).prop({disabled: true}).parent().attr('class', 'disabled');
                }
            });
        }
    }
});