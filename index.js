/*global $*/

$('document').ready(function () {
    'use strict';
    
    //When hovering over Chatterbot link
    $('#chatterbot').mouseenter(function () {
        $('#chatterbot span').text('ENTER');
    });
    $('#chatterbot').mouseleave(function () {
        $('#chatterbot span').text('CHATTERBOT');
    });
    
    //When hovering over SPIFF
    $('#spiff').mouseenter(function () {
        $('#spiff span').text('ENTER');
    });
    $('#spiff').mouseleave(function () {
        $('#spiff span').text('S.P.I.F.F.');
    });
    
    //When hovering over elements
    $('#elements').mouseenter(function () {
        $('#elements span').text('ENTER');
    });
    $('#elements').mouseleave(function () {
        $('#elements span').text('ELEMENTS');
    });
    
    //when hovering over GSClasses
    $('#djinni').mouseenter(function () {
        $('#djinni span').text('ENTER');
    });
    $('#djinni').mouseleave(function () {
        $('#djinni span').text('CLASS CALCULATOR');
    });
    
    //upon clicking links
    $('#chatterbot').click(function () {
        $(this).css('animation-name', 'zoom').bind('animationend', function (e) {
            window.location.href = '/TTS/tts.html'
        });
    });
});