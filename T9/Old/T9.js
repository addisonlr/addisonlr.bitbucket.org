/*global $, alert, window, html */
var random = 0;
var correct = 0;
var incorrect = 0;
var round = 1;
var oldPos;
var newPos;
var gameOver = false;
var height;
var newHeight;

function toggleFullScreen() {
    'use strict';
    var doc = window.document,
        docEl = doc.documentElement,
        requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen,
        cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    } else {
        cancelFullScreen.call(doc);
    }
}

//removes effects on incorrectly pressed tiles
var clear = function () {
    'use strict';
    
    //remove shading
    $('#one').css({ boxShadow: 'none' });
    $('#two').css({ boxShadow: 'none' });
    $('#three').css({ boxShadow: 'none' });
    $('#four').css({ boxShadow: 'none' });
    $('#five').css({ boxShadow: 'none' });
    $('#six').css({ boxShadow: 'none' });
    $('#seven').css({ boxShadow: 'none' });
    $('#eight').css({ boxShadow: 'none' });
    $('#nine').css({ boxShadow: 'none' });
};

//randomly assigns a new tile to contain the oh
var shuffle = function () {
    'use strict';
    
    //increase counter of correct guesses
    correct += 1;
    round += 1;
    $('.correct').text(correct);
    
    //generate random number for cell shuffle
    random = Math.floor(Math.random() * 9) + 1;
    
    //set all cells to 0 (zero)
    $('.cell').each(function (i, el) {
        $(el).text('0');
    });
    
    //change one random cell to O (oh)
    switch (random) {
    case 1:
        $('#one').text('O');
        break;
    case 2:
        $('#two').text('O');
        break;
    case 3:
        $('#three').text('O');
        break;
    case 4:
        $('#four').text('O');
        break;
    case 5:
        $('#five').text('O');
        break;
    case 6:
        $('#six').text('O');
        break;
    case 7:
        $('#seven').text('O');
        break;
    case 8:
        $('#eight').text('O');
        break;
    case 9:
        $('#nine').text('O');
        break;
    default:
        break;
    }
};

//called when a wrong tile is pressed
var wrong = function () {
    'use strict';
    
    incorrect += 1;
    round = 1;
    
    //play incorrect boop noise
    $('#wrong').trigger('play');
    
    //adjust display or end game
    switch (incorrect) {
    case 1:
        $('.lives').text('\u2665\u2665\u2661');
        break;
    case 2:
        $('.lives').text('\u2665\u2661\u2661');
        break;
    case 3: //game over
        clear();
        //animate game out of area
        $('.game').animate({'top': '150%', 'opacity': '0'}, 500);
        $('.endGame').show();
        gameOver = true;
        break;
    default:
        break;
    }
};

//assigns correct heights and widths to various elements
var layOut = function () {
    'use strict';
    
    //set ratio of game to be 9:16
    $('.game').width(0.5625 * $('.game').height());
    
    //make 3x3 grid have height = width
    $('.container').height($('.container').width());
    
    //vertically center text in 3x3 grid
    $('.cell').css('line-height', $('.cell').width() + 'px');

    //set game over screen ratio
    $('.endGame').css('line-height', $('.endGame').height() + 'px');
};

//called when red play button is clicked
var playButton = function () {
    'use strict';
    
    //animate transition
    height = $('.about').css('height');
    newHeight = $('.meta').css('height');
    $('.meta').css({'height': height, 'background-color': '#0084B0'});
    $('.meta').animate({'height': newHeight, 'background-color': '#FF2800'}, 250);
    
    //hide other menus
    $('.about').hide();
    
    //show stats menu
    $('.meta').show();
    
    if (gameOver) {
        location.reload(true);
    }
};

//called when blue ? button is clicked
var helpButton = function () {
    'use strict';
    
    //animate transition
    height = $('.meta').css('height');
    newHeight = $('.about').css('height');
    $('.about').css({'height': height, 'background-color': '#FF2800'});
    $('.about').animate({'height': newHeight, 'background-color': '#0084B0'}, 250);
    
    //hide other menus
    $('.meta').hide();
    
    //show help menu
    $('.about').show();
};

//automatically called on page load
$('document').ready(function () {
    'use strict';
    
    //set height to width ratios
    layOut();
    
    //events listeners - makes game work
    $('#one').click(function () {
        clear();
        if ($(this).text() === 'O') {
            shuffle();
        } else {
            $(this).css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#two').click(function () {
        clear();
        if ($(this).text() === 'O') {
            shuffle();
        } else {
            $(this).css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#three').click(function () {
        clear();
        if ($('#three').text() === 'O') {
            shuffle();
        } else {
            $('#three').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#four').click(function () {
        clear();
        if ($('#four').text() === 'O') {
            shuffle();
        } else {
            $('#four').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#five').click(function () {
        clear();
        if ($('#five').text() === 'O') {
            shuffle();
        } else {
            $('#five').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#six').click(function () {
        clear();
        if ($('#six').text() === 'O') {
            shuffle();
        } else {
            $('#six').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#seven').click(function () {
        clear();
        if ($('#seven').text() === 'O') {
            shuffle();
        } else {
            $('#seven').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#eight').click(function () {
        clear();
        if ($('#eight').text() === 'O') {
            shuffle();
        } else {
            $('#eight').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    $('#nine').click(function () {
        clear();
        if ($('#nine').text() === 'O') {
            shuffle();
        } else {
            $('#nine').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
            wrong();
        }
    });
    
    //number pad event listeners
    $(window).on('keydown', function (e) {
        clear(); //remove shading
        
        switch (e.which) {
        case 97:
            if ($('#one').text() === 'O') {
                shuffle();
            } else {
                $('#one').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 98:
            if ($('#two').text() === 'O') {
                shuffle();
            } else {
                $('#two').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 99:
            if ($('#three').text() === 'O') {
                shuffle();
            } else {
                $('#three').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 100:
            if ($('#four').text() === 'O') {
                shuffle();
            } else {
                $('#four').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 101:
            if ($('#five').text() === 'O') {
                shuffle();
            } else {
                $('#five').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 102:
            if ($('#six').text() === 'O') {
                shuffle();
            } else {
                $('#six').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 103:
            if ($('#seven').text() === 'O') {
                shuffle();
            } else {
                $('#seven').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 104:
            if ($('#eight').text() === 'O') {
                shuffle();
            } else {
                $('#eight').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        case 105:
            if ($('#nine').text() === 'O') {
                shuffle();
            } else {
                $('#nine').css({ boxShadow: 'inset 0px 0px 100px 10px rgba(255,0,0,.75)' });
                wrong();
            }
            break;
        default:
            break;
        }
    });
    
    //play button clicked
    $('.play').click(function () {
        
        playButton();
    });
    
    //about button clicked
    $('.help').click(function () {
        helpButton();
    });
    
    $('.endGame').click(function () {
        location.reload(true);
    });
    
    //$('.startOverlay').click(function () {
        //toggleFullScreen();
    $('.startOverlay').hide();
    //});
});

//readjust layout when window is resized
$(window).resize(function () {
    'use strict';
    
    layOut();
});