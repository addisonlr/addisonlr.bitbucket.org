/*global $, alert, Audio*/

var random = 0,
    numCorrect = 0,
    stage = 0,
    lives = 3,
    sound1,
    timing = false,
    minutes,
    seconds,
    startTime,
    currentTime,
    timer,
    tri = true,
    quad = false,
    interval = 2000; //time before next shuffle

function toggleFullScreen() {
    'use strict';
    var doc = window.document,
        docEl = doc.documentElement,
        requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen,
        cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    } else {
        cancelFullScreen.call(doc);
    }
}

var jumble = function () {
    'use strict';
    
    $('.up').css({'line-height': '15vmin'});
    $('.down').css({'line-height': '25vmin'});
    $('.left').css({'text-align': 'left'});
    $('.right').css({'text-align': 'right'});
};

var shift = function () {
    'use strict';
    
    var tile = Math.floor(Math.random() * 16) + 1, //tile to be effected
        effect = Math.floor(Math.random() * 4) + 1, //which of four effects
        effectClass = ''; //class to be applied
    
    switch (effect) {
    case 1:
        effectClass = 'up';
        break;
    case 2:
        effectClass = 'down';
        break;
    case 3:
        effectClass = 'left';
        break;
    case 4:
        effectClass = 'right';
        break;
    default:
        break;
    }
    
    switch (tile) {
    case 1:
        $('#one').addClass(effectClass);
        break;
    case 2:
        $('#two').addClass(effectClass);
        break;
    case 3:
        $('#three').addClass(effectClass);
        break;
    case 4:
        $('#four').addClass(effectClass);
        break;
    case 5:
        $('#five').addClass(effectClass);
        break;
    case 6:
        $('#six').addClass(effectClass);
        break;
    case 7:
        $('#seven').addClass(effectClass);
        break;
    case 8:
        $('#eight').addClass(effectClass);
        break;
    case 9:
        $('#nine').addClass(effectClass);
        break;
    case 10:
        $('#ten').addClass(effectClass);
        break;
    case 11:
        $('#eleven').addClass(effectClass);
        break;
    case 12:
        $('#twelve').addClass(effectClass);
        break;
    case 13:
        $('#thirteen').addClass(effectClass);
        break;
    case 14:
        $('#fourteen').addClass(effectClass);
        break;
    case 15:
        $('#fifteen').addClass(effectClass);
        break;
    case 16:
        $('#sixteen').addClass(effectClass);
        break;
    default:
        break;
    }
    
    jumble();
};

var clickEffect = function (state, xPos, yPos) {
    'use strict';
    
    var dimension = $('.clickCircle').width(),
        background;
    
    if (state === 'right') {
        background = 'limegreen';
    } else {
        background = 'red';
    }
    $('.clickCircle').css({
        'top' : yPos,
        'left' : xPos,
        'width' : '0',
        'height' : '0',
        'background' : background,
        'opacity' : '1',
        'z-index' : '3'
    });
    
    $('.clickCircle').show().animate({
        'width' : '20vmax',
        'height' : '20vmax',
        'opacity' : '0',
        'left' : xPos - (dimension / 2),
        'top' : yPos - (dimension / 2),
        'z-index' : '-1'
    }, 450, "easeOutQuad");
};

var toTri = function () {
    'use strict';
    
    tri = true;
    quad = false;
    
    $('#ten').hide();
    $('#eleven').hide();
    $('#twelve').hide();
    $('#thirteen').hide();
    $('#fourteen').hide();
    $('#fifteen').hide();
    $('#sixteen').hide();
    
    //if landscape
    if (window.innerHeight < window.innerWidth) {
        $('.grid > div').css({
            'width' : '30vh',
            'height' : '',
            'line-height' : '30vh'
        });
    } else {
        $('.grid > div').css({
            'width' : '30vw',
            'height' : '30vw',
            'line-height' : '30vw'
        });
    }
    
    $('.up').css({'line-height': ''});
    $('.down').css({'line-height': ''});
    $('.left').css({'text-align': ''});
    $('.right').css({'text-align': ''});
};

var toQuad = function () {
    'use strict';
    
    tri = false;
    quad = true;
    
    $('#ten').show();
    $('#eleven').show();
    $('#twelve').show();
    $('#thirteen').show();
    $('#fourteen').show();
    $('#fifteen').show();
    $('#sixteen').show();
    
    //if landscape
    if (window.innerHeight < window.innerWidth) {
        $('.grid > div').css({
            'width' : '20vh',
            'height' : '0',
            'line-height' : '24vh'
        });
    } else {
        $('.grid > div').css({
            'width' : '20vw',
            'height' : '20vw',
            'line-height' : '20vw'
        });
    }
};

var startTimer = function () {
    'use strict';
    
    timing = true;
    startTime = Date.now();
    
    timer = setInterval(function () {
        //get current time
        currentTime = Date.now();
        minutes = (currentTime - startTime) / 60000;
        minutes = Math.floor(minutes);
        seconds = (currentTime - startTime) / 1000;
        seconds = Math.floor(seconds) % 60;
        
        $('.timer').html(minutes.toString() + 'm ' + seconds.toString() + 's');
        
        if (timing === false) {
            clearInterval(timer);
        }
    }, 500);
};

var startBGM = function () {
    'use strict';
    
    sound1 = new Audio();
    var source1 = document.createElement('source');
    source1.type = 'audio/wav';
    source1.src = 'audio/TestBGM.wav';
    sound1.appendChild(source1);
    sound1.currentTime = 0;
    sound1.addEventListener('ended', function () {
        this.currentTime = 0;
        this.play();
    }, false);
    sound1.play();
};

var shuffle = function () {
    'use strict';
    
    //generate random number for cell shuffle
    if (tri) {
        random = Math.floor(Math.random() * 9) + 1;
    } else if (quad) {
        random = Math.floor(Math.random() * 16) + 1;
    }
        
    //set all cells to 0 (zero)
    $('.cell').each(function (i, el) {
        $(el).text('0');
    });
    
    //change one random cell to O (oh)
    switch (random) {
    case 1:
        $('#one').text('O');
        break;
    case 2:
        $('#two').text('O');
        break;
    case 3:
        $('#three').text('O');
        break;
    case 4:
        $('#four').text('O');
        break;
    case 5:
        $('#five').text('O');
        break;
    case 6:
        $('#six').text('O');
        break;
    case 7:
        $('#seven').text('O');
        break;
    case 8:
        $('#eight').text('O');
        break;
    case 9:
        $('#nine').text('O');
        break;
    case 10:
        $('#ten').text('O');
        break;
    case 11:
        $('#eleven').text('O');
        break;
    case 12:
        $('#twelve').text('O');
        break;
    case 13:
        $('#thirteen').text('O');
        break;
    case 14:
        $('#fourteen').text('O');
        break;
    case 15:
        $('#fifteen').text('O');
        break;
    case 16:
        $('#sixteen').text('O');
        break;
    default:
        break;
    }
};

var gameOver = function () {
    'use strict';
    sound1.pause();
    
    stage = 0;
    
    //stop timer
    timing = false;
    
    var sound = new Audio(),
        source = document.createElement('source');
    source.type = 'audio/wav';
    source.src = 'audio/gameOver.wav';
    sound.appendChild(source);
    sound.play();
    
    $('.playAgain').show();
    
    //unjumble
    $('.up').css({'line-height': ''});
    $('.down').css({'line-height': ''});
    $('.left').css({'text-align': ''});
    $('.right').css({'text-align': ''});
};

var playAgain = function () {
    'use strict';
    
    numCorrect = 0;
    lives = 3;
    $('.lives').html('Lives: &#9829; &#9829; &#9829;');
    sound1.currentTime = 0;
    sound1.play();
    toTri();
    minutes = 0;
    seconds = 0;
};

var correct = function (xPos, yPos) {
    'use strict';
    
    var sound = new Audio(),
        source = document.createElement('source'),
        switchTimer;
    source.type = 'audio/wav';
    source.src = 'audio/correct.wav';
    sound.appendChild(source);
    sound.play();
    
    //increment relevant statistics
    numCorrect += 1;
    stage += 1;
    
    if (stage === 15) {
        toQuad();
    }
    
    //jumble tiles
    if (stage === 30) {
        jumble();
    }
    
    if (stage >= 45) {
        shift();
    }
    
    if (stage >= 60) {
        switchTimer = setInterval(function () {
            shuffle();
        
            if (stage < 60) {
                clearInterval(switchTimer);
            }
        }, 1000);
    }
    
    //begin animation
    clickEffect('right', xPos, yPos);
    
    //push new information to user
    $('.stats').text('Correct: ' + numCorrect);
    shuffle();
};

var wrong = function (xPos, yPos) {
    'use strict';
    
    var sound = new Audio(),
        source = document.createElement('source');
    source.type = 'audio/wav';
    source.src = 'audio/wrong.wav';
    sound.appendChild(source);
    sound.play();
    
    //adjust relevant statistics
    //numCorrect = 0;
    if (stage > 20) {
        stage -= 20;
    } else {
        stage = 0;
    }
    
    $('.stats').html('Correct: ' + numCorrect);
    lives -= 1;
    
    if (stage >= 45) {
        shift();
    } else if (stage >= 30) {
        jumble();
    } else if (stage >= 15) {
        toQuad();
    } else {
        toTri();
    }
    shuffle();
    
    switch (lives) {
    case 2:
        $('.lives').html('Lives: &#9829; &#9829; &#9825;');
        break;
    case 1:
        $('.lives').html('Lives: &#9829; &#9825; &#9825;');
        break;
    case 0:
        $('.lives').html('Lives: &#9825; &#9825; &#9825;');
        gameOver();
        break;
    default:
        break;
    }
    
    //begin animation
    clickEffect('wrong', xPos, yPos);
};

//automatically called on page load
$('document').ready(function () {
    'use strict';
    
    $('.playAgain').hide();
    $('.clickCircle').hide();
    
    toTri();
    
    //listen for events
    $(window).resize(function () {
        if (window.innerHeight > window.innerWidth) {
            if (tri) {
                toTri();
            } else if (quad) {
                toQuad();
            }
        } else {
            if (tri) {
                toTri();
            } else if (quad) {
                toQuad();
            }
        }
    });
    
    $('.restart').click(function () {
        timing = false;
        playAgain();
        shuffle();
        startTimer();
    });
    
    $('.play').click(function () {
        $(this).hide();
        toggleFullScreen();
        startBGM();
        startTimer();
    });
    
    $('.playAgain').click(function () {
        $(this).hide();
        playAgain();
        startTimer();
    });
    
    $('#one').click(function (event) {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#two').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#three').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#four').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#five').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#six').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#seven').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#eight').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#nine').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#ten').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#eleven').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#twelve').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#thirteen').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#fourteen').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#fifteen').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
    
    $('#sixteen').click(function () {
        if ($(this).text() === 'O') {
            correct(event.pageX, event.pageY);
        } else {
            wrong(event.pageX, event.pageY);
        }
    });
});