/*global $, alert, Audio, console*/
/*jslint regexp: true */

var phonetic = [];
var skip = -2; //use to know when to make a letter silent
var wait = 0; //indicates number of iterations to skip
var vowels = ['a', 'e', 'i', 'o', 'u'];
var alveolars = ['n', 't', 'd', 's', 'z', 'l'];
var stops = ['p', 'b', 't', 'd', 'k', 'g'];
var syllabics = ['k'];
var constants = ['b', 'd', 'f', 'l', 'n', 'p', 'r', 't', 'v', 'z'];
var oolets = ['m', 'p', 'b', 'f', 'v', 'ch', 'dge', 'sh', 'ge'];
var consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't'];
var misc = ['s', 'z', 'r', 'd', 'c', 'j'];
var index = 0;

//parseNums variable
var suffix = ['', ' thousand ', ' milleein ', ' billeein ', ' trilleein ', ' qwadrilleein ', ' qwintilleein ', ' sextilleein ', ' septilleein ', ' octilleein ', ' nahnilleein ', ' decilleein '];

var parseNums = function (segment) {
    'use strict';
    
    //create variables needed for following for loop
    var splitSegment = segment.split(" "),
        i = 0,
        j = 0,
        temp = '',
        length = 0,
        word = '';
    
    for (i = 0; i < splitSegment.length; i += 1) {
        //check for numbers in segment
        if (/\D*(\d+)\D*/.test(splitSegment[i])) {
            
            //removes comma
            segment = segment.replace(temp, temp.replace(',', ''));
            
            //gets rid of all non-digit characters in word
            temp = splitSegment[i].replace(/\D*(\d+)\D*/, '$1');
            length = temp.length;
            
            //if number is zero, then function can end early
            if (temp === '0') {
                word = 'zeero';
                return;
            }
            
            //loop through each character of the current word
            for (j = 0; j < length; j += 1) {
                
                if (((length - j) % 3) === 0) {
                    switch (temp.charAt(j)) {
                    case '1':
                        word += ' wun hundrid ';
                        break;
                    case '2':
                        word += ' two hundrid ';
                        break;
                    case '3':
                        word += ' three hundrid ';
                        break;
                    case '4':
                        word += ' for hundrid ';
                        break;
                    case '5':
                        word += ' feyev hundrid ';
                        break;
                    case '6':
                        word += ' six hundrid ';
                        break;
                    case '7':
                        word += ' seven hundrid ';
                        break;
                    case '8':
                        word += ' eight hundrid ';
                        break;
                    case '9':
                        word += ' nine hundrid ';
                        break;
                    case '0':
                        break;
                    default:
                        break;
                    }
                } else if (((length - j) % 3) === 2) {
                    if (temp.charAt(j) !== '1') {
                        switch (temp.charAt(j)) {
                        case '2':
                            word += ' twenty ';
                            break;
                        case '3':
                            word += ' thirty ';
                            break;
                        case '4':
                            word += ' forty ';
                            break;
                        case '5':
                            word += ' fifty ';
                            break;
                        case '6':
                            word += ' sixty ';
                            break;
                        case '7':
                            word += ' seventy ';
                            break;
                        case '8':
                            word += ' eighty ';
                            break;
                        case '9':
                            word += ' neyenty ';
                            break;
                        default:
                            break;
                        }
                    }
                } else if (((length - j) % 3) === 1) {
                    if ((temp.charAt(j - 1) === '1') && (temp.charAt(j) !== '0')) {
                        switch (temp.charAt(j)) {
                        case '1':
                            word += ' eleven ';
                            break;
                        case '2':
                            word += ' twelve ';
                            break;
                        case '3':
                            word += ' thirteen ';
                            break;
                        case '4':
                            word += ' forteen ';
                            break;
                        case '5':
                            word += ' fifteen ';
                            break;
                        case '6':
                            word += ' sixteen ';
                            break;
                        case '7':
                            word += ' seventeen ';
                            break;
                        case '8':
                            word += ' eighteen ';
                            break;
                        case '9':
                            word += ' nineteen ';
                            break;
                        default:
                            break;
                        }
                    } else {
                        switch (temp.charAt(j)) {
                        case '1':
                            word += 'one';
                            break;
                        case '2':
                            word += 'two';
                            break;
                        case '3':
                            word += 'three';
                            break;
                        case '4':
                            word += 'for';
                            break;
                        case '5':
                            word += 'feyeve';
                            break;
                        case '6':
                            word += 'six';
                            break;
                        case '7':
                            word += 'seven';
                            break;
                        case '8':
                            word += 'eight';
                            break;
                        case '9':
                            word += 'nine';
                            break;
                        default:
                            break;
                        }
                    }
                    
                    //adds appropriate suffix (e.g., thousand, million, etc.)
                    if (temp.substr(j - 2, 3) !== '000') { //do nothing for 000's
                        word += suffix[(length - j - 1) / 3];
                    }
                }
            }
            segment = segment.replace(temp, word);
        }
    }
    return segment;
};

var specialWord = function (word) {
    'use strict';
    
    var specials = ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'people'],
        ees = ['b', 'd', 'p', 't', 'v', 'z'],
        ehs = ['f', 'l', 'm', 'n', 's'];
    
    //checks if current word is a special word
    //if not, function ends
    if ($.inArray(word, ees) > -1) {
        phonetic.push(String(word));
        phonetic.push('ee');
        
        return true;
    } else if ($.inArray(word, ehs) > -1) {
        phonetic.push('e');
        phonetic.push(String(word));
        
        return true;
    } else if ($.inArray(word, specials) > -1) {
        switch (word) {
        case 'c':
            phonetic.push('s');
            phonetic.push('ee');
            break;
        case 'e':
            phonetic.push('ee');
            break;
        case 'g':
            phonetic.push('dge');
            phonetic.push('ee');
            break;
        case 'h':
            phonetic.push('eigh');
            phonetic.push('t');
            phonetic.push('ch');
            break;
        case 'j':
            phonetic.push('dge');
            phonetic.push('eigh');
            break;
        case 'k':
            phonetic.push('k');
            phonetic.push('eigh');
            break;
        case 'o':
            phonetic.push('oe');
            break;
        case 'q':
            phonetic.push('k');
            phonetic.push('ue');
            break;
        case 'r':
            phonetic.push('au');
            phonetic.push('r');
            break;
        case 'u':
            phonetic.push('ju');
            break;
        case 'w':
            phonetic.push('d');
            phonetic.push('u');
            phonetic.push('b');
            phonetic.push('l');
            phonetic.push('ju');
            break;
        case 'x':
            phonetic.push('e');
            phonetic.push('k');
            phonetic.push('s');
            break;
        case 'y':
            phonetic.push('w');
            phonetic.push('eye');
            break;
        case 'people':
            phonetic.push('p');
            phonetic.push('ee');
            phonetic.push('p');
            phonetic.push('l');
            phonetic.push('silent');
            break;
        default:
            break;
        }
        
        return true;
    }
};

var parse = function (index, value, word) {
    'use strict';
    
    var four = word.substring(index, index + 4), //current letter and three following
        two = '', //current letter and following
        i = 0, //for loop index variable
        passed = false, //used to see if condition is met
        cut = ''; //used to remove current letter and exaluate rest of word
    
    if (index >= 2) {
        two = word.substring(index - 2, index);
    }
    
    
    //checks all four letter phonemes
    switch (four) {
    case 'eigh':
        phonetic.push('eigh');
        return 3;
    case 'ient':
        phonetic.push('ee');
        phonetic.push('i');
        phonetic.push('n');
        phonetic.push('t');
        return 3;
    case 'sono':
        phonetic.push('s');
        phonetic.push('oe');
        phonetic.push('n');
        phonetic.push('oe');
        return 3;
    default:
        break;
    }
    
    //check all three letter phonemes
    switch (four.substr(0, 3)) {
    case 'air':
        phonetic.push('air');
        return 2;
    case 'ere':
        if (word.indexOf('h') > -1) { //if word contains an h
            phonetic.push('ear'); //example word: here
            return 2;
        } else if ((index + 2) === (word.length - 1)) {
            phonetic.push('ur');
            phonetic.push('silent');
            return 2;
        } else {
            phonetic.push('air');
            return 2;
        }
    case 'are':
        if (index !== 0) {
            phonetic.push('air');
            return 2;
        } else if (word === 'are') {
            phonetic.push('au');
            phonetic.push('r');
            return 2;
        }
        break;
    case 'ary':
        phonetic.push('air');
        phonetic.push('ee');
        return 2;
    case 'and':
        phonetic.push('a3');
        phonetic.push('n');
        phonetic.push('d');
        return 2;
    case 'ear':
        for (i = 0; i < alveolars.length; i += 1) {
            if (word.indexOf(alveolars[i]) > -1) {
                phonetic.push('ear');
                return 2;
            }
        }
        phonetic.push('ur');
        return 2;
    case 'eye':
        phonetic.push('eye');
        return 2;
    case 'ing':
        if (index !== 0) { //if not beginning of word
            phonetic.push('ee');
            phonetic.push('ng');
            return 2;
        }
        //ing at beginning of word is always (in g~) not (ing~)
        break;
    case 'qui':
        phonetic.push('k');
        phonetic.push('w');
        if (word.charAt(index + 4) === 'e') {
            if (word.charAt(index + 3) === 'v') {
                phonetic.push('i');
            }
            skip = 199;
        } else {
            phonetic.push('eye');
        }
        return 2;
    case 'ure':
        phonetic.push('ure');
        return 2;
    case 'our':
        if (word !== 'our') {
            phonetic.push('ure');
            return 2;
        }
        break;
    case 'dge':
        phonetic.push('dge');
        return 2;
    case '...':
        phonetic.push('elipsis');
        return 2;
    default:
        break;
    }
    
    //check all two letter phonemes
    switch (four.substr(0, 2)) {
    case 'ai':
    case 'ay':
        phonetic.push('eigh');
        return 1;
    case 'au':
        phonetic.push('au');
        return 1;
    case 'at':
        if (word.charAt(index + 2) === 'e') {
            phonetic.push('eigh');
            skip = 199;
            return 0;
        }
        phonetic.push('a3');
        return 0;
    case 'an':
        phonetic.push('a3');
        phonetic.push('n');
        return 1;
    case 'cc':
        if (/.*(ee|ss).*/.test(word)) {
            phonetic.push('k');
            phonetic.push('s');
            return 1;
        }
        phonetic.push('k');
        return 1;
    case 'ee':
        phonetic.push('ee');
        return 1;
    case 'ea':
        if (word.substr(index, 5) === 'ealth') { //if current five letters is ealth
            phonetic.push('e');
        } else if (((index + 1) === (word.length - 1)) && (word.indexOf('r') > -1)) { //if next letter is second to last and word contains r
            phonetic.push('ee');
            phonetic.push('a1');
        } else if ((four === 'eate') && (word.charAt(index - 1) !== 'b')) { //activates when word is create not beaten (ee ay) vs. (ee)
            phonetic.push('ee');
            phonetic.push('eigh');
        } else {
            phonetic.push('ee');
        }
        return 1;
    case 'ed':
        if (index === word.length - 2) { //if 3rd to last letter
            phonetic.push('silent');
            phonetic.push('d');
            return 1;
        }
        break;
    case 'ew':
        phonetic.push('ue');
        return 1;
    case 'ff':
        phonetic.push('f');
        return 1;
    case 'ie':
        if (four.startsWith('ies') || four.startsWith('iev')) {
            phonetic.push('ee');
            phonetic.push(four.charAt(2));
            return 2;
        }
        phonetic.push('eye');
        return 1;
    case 'oa':
    case 'oe':
        phonetic.push('oe');
        return 1;
    case 'ow':
        phonetic.push('e');
        if (four.charAt(3) === 'e') {
            skip = 200;
        }
        return 1;
    case 'so':
        //example of failure: soap
        if (four.charAt(2) !== 'a') {
            //example of pass: son
            if (four.charAt(2) === 'n') {
                phonetic.push('s');
                phonetic.push('u');
                return 1;
            } else { //example: so
                phonetic.push('s');
                phonetic.push('oe');
                return 1;
            }
        }
        break;
    /*case 'in':
        phonetic.push('i');
        phonetic.push('n');
        return 1;*/
    case 'ur':
        phonetic.push('ur');
        return 1;
    case 'er':
        if (four.charAt(3) === 'i') {
            phonetic.push('e');
            phonetic.push('r');
            phonetic.push('i');
            return 2;
        }
        phonetic.push('ur');
        return 1;
    case 'ir':
        if (index === 0) {
            phonetic.push('eye');
            phonetic.push('r');
            if (four.charAt(3) === 'e') {
                skip = 200;
            }
        } else if (four.charAt(3) === 'e') {
            phonetic.push('eye');
            phonetic.push('r');
            skip = 200;
        } else {
            phonetic.push('ur');
        }
        return 1;
    case 'oo':
        for (i = 0; i < oolets.length; i += 1) {
            if (word.indexOf(oolets[i]) > -1) {
                phonetic.push('ue');
                return 1;
            }
        }
        phonetic.push('oo');
        return 1;
    case 'or':
        phonetic.push('or');
        if (four.charAt(3) === 'e') {
            skip = 200;
        }
        return 1;
    case 'ou':
        if ((index === 0) || (word.substring(0, index).indexOf('r') > -1)) {
            phonetic.push('ou1');
            return 1;
        }
        for (i = 0; i < stops.length; i += 1) {
            if (word.substring(0, index).indexOf(stops[i]) > -1) {
                phonetic.push('ou1');
                return 1;
            }
        }
        if (four.substr(2, 2) === 'gh') {
            phonetic.push('ue');
            return 1;
        } else {
            phonetic.push('oo');
            return 1;
        }
    case 'oy':
        phonetic.push('oy');
        return 1;
    case 'ue':
        phonetic.push('ue');
        return 1;
    case 'gh':
        if ((two === 'ou') || (two === 'ei')) {
            phonetic.push('silent');
            return 1;
        } else if (two === 'au') {
            phonetic.push('f');
            return 1;
        } else {
            phonetic.push('g');
            return 1;
        }
    case 'gy':
        if (!(word.endsWith('gy'))) {
            phonetic.push('dge');
            return 1;
        }
        break;
    case 'ck':
        //phonetic.push('silent');
        phonetic.push('k');
        return 1;
    case 'el':
        if ((i + 1) === (word.length - 1)) {
            phonetic.push('l');
            return 1;
        }
        break;
    case 'le':
        if ((i !== 0) && (four.charAt(3) === 'd')) {
            phonetic.push('l');
            return 1;
        }
        break;
    case 'll':
        phonetic.push('l');
        return 1;
    case 'ng':
        phonetic.push('ng');
        return 1;
    case 'ss':
        phonetic.push('s');
        return 1;
    case 'sh':
        phonetic.push('sh');
        return 1;
    case 'ti':
        if (four === 'tion') {
            phonetic.push('sh');
            phonetic.push('i');
            phonetic.push('n');
            return 3;
        }
        break;
    case 'tt':
        phonetic.push('t');
        return 1;
    case 'pt':
        if (i === 0) {
            phonetic.push('t');
            return 1;
        }
        break;
    case 'ch':
        phonetic.push('ch');
        return 1;
    case 'th':
        phonetic.push('th');
        return 1;
    case 've':
        if (word.endsWith('ve')) {
            phonetic.push('v');
            return 1;
        } else {
            phonetic.push('v');
            return 0;
        }
    case 'ge':
        if (word.endsWith('ge')) {
            phonetic.push('ge');
            return 1;
        }
        break;
    case 'wh':
        if (four.charAt(3) === 'o') {
            phonetic.push('h');
        } else {
            phonetic.push('w');
        }
        return 1;
    case 'zz':
        phonetic.push('t');
        phonetic.push('z');
        return 1;
    default:
        break;
    }
    
    //check all one letter phonemes
    switch (value) {
    case 'a':
        for (i = 0; i < stops.length; i += 1) {
            if (word.indexOf(stops[i]) > -1) {
                phonetic.push('a3');
                return 0;
            }
        }
        if ((four.charAt(2) === 'e') || (word === 'a') || ((four.charAt(2) === 'o') && (index !== 0))) {
            phonetic.push('eigh');
            return 0;
        } else if (i === (word.length - 1)) {
            phonetic.push('a1');
            return 0;
        } else if (word.indexOf('r') > -1) {
            phonetic.push('a2');
            return 0;
        } else if (index === 0) {
            if (four.charAt(2) === 'n') {
                phonetic.push('a3');
                return 0;
            } else {
                phonetic.push('a1');
                return 0;
            }
        } else {
            phonetic.push('au');
            return 0;
        }
    case 'c':
        if (four.substring(1, 2) === 'e') {
            phonetic.push('s');
            return 0;
        } else {
            phonetic.push('k');
            return 0;
        }
    case 'e':
        cut = word.substring(0, index) + word.substring(index + 1, word.length);
        if (word === 'the') {
            phonetic.push('u');
            return 0;
        }
        for (i = 0; i < vowels.length; i += 1) {
            if (cut.indexOf(vowels[i]) > -1) {
                passed = true;
                break;
            }
        }
        if ((index === (word.length - 1)) && (passed === false)) {
            phonetic.push('ee');
            return 0;
        } else if (four.charAt(2) === 'v') {
            for (i = 0; i < misc.length; i += 1) {
                if (four.charAt(1).indexOf(misc[i]) > -1) {
                    phonetic.push('ee');
                    return 0;
                }
            }
        } else if (index === (word.length - 1)) {
            phonetic.push('silent');
            return 0;
        } else {
            phonetic.push('e');
            return 0;
        }
        break;
    case 'g':
        phonetic.push('g');
        return 0;
    case 'h':
        for (i = 0; i < consonants.length; i += 1) {
            if (two.charAt(1) === consonants[i]) {
                phonetic.push('h');
                return 0;
            }
        }
        if (index === 0) {
            phonetic.push('h');
            return 0;
        } else {
            phonetic.push('silent');
            return 0;
        }
    case 'i':
        cut = word.substring(0, index) + word.substring(index + 3, word.length);
        if (four.charAt(2) === 'i') {
            phonetic.push('i');
            return 0;
        } else if (four.substr(0, 3) === 'igh') {
            phonetic.push('eye');
            return 2;
        } else if ((word === 'i') || (index === (word.length - 1))) {
            phonetic.push('eye');
            skip = 199;
            return 0;
        } else if (four.charAt(2) === 'e') {
            passed = true;
            for (i = 0; i < alveolars.length; i += 1) {
                if (cut.indexOf(alveolars[i]) === -1) {
                    passed = false;
                    break;
                }
            }
            if (passed) {
                phonetic.push('eye');
                skip = 199;
                return 0;
            }
        } else {
            phonetic.push('i');
            return 0;
        }
        break;
    case 'j':
        phonetic.push('dge');
        return 0;
    case 'm':
        if (index === (word.length - 1)) {
            phonetic.push('m_end');
        } else {
            phonetic.push('m');
        }
        return 0;
    case 'o':
        cut = word.substring(0, index) + word.substring(index + 1, word.length);
        passed = true;
        if (word.indexOf('n') > -1) {
            for (i = 0; i < vowels.length; i += 1) {
                if (cut.indexOf(vowels[i]) > -1) {
                    passed = false;
                    break;
                }
            }
            if (passed) {
                phonetic.push('oe');
                return 0;
            }
        }
        if (index === (word.length - 1)) {
            for (i = 0; i < vowels.length; i += 1) {
                if (cut.indexOf(vowels[i]) > -1) {
                    passed = false;
                    break;
                }
            }
            if (passed) {
                phonetic.push('ue');
                return 0;
            }
        }
        if ((four.charAt(2) === 'a') || (four.charAt(2) === 'i') || (index === (word.length - 1))) {
            phonetic.push('oe');
            return 0;
        } else if (four.charAt(2) === 'e') {
            if (four.charAt(1) !== 'v') {
                phonetic.push('au');
            } else {
                phonetic.push('au');
            }
            skip = 199;
            return 0;
        } else {
            phonetic.push('au');
            return 0;
        }
    case 'q':
        phonetic.push('k');
        return 0;
    case 's':
        if ((vowels.indexOf(two.charAt(1)) > -1) && (vowels.indexOf(four.charAt(1)) > -1)) {
            if ((index + 1) === (word.length - 1)) {
                phonetic.push('z');
                return 0;
            } else {
                phonetic.push('s');
                return 0;
            }
        } else {
            phonetic.push('s');
            return 0;
        }
    case 'u':
        if (index === 0) {
            if (vowels.indexOf(four.charAt(2)) > -1) {
                if (four.charAt(2) === 'e') {
                    skip = 199;
                }
                phonetic.push('ju');
                return 0;
            } else {
                phonetic.push('u');
                return 0;
            }
        } else if (four.charAt(2) === 'e') {
            phonetic.push('ue');
            return 0;
        } else if ((stops.indexOf(two.charAt(1)) > -1) && (stops.indexOf(four.charAt(1)) > -1)) {
            phonetic.push('ju');
            return 0;
        } else {
            phonetic.push('u');
            return 0;
        }
    case 'w':
        if (four.charAt(1) === 'o') {
            phonetic.push('silent');
            return 0;
        }
        phonetic.push('w');
        return 0;
    case 'x':
        phonetic.push('k');
        phonetic.push('s');
        return 0;
    case 'y':
        if ((index === 0) || (two.charAt(1) === 'e')) {
            phonetic.push('y');
            return 0;
        } else if (index === word.length - 1) {
            for (i = 0; i < vowels.length; i += 1) {
                if (word.indexOf(vowels[i]) > -1) {
                    phonetic.push('ee');
                    return 0;
                }
            }
            phonetic.push('eye');
            return 0;
        } else {
            phonetic.push('i');
            return 0;
        }
    case ',':
    case '\'':
        phonetic.push('comma');
        return 0;
    case ';':
        phonetic.push('scolon');
        return 0;
    case ':':
        phonetic.push('colon');
        return 0;
    case '-':
        phonetic.push('dash');
        return 0;
    case '.':
    case '?':
    case '!':
        phonetic.push('fullstop');
        return 0;
    default:
        for (i = 0; i < syllabics.length; i += 1) {
            if (value.indexOf(syllabics[i]) > -1) {
                phonetic.push(value);
                return 0;
            }
        }
        for (i = 0; i < constants.length; i += 1) {
            if (value.indexOf(constants[i]) > -1) {
                phonetic.push(value);
                return 0;
            }
        }
        phonetic.push('unknown');
        alert('You found an text combo the program doesn\'t understand. The developer is notified, as this will only appear during development, and only the developer has access to the program during that time. As such, this message is almost entirely pointless as the following would suffice, "Unknown Input".');
        return 0;
    }
};

var smooth = function () {
    'use strict';
    var index = 0, //current position in phonetic array
        value = ''; //vallue of phonetic[index]
    
    for (index = 0; index < phonetic.length; index += 1) {
        value = phonetic[index];
        
        if (index !== (phonetic.length - 1)) {
            switch (value) {
            case 'b':
                switch (phonetic[index + 1]) {
                case 'a2':
                case 'au':
                    phonetic[index] = 'ba2';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'e':
                    phonetic[index] = 'be';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'ee':
                    phonetic[index] = 'bee';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'oe':
                    phonetic[index] = 'boe';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'u':
                    phonetic[index] = 'bu';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'ur':
                    phonetic[index] = 'bur';
                    phonetic.splice(index + 1, 1);
                    break;
                default:
                    break;
                }
                break;
            case 'd':
                if ((phonetic[index + 1] === 'a2') || (phonetic[index + 1] === 'au')) {
                    phonetic[index] = 'da2';
                    phonetic.splice(index + 1, 1);
                    break;
                }
                break;
            case 'i':
                switch (phonetic[index + 1]) {
                case 'n':
                    phonetic[index] = 'in';
                    phonetic.splice(index + 1, 1);
                    break;
                case 's':
                    phonetic[index] = 'is';
                    phonetic.splice(index + 1, 1);
                    break;
                default:
                    break;
                }
                break;
            case 'k':
                switch (phonetic[index + 1]) {
                case 'a2':
                case 'au':
                    phonetic[index] = 'ka2';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'eigh':
                    phonetic[index] = 'keigh';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'l':
                    phonetic[index] = 'kl';
                    phonetic.splice(index + 1, 1);
                    break;
                case 's':
                    phonetic[index] = 'ks';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'u':
                    phonetic[index] = 'ku';
                    phonetic.splice(index + 1, 1);
                    break;
                default:
                    break;
                }
                break;
            case 'l':
                switch (phonetic[index + 1]) {
                case 'a1':
                    phonetic[index] = 'la1';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'a2':
                    phonetic[index] = 'la2';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'a3':
                    phonetic[index] = 'la3';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'eigh':
                    phonetic[index] = 'leigh';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'e':
                    phonetic[index] = 'le';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'ee':
                    phonetic[index] = 'lee';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'air':
                    phonetic[index] = 'lair';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'eye':
                    phonetic[index] = 'leye';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'au':
                    phonetic[index] = 'lau';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'oe':
                    phonetic[index] = 'loe';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'u':
                    phonetic[index] = 'lu';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'ur':
                    phonetic[index] = 'lur';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'ue':
                    phonetic[index] = 'lue';
                    phonetic.splice(index + 1, 1);
                    break;
                default:
                    break;
                }
                break;
            case 'm':
                switch (phonetic[index + 1]) {
                case 'eigh':
                    phonetic[index] = 'meigh';
                    phonetic.splice(index + 1, 1);
                    break;
                default:
                    break;
                }
                break;
            case 'n':
                switch (phonetic[index + 1]) {
                case 'd':
                    phonetic[index] = 'nd';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'g':
                    phonetic[index] = 'ng';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'o':
                    phonetic[index] = 'no';
                    phonetic.splice(index + 1, 1);
                    break;
                case 'or':
                    phonetic[index] = 'or';
                    phonetic.splice(index + 1, 1);
                    break;
                }
                break;
            case 'p':
                if (phonetic[index + 1] === 'u') {
                    phonetic[index] = 'pu';
                    phonetic.splice(index + 1, 1);
                }
                break;
            default:
                break;
            }
        }
    }
};

var chain = function () {
    'use strict';
    var i = 0,
        url = 'phonemes/';
    
    $('#audioBox').empty();
    
    //loop through phoneme list and create needed audio elements
    for (i; i < phonetic.length; i += 1) {
        $("#audioBox").append('<audio id="' + phonetic[i] + i + '" src="' + url + phonetic[i] + '.wav" preload="none" controls=""></audio>');
    }
    
    for (i = 0; i < phonetic.length; i += 1) {
        document.getElementById(phonetic[i] + i).addEventListener('ended', function () { index += 1; document.getElementById(phonetic[index] + index).play(); });
    }
    index = 0;
    document.getElementById(phonetic[0] + index).play();
};

var tokenize = function (passage) {
    'use strict';
    
    phonetic = [];
    
    //adds a space before specified punctuation
    passage = passage.replace(/,|;|:|-|\.|\?|\!|\(|\)/g, ' $&');
    passage = passage.replace('  ', ' '); //replace double with single space
    passage = passage.toLowerCase();
    
    //the following replace double consonants with singe so as to prevent stuttering
    passage = passage.replace('bb', 'b');
    passage = passage.replace('dd', 'd');
    passage = passage.replace('ph', 'f');
    passage = passage.replace('kn', 'n');
    passage = passage.replace('mm', 'm');
    passage = passage.replace('nn', 'n');
    passage = passage.replace('pp', 'p');
    passage = passage.replace('rr', 'r');
    
    //calls a function to replace numbers with words
    passage = parseNums(passage);
    
    wait = 0;
    //splits input into words
    var splitPassage = passage.split(' '),
        i = 0,
        length = 0,
        j = 0;
    
    //loops through words in input
    for (i = 0; i < splitPassage.length; i += 1) {
        
        //check if the current word is special (i.e., has a unique pronunciation)
        if (!specialWord(splitPassage[i])) {
            
            length = splitPassage[i].length; //length of current word
                
            //loops through letters in current word
            for (j = 0; j < length; j += 1) {
                
                if (wait !== 0) {
                    wait -= 1;
                } else if (skip === 200) {
                    phonetic.push('silent');
                    skip += 1;
                } else {
                    skip += 1;
                    wait = parse(j, splitPassage[i].charAt(j), splitPassage[i]);
                }
            }
        }
        
        //adds a pause between words
        phonetic.push('space');
    }
    
    console.log(phonetic);
    
    //merges phonemes into diphones when recording is available for smooth pronunciation
    smooth();
    
    //strings the audio files together, and plays it
    chain();
};