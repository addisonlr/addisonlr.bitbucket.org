/*global $, alert, tokenize, cats*/
/*jslint regexp: true*/
var input = '';

//transinput variables which will effect course of conversation
var apologize = false; //enables when ai is insulted. off when user apologizes.
var greeted = false; //true after ai is greeted
var ors = false; //activated when or question is asked
var unclearAsked = false;
var started = false; //true after first user input
var catTopic = false;
var audioOn = true;

//scrolls history to bottom
var scrollDown = function () {
    'use strict';
    
    $('.history').animate({scrollTop: $('.history')[0].scrollHeight}, 500);
};

//evaluates text to see if it is nonsence
var checkValidity = function (input) {
    'use strict';
    
    //checks for user remorse after triggering conditions below
    if (input.match(/(sorry|apologize)/i)) {
        if (apologize) {
            apologize = false;
            return 'Thank you. That means a lot to me.';
        } else { //If user randomly apologizes for nothing
            return 'Why are you apologizing? You didn\'t do anything';
        }
    }
    
    //checks for 5 or more identical, characters
    if (input.match(/(.)\1{3,}/)) {
        //if a nonsense sentence
        if (input.indexOf(' ') > -1) {
            apologize = true;
            return 'How do you expect me to respond to that? That\'s not even a real sentence!';
        } else {
            apologize = true;
            return 'How do you expect me to respond to that? That\'s not even a word!';
        }
    }
    
    //checks for home-row spam, which is revealed by excesive semi colons
    if (input.match(/(;(.+)){3,}/)) {
        apologize = true;
        return 'Please don\'t do that. How would you like it if I started yelling at you when you don\'t respond how I expect?';
    }
    return 'Is that so?';
};

var greetingCheck = function (input) {
    'use strict';
    var parrotGreet = ['hi', 'howdy', 'hey', 'good morning', 'salutations', 'morning', 'good afternoon', 'good evening'], //response is input
        formulaGreet = ['hello', 'how\'re you', 'how are you', 'what\'s up', 'whats up'], //response requires additions
        i = 0; //used for for loops
    
    //check for casual greeting
    for (i = 0; i < parrotGreet.length; i += 1) {
        if (input.startsWith(parrotGreet[i])) {
            greeted = true;
            return parrotGreet[i] + '.';
        }
    }
    
    //check for regular greeting
    for (i = 0; i < formulaGreet.length; i += 1) {
        if (input.indexOf(formulaGreet[i]) > -1) {
            if (i > 2) { //the whats up variations
                greeted = true;
                return 'not much, you?';
            } else if (i > 0) { //the how are you variations
                if (greeted) {
                    return 'i\'m good, thanks.';
                }
                greeted = true;
                return 'i\'m good, thank you. How about you?';
            } else {
                greeted = true;
                return 'hello, how are you?';
            }
        }
    }
    
    //returns generic when no above conditions are met
    return 'Is that so?';
};

var evaluateQuestion = function (input) {
    'use strict';
    var yesNos = ['are', 'am', 'was', 'were', 'will', 'do', 'does', 'did', 'have', 'had', 'has', 'is', 'can', 'could', 'should', 'shall', 'may', 'might', 'would'],
        openEnded = ['who', 'whom', 'whose', 'what', 'where', 'when', 'why', 'how', 'which'],
        i = 0; //used for for loops
    
    if (input.indexOf(' cat') > -1 && catTopic === false) {
        catTopic = true;
        return "Let'ts talk about cats.";
    }
    
    if (input.indexOf(' talking') > -1 || input.indexOf(' talk') > -1) {
        if (input.indexOf(' stop') > -1 || input.indexOf(' don\'t') > -1 || input.indexOf(' dont') > -1) {
            audioOn = false;
        } else {
            audioOn = true;
        }
        return 'Of course.';
    }
    
    for (i = 0; i < yesNos.length; i += 1) {
        if (input.startsWith(yesNos[i])) {
            if (input.indexOf(' or ') > -1) { //if an or question
                ors = true;
                if (Math.floor(Math.random() * 2) === 1) {
                    return 'I\'ll go with the first one.';
                } else {
                    return 'i\'ll go with the second one';
                }
            }
            ors = false;
            //do you know questions - special response to sound more natural
            if (yesNos[i] === 'do') {
                if (input.startsWith('do you know')) {
                    return 'Sorry, I don\'t';
                }
            }
            if (i < 12) {
                if (unclearAsked) {
                    return 'Maybe? I don\'t know.';
                }
                unclearAsked = true;
                return 'Maybe? I really wouldn\'t know';
            } else { //can could should shall may
                if (unclearAsked) {
                    return 'You know I can\'t say.';
                }
                unclearAsked = true;
                return 'It\'s not my place to say one way or another.';
            }
        }
    }
    
    for (i = 0; i < openEnded.length; i += 1) {
        if (ors && (input.indexOf(' or ') > -1)) {
            if (Math.floor(Math.random() * 2) === 1) {
                return 'I\'ll go with the first one.';
            } else {
                return 'i\'ll go with the second one';
            }
        }
        ors = false;
        if (input.startsWith(openEnded[i])) {
            if (input.indexOf('are you') > -1) {
                input = input.replace('are you', 'I am');
            } else if (input.indexOf('do you') > -1) {
                input = input.replace('do you', 'I do');
            } else if (input.indexOf('is there') > -1) {
                input = input.replace('is there', 'there is');
            }
            input = input.replace('your', 'my');
            input = input.replace('yours', 'mine');
            input = input.replace('you', 'me');
            return 'I don\'t know ' + input;
        }
    }
    
    return 'Is that so?';
};

//evaluates user input and generates response
var respond = function (input) {
    'use strict';
    var normal = 'Is that so?',
        response = '', //final computer response
        responseElements = '<br /><div class=\'dialogue computer\'>',
        result = ''; //currently just a text. this will be deleted eventually
    
    //lowercases input to make future code shorter
    input = input.toLowerCase();
        
    //evaluates input for presence of question
    response = evaluateQuestion(input);
    
    //check for nonsence input
    if (response === normal) {
        response = checkValidity(input);
    }
        
    //check for greeting
    if (response === normal) {
        response = greetingCheck(input);
    }
    
    if (catTopic && response !== "Let'ts talk about cats.") {
        response = cats(input);
    }
        
    //insert computer response to UI
    $('.history').append(responseElements + response + '</div>');
    
    //scrolls history down
    scrollDown();
    
    //sends text to be processed for speach
    if (audioOn) {
        tokenize(response);
    }
};

//add user text to history
var submit = function () {
    'use strict';
    
    if ($('[name=input]').val() !== '') {
        $('.history').append('<br /><div class=\'dialogue user\'>' + $('[name=input]').val() + '</div><br />');
        
        //saves user input
        input = $('[name=input]').val();
        
        //clears textbox
        $('[name=input]').val('');
    
        //sends user input to function for processing
        setTimeout(function () { respond(input); }, 500);
        //respond(input);
    }
};

//called on page load
$('document').ready(function () {
    'use strict';
    
    //listens for submit button click
    $('#submit').click(function () {
        submit();
    });
    
    //listens for a press of the enter key
    $(document).keypress(function (e) {
        if (e.which === 13 && $('[name=input]').is(':focus')) {
            submit();
        }
    });
});