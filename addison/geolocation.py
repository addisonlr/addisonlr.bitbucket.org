import re, requests, traceback
from requests.exceptions import RequestException
from flask import g

def zip(zip):
    try:
        req = requests.get('http://www.uszip.com/zip/' + zip)
        contents = req.text.splitlines()
        
        for i in range(15):
            input_line = contents[i]
            if input_line.startswith('<title>'):
                input_line = re.sub(r'(\<.*?\>)', '', input_line.strip())
                break
        if input_line:
            return '<span class="larger">' + zip + '</span> - ZIP<hr>&ensp;<span class="larger">&bull;</span>&ensp;' + input_line.replace(' zip code', '') + '\nSource: ' + str(req.url)
        else:
            return 'There is no location with a zip code of ' + zip + '.'
    except:
        print 'There was an error in geolocation.zip()'
        traceback.print_exc()
        
def area_code(code):
    try:
        req = requests.get('http://www.allareacodes.com/' + code)
        contents = req.text.splitlines()
        
        input_line = contents[78]
        if not input_line.strip():
            return 'There is no area by the code of "' + code + '".'
        input_line = input_line.replace('<br></td></tr></table><br>	</div>', '')
        input_line = input_line.replace('<br>', ', ')
        input_line = re.sub(r'(\<.*?\>)', '', input_line)
        input_line = input_line[input_line.rindex(code) + 3:]
        return '<span class="larger">' + code + '</span> - Area Code<hr>&ensp;<span class="larger">&bull;</span>&ensp;' + input_line + '\nSource: ' + str(req.url)
    except:
        print 'There was an error in geolocation.area_code()'
        traceback.print_exc()