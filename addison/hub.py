from search import inline
import question, statement, who, what, where, when, how, words, semantics, re, hub, conversions, xkcd

def main(sentence): #routes all sentences to appropriate methods
    sentence = sentence.lower().lstrip().rstrip()
    #sentence = re.sub(r'([^\s\w\+\-\*\/\(\)\'\.\^\=\#\,\!]|_)+', '', sentence)
    word = sentence.split()
    
    if len(word) > 2:
        subject = sentence.replace(word[0] + ' ' + word[1] + ' ', '') #splits sentence after second word
    else:
        subject = ''
    
    #routes to appropriate answer method
    if hub.special_case(sentence):
        return hub.special_case(sentence)
    
    if question.is_question(word[0]): #if the sentence is a question
        if word[0] == 'who':
            if word[1] == 'was':
                return who.was(word, subject)
            elif word[1] == 'were':
                return who.were(word, subject)
            elif word[1] == 'are':
                return who.are(word, subject)
            elif word[1] == 'is':
                return who.is_(word, subject)
            else:
                return inline(word, subject, 'who')
        elif word[0] =='what':
            if word[1] == 'was':
                return what.was(word, subject)
            elif word[1] == 'are':
                return what.are(word, subject)
            elif word[1] == 'does':
                return what.does(word, subject)
            elif what.time(word, sentence):
                return what.time(word, sentence)
            elif word[1] == 'is':
                return what.is_(word, subject)
            elif word[1] == 'may' or word[1] == 'can': #what may/can i say/do/ask
                if 'i' in word:
                    return 'questiontypes'
            elif ' rhyme' in sentence:
                return words.rhyme(subject[subject.rindex(' '):])
            elif ' questions ' in subject and ' i ' in subject:
                return 'questiontypes'
            else:
                return inline(word, subject)
        elif word[0] == 'where':
            if word[1] == 'does':
                return where.does(word, subject)
            elif word[1] == 'is':
                return where.is_(word, subject)
            elif word[1] == 'am':
                return 'user-location'
            elif ' right now' in subject:
                return 'user-location'
            elif ' carmen san' in subject:
                return '<span class="larger">Carmen Sandiego</span> - Location<hr>You know, after decades of search you\'d think someone would have found her by now.'
            else:
                return inline(word, subject)
        elif word[0] == 'when':
            if word[1] == 'did':
                return when.did(word, subject)
            elif word[1] == 'was':
                return when.was(word, subject)
            elif 'you ' in subject and ' created' in subject:
                return '<span class="larger">February 10 2013</span> - Date of creation<hr>S.P.I.F.F. was first <span class="weighted">backed up to the internet on February 10 2013</span>. It started earlier as an idea, and remained as such for a couple of weeks. It was not until April 20 that S.P.I.F.F. was released on the internet. The name <span class="weighted">S.P.I.F.F was first thought of and implemented November 7 2013.</span>'
            else:
                return inline(word, subject)
        elif word[0] == 'why':
            if special_case(sentence):
                return special_case(sentence)
            return inline(word, subject, 'why')
        elif word[0] == 'how':
            if 'tootsie' in subject:
                return 'tootsie'
            elif word[1] == 'did':
                return how.did(word, subject)
            elif word[1] == 'was':
                return how.was(word, subject)
            elif word[1] == 'do':
                return how.do_(word, subject)
            elif word[1] == 'many':
                if re.match(r'\D+ (are in a|are in an|in a|in an|are in|in)(|\d+) \D+', subject):
                    initial = re.sub('\D+ (in a|in an) (\D+)', '\g<2>', subject.replace('.', ''))
                    final = re.sub('(\D+) (in a|in an) (\D+)', '\g<1>', subject.replace('.', ''))
                    return conversions.convert('', initial.replace(' are', '').replace(' there', '').strip(), final.replace(' are', '').replace(' there', '').strip(), 'how')
            elif word[1] == 'far':
                if re.match(r'^(away is |is ).*(from ).*$', subject):
                    loc1 = subject[subject.index('is ') + 3:subject.index('from ')]
                    loc2 = subject[subject.index('from ') + 5:]
                    return 'distance:' + loc1 + '&' + loc2
                elif re.match(r'^(away is |is |away am i from |am i from ).*$', subject):
                    try:
                        loc = subject[subject.index('is ') + 3:]
                    except:
                        loc = subject[subject.index('from ') + 5:]
                    return 'distance:current&' + loc
                else:
                    return inline(word, subject, 'how')
            else:
                return inline(word, subject, 'how')
        elif word[0] == 'is':
            return inline(word, sentence, 'is')
        elif word[1] == 'much':
            return statement.statement_type(sentence)
        elif special_case(sentence):
            return special_case(sentence)
        else:
            return inline(word, subject)
    else: #if not a question but a statement
        return statement.statement_type(sentence)
    
def special_case(sentence):
    if sentence == 'xkcdrandom':
        return xkcd.random()
    if ' wood ' in sentence and ' chuck' in sentence:
        return '<span class="larger">Amount of wood a woodchuck could chuck</span> - Hypothetical<hr><img src="/static/woodchuckgraph.png" id="graph"></img><span class="weighted italic">Assuming to chuck means to throw,</span>  one could assume that an average woodchuck could chuck 3 times its own weight. (The average woodchuck weighs between 4 & 9 lbs.)<br/>This could be modeled by the equation y=(6.5*3)x, where x is the number of days since the woodchuck started chucking wood.'
    
    if ' chicken ' in sentence and ' road' in sentence:
        return '<span class="larger">Chicken crossing a road</span> - Reason<hr><img id="image" style="width:50%;" src="http://upload.wikimedia.org/wikipedia/commons/9/95/Why_did_the_chicken_cross_the_road%3F.JPG"></img>There is a <a href="http://yonilerner.org/query/define%20%D7%9E%D7%97%D7%9C%D7%95%D7%A7%D7%AA">&#1502;&#1495;&#1500;&#1493;&#1511;&#1514;</a> as to the answer. <a href="http://yonilerner.org/query/hobbes">Hobbes</a> answers that the chicken was born bad, and its farmer failed to keep law and order.  <a href="http://yonilerner.org/query/john%20locke">Locke</a> answers the chicken was rebelling angainst its unjust farmer\'s rules.<br>The <a href="http://yonilerner.org/query/nahmanides">Ramban</a> disagrees; He answer that the chicken crossed the road to get to the other side.'
    
    if ' six afraid of seven' in sentence:
        return '<span class="larger">Six being afraid of seven</span> - Reason<hr><span class="weighted">It wasn\'t.</span> Numbers are not sentient and therefore do not possess the ability to fear.'
    
    if ' joke' in sentence:
        return '<span class="larger">Bar Joke</span> - Humor<hr><span class="titled" title="set-up">A <a href="http://www.yonilerner.org/query/wavefunction" target="_blank">wavefunction</a> walks into a bar and then collapses.</span> It procedes to scream, <a class="titled" title="punchline" href="http://www.yonilerner.org/query/wave%20function%collapse" target="_blank">"What are you looking at!"</a>'
