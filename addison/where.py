from search import inline
import geolocation, re

def does(words, subject):
    return inline(words, subject)

def is_(words, subject):
    if re.match(r'^(\d{3})$', subject):
        return geolocation.area_code(subject)
        
    if re.match(r'^(\d{5})$', subject):
        return geolocation.zip(subject)
    
    if len(words) == 3:
        return 'location ' + words[2]
    elif len(words) == 4:
        return 'location ' + words[2] + '+' + words[3]
    else:
        return inline(words, subject)