import requests
import json
def send_no_answer_email(question):
    """
    Straight forward mail send function. Provide to, subject, and content, and we send an email.
    """
    api_location = 'https://mandrillapp.com/api/1.0/'
    api_request_url = api_location + 'messages/send.json'
    request_json = {
        "key": "cPLXiRtpRH35jrACRf2nYg",
        "message": {
            "html": "Question: " + question,
            "subject": "Question without Answer",
            "from_email": "addisonlr@gmail.com",
            "from_name": "Addison Q&A Bug Report",
            "to": [
                {
                    "email": "addisonlr@gmail.com"
                }
            ],
            "track_opens": False,
            "track_clicks": False,
            "inline_css": True,
            "url_strip_qs": True,
            "preserve_recipients": False,
            "bcc_address": "yonilerner@gmail.com",
            "merge": True
        },
        "async": False
    }
    headers = {'content-type': 'application/json', 'accept': 'application/json'}
    try:
        mail_request = requests.post(api_request_url, data=json.dumps(request_json), headers=headers)
    except requests.exceptions.RequestException as e:
        mail_request = False
    return mail_request