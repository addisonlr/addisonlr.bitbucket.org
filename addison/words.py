import re, requests, traceback, words, semantics
from utils import send_no_answer_email
from requests.exceptions import RequestException
from flask import g
memory = ''

def define(word):
    try:
        words.memory = word
        req = requests.get('http://en.wiktionary.org/wiki/' + word)
        print str(req.url)
        if req.status_code == 404:
            return 'No definition could be found. Please check your spelling.'
        contents = req.text.splitlines()
        
        all_defs = []
        ordered_list = False #true if inside the ordered list
        
        for i in range(len(contents) - 1):
            if contents[i] == '</ol>':
                ordered_list = False;
                break
            if ordered_list:
                all_defs.append(re.sub(r'<(?!li|dd).*?>', '', contents[i]).replace('<li>', '<span class="weighted">&bull;&ensp;</span>').replace('<dd>', 'iiiii&emsp;&ndash;&nbsp;'))
            if contents[i].startswith('<ol>'):
                ordered_list = True;
                all_defs.append('<span class="larger">' + word.title() + '</span> - ' + re.sub(r'(<.*?>|\[.*?\])', '', contents[i-2]) + '<hr>')
        for i in range(len(all_defs)):
            all_defs[i] = re.sub(r'(?<![a-zA-Z ])(\(.*?\))', '<span class="weighted">\g<1></span>', all_defs[i])
            if all_defs[i].startswith('iiiii'):
                all_defs[i] = '<span class="italic hanging">' + all_defs[i].replace('iiiii', '') + '</span>'
        return re.sub('\\n', '', '\n'.join(filter(None, all_defs)), 1) + 'Source: ' + str(req.url)
    except:
        print 'There was an error in words.define().'
        traceback.print_exc()

def etymologize(word):
    words.memory = word
    try:
        req = requests.get('http://en.wiktionary.org/wiki/' + word)
        if req.status_code == 404:
            return 'No etymology could be found. Please check your spelling.'
        contents = req.text.splitlines()
        
        input_line = ''
        for i in range(len(contents) - 1):
            if contents[i].startswith('<h3>') and 'Etymology' in contents[i]:
                input_line = contents[i+1]
                break
                
        input_line = re.sub(r'(<.*?>|\&.*?;)', '', input_line)
        input_line = re.sub(r'(\(.*?\))', '<span class="italic hanging">\g<1></span>', input_line)
        input_line = input_line[0:1].lower() + input_line[1:]
        
        try:
            first = ' ' + input_line[:input_line.index('. ')]
            first = first.replace(' from ', ' <span class="larger">&#10137;</span> ')
            input_line = input_line.replace(input_line[:input_line.index('. ')], first)
        except:
            if input_line.endswith('.'):
                input_line = input_line.replace(' from ', ' <span class="larger">&#10137;</span> ')
        
        other = words.etymologize_other(word)
        return '<span class="larger">' + word.strip().title() + '</span> - Etymology<hr>' + word.strip().title() + ' ' + input_line.replace('(', ' (') + '\nSource: ' + str(req.url) + other
    except:
        traceback.print_exc()

def etymologize_other(word):
    try:
        req = requests.get('http://www.etymonline.com/index.php?allowed_in_frame=0&search=' + word + '&searchmode=none')
        if req.status_code == 404:
            return ''
        contents = req.text.splitlines()
        
        for i in range(80, 200):
            if contents[i].strip().startswith('<dt class="highlight">') and word in contents[i]:
                print 'etymonline'
                input_line = contents[i+1]
                chosen = re.sub(r'<.*?>', '', contents[i])
                chosen = re.sub(r'\(.*?\)', '', chosen).strip()
                break
        input_line = re.sub('<(?!span|/span).*?>', '', input_line.replace('class="foreign"', 'class=\'weighted\'').replace(' from ', ' <span class=\'larger\'>&#10137;</span> '))
        input_line = re.sub('(".*?")', '<span class="italic">\g<1></span>', input_line)
        
        if input_line == '':
            return ''
        return 'there is another<span class="larger">' + chosen.title() + '</span> - Etymology<hr>' + input_line.replace('(', ' (') + '\nSource: ' + str(req.url)
    except:
        return ''
        
def translate(word, language):
    words.memory = word
    try:
        req = requests.get('http://en.wiktionary.org/wiki/' + word)
        contents = req.iter_lines()
        
        terms = []
        parts = ''
        section = False
        for input_line in contents:
            input_line = input_line.decode('utf-8')
            if input_line.startswith('<h') and 'Translations' in input_line:
                section = True
            elif input_line.startswith('<h2>') and section:
                break
                
            if section:
                if input_line.startswith('<div class="NavHead"') and 'tp be checked' not in re.sub(r'(<.*?>)', '', input_line):
                    terms.append(re.sub(r'(<.*?>)', '', input_line))
                elif input_line.lower().startswith('<li>' + language + ':'):
                    terms.append(re.sub(r'(\&.*?\;)', '', re.sub(r'(<.*?>|\(..\))', '', input_line)))
                    
        print terms
        language = language.title().strip()
        for i in range(len(terms)):
            if language in terms[i]:
                terms[i] = terms[i].replace(language + ':', '').strip()
                if i == 1:
                    parts += '&ensp;<span class="weighted">&bull;&ensp;' + terms[i] + '</span> in terms ' + terms[i - 1]
                else:
                    parts += '<br />&ensp;<span class="weighted">&bull;&ensp;' + terms[i] + '</span> in terms ' + terms[i - 1]
            elif terms[i].startswith(('a', 'e', 'i', 'o', 'u')):
                terms[i] = 'of an ' + terms[i]
            else:
                terms[i] = 'of ' + terms[i]
                
        parts = parts.replace(', .', '.')
        if ', ' in parts:
            parts = parts[0:parts.rindex(', ') + 2] + 'and ' + parts[parts.rindex(', ') + 2:]
            
        if not parts.replace('.', ''):
            return word.title() + ' <span class="weighted">could not be translated</span> to ' + language
            send_no_answer_email('translate ' + word + ' to ' + language)
        
        return '<span class="larger">' + word.strip().title() + '</span> - English to ' + language.strip() + '<hr>' + parts.replace(' , ', ' ').replace(' and', ' and ') + '\nSource: ' + str(req.url)
    except:
        traceback.print_exc()

def antonyms(word):
    words.memory = word
    try:        
        req = requests.get('http://www.synonyms.net/antonyms/' + word)
        contents = req.text.splitlines()
        
        for input_line in contents:
            if input_line.strip().startswith('<div class="rc5">'):
                break
            
        input_line = input_line[input_line.index('<strong>Antonyms:</strong>'):]
        input_line = input_line[26: input_line.index('</p>') + 4]
        input_line = re.sub(r'(<.*?>)', '', input_line)
        input_line = input_line[:input_line.rindex(', ')] + ', and ' + input_line[input_line.rindex(', ')+1:]
        
        return '<span class="larger">' + word.title().strip() + '</span> - Antonyms<hr>' + input_line + '.\nSource: ' + str(req.url)
    except:
        traceback.print_exc()

def synonyms(word):
    words.memory = word
    try:
        req = requests.get('http://www.synonyms.net/antonyms/' + word)
        contents = req.text.splitlines()
        
        for input_line in contents:
            if input_line.strip().startswith('<div class="rc5">'):
                break
            
        input_line = input_line[input_line.index('<strong>Synonyms:</strong>'):]
        input_line = input_line[26: input_line.index('</p>') + 4]
        input_line = re.sub(r'(<.*?>)', '', input_line)
        input_line = input_line[:input_line.rindex(', ')] + ', and ' + input_line[input_line.rindex(', ')+1:]
        
        return '<span class="larger">' + word.strip().title() + '</span> - Synonyms<hr>' + input_line + '.\nSource: ' + str(req.url)
    except:
        traceback.print_exc()
        
def expand(abbr):
    try:
        req = requests.get('http://www.abbreviations.com/' + abbr)
        contents = req.text.splitlines()
        
        for i in range(110, 150):
            if abbr in contents[i].lower():
                input_line = contents[i]
                break
            elif i == 149:
                return '<span class="weighted">No match was found</span> for ' + abbr + '.'
        
        input_line = re.sub(r'<.*?>', ', ', input_line[input_line.index('<td class="tal tm">') : input_line.index('</p>', input_line.index('</p>') + 10)])
        
        part_list = input_line.split(', ')
        shortened = part_list[2];
        expanded = part_list[6];
        
        return '<span class="weighted">' + shortened + '</span> is short for <span class="weighted">' + expanded + '</span>.\nSource: ' + str(req.url)
    except:
        traceback.print_exc()

def contract(phrase):
    try:
        req = requests.get('http://www.abbreviations.com/serp.php?st=' + phrase + '&qtype=2')
        contents = req.text.splitlines()
        
        for i in range(110, 150):
            if phrase in contents[i].lower():
                input_line = contents[i]
                break
            elif i == 149:
                return '<span class="weighted">No abbreviation or acronym was found</span> for ' + phrase + '.'
        
        input_line = re.sub(r'<.*?>', ', ', input_line[input_line.index('<td class="tal tm">'):input_line.index('</p>', input_line.index('</p>') + 10)])
        
        part_list = input_line.split(', ')
        shortened = part_list[2];
        expanded = part_list[6];
        
        return '<span class="weighted">' + shortened + '</span> is short for <span class="weighted">' + expanded + '</span>.\nSource: ' + str(req.url)
    except:
        traceback.print_exc()

def explain(phrase):
    try:
        req = requests.get('http://www.phrases.net/psearch/' + phrase.replace(' ', '%20'))
        contents = req.text.splitlines()
        
        for i in range(105, 120):
            if contents[i].strip().startswith('<table class="tdata"'):
                input_line = contents[i]
                break
            elif i == 119:
                return 'No explanation could be found for ' + phrase + '.'
        
        input_line = re.sub(r'<.*>', '', input_line[:input_line.index('</a></td>')]).strip().lower()
        
        return '<span class="weighted">' + phrase.title() + '</span> - Phrase<hr>&ensp;<span class="larger">&bull;</span>&ensp;' + input_line + '\nSource: ' + str(req.url)
    except:
        traceback.print_exc()

def rhyme(word):
    try:
        req = requests.get('http://www.rhymes.net/rhyme/' + word)
        contents = req.text.splitlines()
        
        for i in range(80, 150):
            if contents[i].strip().startswith('<div class="row">'):
                input_line = contents[i]
                break
            elif i == 149:
                return '<span class="weighted">No rhymes could be found </span> for ' + word + '.'
            
        input_line = re.sub(r'<.*?>', '', input_line.lstrip().rstrip().replace('</header>', ':'))
        input_line = re.sub(r'(\d+ (Syllables|Syllable):)', '<br /><span class="weighted">\g<1></span>', input_line)
        input_line = re.sub('<br />', '', input_line, 1)
        input_line = input_line.replace(' &middot;', ', ')
        
        ulist = []
        [ulist.append(x) for x in input_line.split(', ') if x not in ulist]
        input_line = ', '.join(ulist)
        
        print input_line
        return '<span class="larger">' + word.strip().title() + '</span> - Rhymes<hr>' + input_line + '\nSource: ' + str(req.url)
    except:
        traceback.print_exc()