import wikipedia, words, mathematics, geolocation, finance, semantics, re, conversions
from search import inline

def was(words, subject):
    if wikipedia.summarize(subject):
        return wikipedia.summarize(subject)
    else:
        return inline(words, subject, 'are')
    
def are(words, subject):
    if re.match(r'^\w+?(s|\'s) stock', subject): #gets stock info
        if re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ year', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'y')
        elif re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ month', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'm')
        elif re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ week', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'w')
        elif re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ day', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'd')
        else:
            return finance.stocks(word[2].replace('\'', '')[:-1], '6m')
        
    if wikipedia.summarize(subject):
        return wikipedia.summarize(subject)
    else:
        return inline(words, subject, 'are')
    
def does(word, subject):
    if re.match(r'spiff (stand for|mean).*', subject):
        return '<span class="larger">SPIFF</span> - Acronym<hr>SPIFF stands for <span class="larger">S</span>emantic and <span class="larger">P</span>resentable <span class="larger">I</span>nformation <span class="larger">F</span>inder.'
    if re.match(r'^\w+? mean', subject): #defines words
        '''if subject.startswith('it '):
            return words.define(words.memory)
        else:'''
        return words.define(word[2])
    elif re.match(r'^(\w+? ){2,}mean', subject): #defines phrases
        return words.explain(subject.replace(' mean', '').lstrip().rstrip())
    elif re.match(r'(.*mean)', subject): #if not word or phrase
        return 'What about mean?'
        
    if re.match(r'\w+( stand for)', subject): #expands abbreviations and acronyms
        return words.expand(word[2])
    elif re.match(r'.*(stand for)', subject):
        return 'Acronyms and abbreviations cannot be more or less than one word.\n'
        
    if re.match(r'.+(look like)', subject): #what does blank look like
        '''if subject.startswith('it '):
            return wikipedia.show(w.topic)
        else:'''
        return wikipedia.show(subject[:subject.index(' look like')])
    
    if re.match(r'.* rhyme with', subject):
        return words.rhyme(subject.replace(' rhyme with', ''))
    
    return inline(words, inline) #if nothing else worked

def time(word, sentence):
    if ' time' in sentence and (' it' in sentence or ' the ' in sentence):
        return 'time'
    
    if ' is ' in sentence and ' date' in sentence:
        return 'date'
    
    if ' is ' in sentence and (' day' in sentence or 'today' in sentence) and not re.match(r'.*\d.*', sentence):
        return 'day'
        
def is_(word, subject):
    if re.match(r'^(\d+)( |)(\D+)( in | to )(\D+)', subject.replace('.', '')):
        amount = re.sub(r'^(\d+|\d+\.\d+)( |)(\D+)( in | to )(\D+)', '\g<1>', subject)
        initial = re.sub(r'^(\d+)( |)(\D+)( in | to )(\D+)', '\g<3>', subject.replace('.', ''))
        final = re.sub(r'^(\d+)( |)(\D+)( in | to )(\D+)', '\g<5>', subject.replace('.', ''))
        return conversions.convert(amount, initial, final)
    
    if subject.startswith('the difference between'): #finds difference between
        firstDet = subject[23:subject.index(' and ')]
        secondDet = subject[subject.index(' and ') + 5:].rstrip()
        if firstDet.endswith('s'):
            firstDet = firstDet[:len(firstDet) - 1]
        if secondDet.endswith('s'):
            secondDet = secondDet[:len(secondDet) - 1]
        if firstDet.startswith('a '):
            firstDet = firstDet[2:]
        if secondDet.startswith('a '):
            secondDet = secondDet[2:]
        return semantics.diffBet(firstDet, secondDet, 'miscellaneous', False)
    
    if re.match('^\d{5}$', subject): #gets information about zip codes
        return geolocation.zip(subject.strip())
        
    if re.match('^\d{3}$', subject): #gets information about area codes
        return geolocation.area_code(subject.strip())
    
    if subject.startswith('the ') and ' weather' in subject:
        if re.match(r'^the weather in (?!my )', subject):
            return 'weather-' + subject.replace('the weather in ', '').replace(' ', '_')
        else:
            return 'weather'
        
    if any(i in subject for i in ('+', '-', '*', '/', 'sin(', 'cos(', 'tan(', 'sqrt(', 'abs(', 'sec(', 'csc(', 'cot(')) and '()' not in subject: #math
        return mathematics.calculate(subject)
    
    if re.match(r'^\w+?(s|\'s) stock', subject): #gets stock info
        if re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ year', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'y')
        elif re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ month', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'm')
        elif re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ week', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'w')
        elif re.match(r'^\w+?(s|\'s) stock[a-z ]+?for \d+ day', subject):
            return finance.stocks(word[2].replace('\'', '')[:-1], re.sub('.* (\d+) .*', '\g<1>', subject) + 'd')
        else:
            return finance.stocks(word[2].replace('\'', '')[:-1], '6m')
            
    if ' definition ' in subject or ' meaning ' in subject: #defines a word
        return words.define(subject[subject.rindex(' '):])
        
    if ' etymology ' in subject or ' origin ' in subject: #returns etymology of a word
        return words.etymologize(subject[subject.rindex(' '):])
    
    if re.match(r'^\w+? in \w+', subject): #translates a word from english
        if re.match(r'.*?\d.*', subject):
            return 'Numbers can only be translated to another language when written out.'
        return words.translate(word[2], word[4])
        
    if re.match(r'^(the opposite of )\w+$', subject): #returns opposites of a word
        return words.antonyms(word[5])
    elif re.match(r'^(the opposite of ).+$', subject):
        return 'Phrases do not have antonyms. Try using a word instead.'
        
    #returns synonyms of a word
    if re.match(r'^(the same as |another word for )\w+$', subject):
        return words.synonyms(word[5])
    elif re.match(r'(another way of saying )\w+$', subject):
        return words.synonyms(word[6])
    elif re.match(r'^(the same as |another word for |another way of saying ).+$', subject):
        return 'Phrases do not have synonyms. Please try using a word instead.'
        
    #expands abbreviations and acronyms
    if re.match(r'.+( short for)$', subject):
        return words.expand(word[2])
        
    #finds abbreviation or acronyms of a word
    if re.match(r'(short for ).+$', subject):
        return words.contract(subject.replace('short for ', ''))
        
    if re.match(r'.+( in terms of)( | a).+', subject): #gets summary of a topic
        subject = re.sub(r'( in terms of)( a | )', ' (', subject) + ')'
        return wikipedia.summarize(subject)
    
    #checks for various contextual questions
    if re.match(r'(its )(origin|etymology).*', subject):
        return words.etymologize(words.memory)
    elif re.match(r'(its )(definition|meaning).*', subject):
        return words.define(words.memory)
    elif re.match(r'^(its opposite)$', subject):
        return words.antonyms(words.memory)
    elif re.match(r'^(the same as it|it the same as)$', subject):
        return words.synonyms(words.memory)
    elif re.match(r'^(its).*', subject) and w.topic:
        detail = subject.replace('its ', '').lstrip().rstrip()
        return wikipedia.elaborate(w.topic, detail, " is ")
    elif re.match(r'(it in).*', subject):
        return words.translate(w.memory, words[4])
        
    if re.match(r'.+(\'s).*$', subject): #example - the sun's surface area
        topic = subject[:subject.index("'")].replace(' ', '_').lstrip().rstrip()
        detail = subject[subject.index("'") + 2:].lstrip().rstrip()
        if wikipedia.elaborate(topic, detail, ' is '):
            return wikipedia.elaborate(topic, detail, ' is ')
        
    if re.match(r'the .* of .*', subject):
        detail = subject[subject.index('the ') + 4:subject.index(' of ')].replace(' ', '_').strip()
        topic = subject[subject.index(' of ') + 4:].strip()
        if wikipedia.elaborate(topic, detail, ' is '):
            return wikipedia.elaborate(topic, detail, ' is ')
        
    if wikipedia.summarize(subject): #returns a summary of a topic only if one is found
        return wikipedia.summarize(subject)
        
    if inline(word, subject):
        return inline(word, subject)