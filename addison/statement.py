import wikipedia, words, mathematics, geolocation, speech, re, conversions, calculus
from utils import send_no_answer_email

def statement_type(sentence):
    if 'tic' in sentence and 'tac' in sentence and 'toe' in sentence and 'play' in sentence:#true if trying to play tic tac toe
        return 'tic-tac-toe'
    
    if 'pong' in sentence:
        return 'pong'
        
    if sentence.startswith(('speak ', 'say ')): #if looking for things to be read aloud
        return speech.tokenize(sentence[sentence.index(' '):])
    
    if sentence.startswith('defin'): #if looking for definition
        return words.define(sentence[sentence.rindex(' '):].strip())
    
    if sentence.startswith(('etymolog', 'origin of ')): #if looking for etymology
        return words.etymologize(sentence[sentence.rindex(' '):])
    
    if sentence.startswith('opposite') and ' ' in sentence:
        return words.antonyms(sentence[sentence.rindex(' '):])
    
    if sentence.startswith('synonym') and ' ' in sentence:
        return words.synonyms(sentence[sentence.rindex(' '):])
    
    if 'periodic table' in sentence:
        return 'periodictable'
    
    if re.match(r'^(\d+)( |)(\D+)( in | to )(\D+)', sentence.replace('.', '')):
        amount = re.sub(r'^(\d+|\d+\.\d+)( |)(\D+)( in | to )(\D+)', '\g<1>', sentence)
        initial = re.sub(r'^(\d+)( |)(\D+)( in | to )(\D+)', '\g<3>', sentence.replace('.', ''))
        final = re.sub(r'^(\d+)( |)(\D+)( in | to )(\D+)', '\g<5>', sentence.replace('.', ''))
        return conversions.convert(amount, initial, final)
    
    if ' in ' in sentence and 'terms of' not in sentence: #if wanting translation from english
        return words.translate(re.sub(r'( in ).*', '', sentence), re.sub(r'.*( in )', '', sentence))
        
    if sentence.startswith(('picture', 'image', 'show me', 'draw')):
        return wikipedia.show(sentence[sentence.rindex(' '):])
    
    if sentence.strip().startswith(('y=', 'y =', 'graph')):
        return mathematics.graph(sentence.replace(' ', '').replace('graph', '')[2:])
    
    if sentence.strip().startswith(('z=', 'z =')):
        return '3D graphing <span class="weighted">is being looked into.</span>'
        return mathematics.three_d_graph(sentence.replace(' ', '')[2:])
    
    if sentence.startswith(('r=', 'r =')): #if a polar equation
        return mathematics.polar_graph(sentence.replace(' ', '')[2:])
        
    if sentence.strip().startswith(('z=', 'z =')): #if a fractal equation
        return 'It appears you\'ve requested the graph of a fractal or 3d equation. These features are not available at this time.'
    
    if sentence.startswith('derivative of'):
        return calculus.derive(sentence.replace('derivative of', '').strip())
        
    if re.match(r'(^((-|)(\d+?),){3,}(-|)(\d+?)$)', sentence.strip()):
        return mathematics.pattern(map(int, sentence.strip().split(',')))
        
    if any(i in sentence for i in ('+', '-', '*', '/', '^', 'sin(', 'cos(', 'tan(', 'csc(', 'sec(', 'cot(', 'sqrt(', 'abs(', '!', 'ln(', 'log(')) and '()' not in sentence: #if math
        return mathematics.calculate(sentence)
        
    if re.match(r'^(\d{3})$', sentence): #if area code
        return geolocation.area_code(sentence)
        
    if re.match(r'^(\d{5})$', sentence): #if zip code
        return geolocation.zip(sentence)
    
    if wikipedia.summarize(sentence): #if wikipedia has an article on it
        return wikipedia.summarize(sentence)
        
    send_no_answer_email(sentence) #if no answer then send email to Addison (Developer)
    return 'The answer to that "question" is as hard to find as the location of Carmen Sandiego'