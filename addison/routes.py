from flask import abort, render_template, flash, request, redirect, Blueprint, g
from hub import main
from utils import send_no_answer_email
import re
import subprocess
import os
import traceback

site = Blueprint('addison', 'addison', template_folder='templates', static_folder='static')

@site.route('/')
def index():
    return render_template('home.html')

@site.route('/query/<path:userq>')
def query(userq):
    return render_template('home.html')

@site.route('/question/<path:question>')
def question(question):
    if not request.is_xhr:
        abort(404)
    g.answer = ''
    try:
        ret = main(question)
    except:
        send_no_answer_email(question)
        traceback.print_exc()
        return 'Oh no! A bug clogged the pipes! A team of highly trained entomologists will be dispatched shortly.'
    if not ret:
        ret = 'The answer is 42\nSource: http://en.wikipedia.org/wiki/42_(number)#The_Hitchhiker.27s_Guide_to_the_Galaxy'
        send_no_answer_email(question)
    return ret

@site.route('/update')
def update():
    try:
        subprocess.call(['/home/ubuntu/python/addison/update-script.sh'])
    except:
        return 'Update failed. Please contact yonilerner@gmail.com'
    return 'Updated'

@site.route('/grades')
def grades():
    return render_template('grades.html')

@site.route('/useragent')
def useragent():
    print request.headers
    x = ''
    for header in request.headers:
        x += str(header)
        x += "<br />"
    x += "<a href='/useragent2'>Test</a>"
    return x


@site.route('/useragent2')
def useragent2():
    print request.headers
    x = ''
    for header in request.headers:
        x += str(header)
        x += "<br />"
    return x

@site.route('/ip')
def ip():
    return request.headers['X-Forwarded-For']
