self.addEventListener('message', function(e) {
    var data = e.data;
    var speed = Math.sqrt(Math.pow(data.x2 - data.x, 2) + Math.pow(data.y2 - data.y, 2)) / 75;
    self.postMessage(speed);
}, false);