/*global alert, summarize, elaborate*/
function whoWas(word, subject) {
    'use strict';
    var topic = subject.slice(0, subject.indexOf('\'')).trim(),
        detail = subject.slice(subject.indexOf('\'') + 2).trim();
    
    if (new RegExp('^.+\'s.+$').test(subject)) {
        elaborate(topic, detail, ' was ');
    } else {
        summarize(subject);
    }
}