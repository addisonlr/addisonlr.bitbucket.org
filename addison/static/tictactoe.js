var AI, Game, Player, data, startGame, util,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

/*
    # The `data` object carries a few important values
    # combinations: All the possible win combinations
    # sides: All of the side spaces in the game
    # corners: All of the corner spaces in the game
    # player: The human player
    # ai: The AI player
    # game: The current game
*/

data = {
  combinations: [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]],
  corners: [[1, 1], [1, 3], [3, 1], [3, 3]],
  sides: [[1, 2], [2, 1], [2, 3], [3, 2]],
  ai: null,
  player: null,
  game: null
};
util = {
  cellToRowColumn: function(cell) {
    var column, row;
    row = Math.floor(cell / 3) + 1;
    column = (cell + 1) - (3 * (row - 1));
    return [row, column];
  },
  cornerFromRowColumn: function(row, column) {
    var corner, i, _i, _len, _ref;
    _ref = data.corners;
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      corner = _ref[i];
      if (corner[0] === row && corner[1] === column) {
        return i;
      }
    }
    return false;
  },
  distanceBetweenSpaces: function(row1, column1, row2, column2) {
    return Math.abs(row1 - row2) + Math.abs(column1 - column2);
  },
  getElem: function(row, column) {
    return $('.tictactoe .' + row).find('.' + column);
  },
  getRowColumn: function(elem) {
    return [+elem.parent().attr('class'), +elem.attr('class')];
  },
  opposite: function(item) {
    switch (item) {
      case 0:
        return 3;
      case 1:
        return 2;
      case 2:
        return 1;
      case 3:
        return 0;
    }
  },
  rowColumnBySameRowOrColumn: function(row1, column1, row2, column2) {
    return (row1 === row2) || (column1 === column2);
  },
  rowColumnIsCorner: function(row, column) {
    var corner, _i, _len, _ref;
    _ref = data.corners;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      corner = _ref[_i];
      if (row === corner[0] && column === corner[1]) {
        return true;
      }
    }
    return false;
  },
  rowColumnIsSide: function(row, column) {
    var side, _i, _len, _ref;
    _ref = data.sides;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      side = _ref[_i];
      if (row === side[0] && column === side[1]) {
        return true;
      }
    }
    return false;
  },
  rowColumnToCell: function(row, column) {
    return (3 * (row - 1)) + (column - 1);
  },
  sortRowColumnByDistance: function(a, b) {
    return a[0] - b[0];
  }
};
startGame = function() {
  var aiGoesFirst;
  $('.tictactoe td').text('').data('checked', null);
  $('#tictactoemessage').empty();
  aiGoesFirst = $('#AIGoesFirst').is(':checked');
  return data.game = new Game(aiGoesFirst);
};
Player = (function() {

  function Player(goesFirst, game) {
    this.isAI = false;
    this.goesFirst = goesFirst;
    this.game = game;
    this.myTurn = goesFirst;
    this.playerOrAI = 1;
    this.moves = [];
    this.corners = [];
    this.sides = [];
  }

  Player.prototype.aboutToLose = function() {
    var nextMove, winner, _ref;
    _ref = this.game.checkIfAboutToWinOrLose(this.playerOrAI, true), winner = _ref[0], nextMove = _ref[1];
    return [winner !== this.playerOrAI && winner !== false, nextMove];
  };

  Player.prototype.aboutToWin = function() {
    var nextMove, winner, _ref;
    _ref = this.game.checkIfAboutToWinOrLose(this.playerOrAI), winner = _ref[0], nextMove = _ref[1];
    return [winner === this.playerOrAI, nextMove];
  };

  Player.prototype.canMove = function(row, column) {
    return this.game.spaceIsOpen(row, column);
  };

  Player.prototype.move = function(row, column) {
    var didWin, xORo;
    if (!this.canMove(row, column)) {
      return;
    }
    if (!this.myTurn) {
      return;
    }
    if (this.game.isOver) {
      return;
    }
    this.myTurn = false;
    this.moves.push([row, column]);
    if (util.rowColumnIsCorner(row, column)) {
      this.corners.push([row, column]);
    } else if (util.rowColumnIsSide(row, column)) {
      this.sides.push([row, column]);
    }
    xORo = this.isAI === true ? 'O' : 'X';
    this.game.makeMove(row, column, xORo);
    didWin = this.game.checkIfWon();
    if (didWin) {
      return this.game.gameOver(didWin);
    } else if (this.game.spacesLeft() === 0) {
      return this.game.gameOver(false);
    }
  };

  Player.prototype.takeTurn = function(row, column) {
    return this.move(row, column);
  };

  return Player;

})();
AI = (function(_super) {

  __extends(AI, _super);

  function AI(goesFirst, game) {
    AI.__super__.constructor.call(this, goesFirst, game);
    this.isAI = true;
    this.playerOrAI = 2;
  }

  AI.prototype.bestCornerForOpponentsMove = function(compareRowColumn) {
    var column, column2, compColumn, compRow, corner, distance, newResults, result, results, row, row2, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2;
    if (compareRowColumn == null) {
      compareRowColumn = false;
    }
    results = [];
    _ref = data.corners;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      corner = _ref[_i];
      row = corner[0], column = corner[1];
      _ref1 = this.game.player.sides;
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        _ref2 = _ref1[_j], row2 = _ref2[0], column2 = _ref2[1];
        if (this.canMove(row, column)) {
          results.push([util.distanceBetweenSpaces(row, column, row2, column2), row, column]);
        }
      }
    }
    if (results.length) {
      if (compareRowColumn) {
        compRow = compareRowColumn[0], compColumn = compareRowColumn[1];
        newResults = [];
        for (_k = 0, _len2 = results.length; _k < _len2; _k++) {
          result = results[_k];
          distance = result[0], row = result[1], column = result[2];
          if (((compRow === row) || (compColumn === column)) && this.game.spaceBetweenIsOpen(row, column, compRow, compColumn)) {
            newResults.push([1, row, column]);
          } else {
            newResults.push([2, row, column]);
          }
        }
        results = newResults;
      }
      return results.sort(util.sortRowColumnByDistance);
    } else {
      return false;
    }
  };

  AI.prototype.bestCornerMove = function(compareRowColumn) {
    var column, corner, cornerMove, cornerMoves, cornerNumber, i, oppositeColumn, oppositeRow, row, weight, _i, _j, _len, _len1, _ref, _ref1, _ref2;
    if (compareRowColumn == null) {
      compareRowColumn = false;
    }
    cornerMoves = this.bestCornerForOpponentsMove(compareRowColumn);
    if (cornerMoves) {
      for (_i = 0, _len = cornerMoves.length; _i < _len; _i++) {
        cornerMove = cornerMoves[_i];
        weight = cornerMove[0], row = cornerMove[1], column = cornerMove[2];
        cornerNumber = util.cornerFromRowColumn(row, column);
        _ref = data.corners[util.opposite(cornerNumber)], oppositeRow = _ref[0], oppositeColumn = _ref[1];
        if (this.canMove(row, column) && !this.game.playerOwns(oppositeRow, oppositeColumn)) {
          this.move(row, column);
          return true;
        }
      }
    }
    _ref1 = data.corners;
    for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
      corner = _ref1[i];
      row = corner[0], column = corner[1];
      _ref2 = data.corners[util.opposite(i)], oppositeRow = _ref2[0], oppositeColumn = _ref2[1];
      if (this.canMove(row, column) && !this.game.playerOwns(oppositeRow, oppositeColumn)) {
        this.move(row, column);
        return true;
      }
    }
    return false;
  };

  AI.prototype.bestSideMove = function() {
    var column, i, move, oppositeColumn, oppositeRow, row, _i, _len, _ref, _ref1;
    _ref = data.sides;
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      move = _ref[i];
      row = move[0], column = move[1];
      _ref1 = data.sides[util.opposite(i)], oppositeRow = _ref1[0], oppositeColumn = _ref1[1];
      if (this.canMove(row, column) && !this.game.playerOwns(oppositeRow, oppositeColumn)) {
        this.move(row, column);
        return true;
      }
    }
    return false;
  };

  AI.prototype.calculateBestMove = function() {
    var moveSuccessful, userCorners;

    userCorners = this.game.player.corners;

    if (!this.goesFirst) {
      if (this.moves.length === 0) {
        if (this.canMove(2, 2)) {
          this.move(2, 2);
          moveSuccessful = true;
        }
      } else if (this.moves.length === 1 && userCorners.length === 1) {
        moveSuccessful = this.bestCornerMove();
        if (moveSuccessful) {
          return;
        }
      }
    } else if (this.corners.length < 2) {
      if (userCorners.length > this.corners.length) {
        if (userCorners.length === 1) {
          if (this.canMove(2, 2)) {
            this.move(2, 2);
            return;
          } else {
            moveSuccessful = this.bestSideMove();
            if (moveSuccessful) {
              return;
            }
          }
        } else if (userCorners.length > 1) {
          moveSuccessful = this.bestSideMove();
          if (moveSuccessful) {
            return;
          }
        }
      }
      moveSuccessful = this.bestCornerMove(this.corners[0]);
      if (moveSuccessful) {
        return;
      }
    } else {
      if (this.canMove(2, 2)) {
        this.move(2, 2);
        return;
      }
    }

    if (!moveSuccessful) {
      moveSuccessful = this.fallBackToAllMoves();
    }

    return moveSuccessful;
  };

  AI.prototype.fallBackToAllMoves = function() {
    var column, i, i2, ret, row, _i, _j;
    ret = false;
    for (i = _i = 1; _i <= 3; i = ++_i) {
      if (ret) {
        break;
      }
      for (i2 = _j = 1; _j <= 3; i2 = ++_j) {
        row = i;
        column = i2;
        if (this.canMove(row, column)) {
          this.move(row, column);
          ret = true;
        }
      }
    }
    return ret;
  };

  AI.prototype.takeTurn = function() {
    var nextMove, willLose, willWin, _ref, _ref1;
    _ref = this.aboutToWin(), willWin = _ref[0], nextMove = _ref[1];
    if (willWin) {
      this.move.apply(this, util.cellToRowColumn(nextMove));
      return true;
    } else {
      _ref1 = this.aboutToLose(), willLose = _ref1[0], nextMove = _ref1[1];
      if (willLose) {
        this.move.apply(this, util.cellToRowColumn(nextMove));
        return true;
      }
    }
    this.calculateBestMove();
    return true;
  };

  return AI;

})(Player);
Game = (function() {

  function Game(aiGoesFirst) {
    this.isOver = false;
    this.aiGoesFirst = aiGoesFirst;
    this.currentTurn = aiGoesFirst ? 2 : 1;
    this.player = data.player = new Player(!aiGoesFirst, this);
    this.ai = data.ai = new AI(aiGoesFirst, this);
    this.generateCellData();
    if (this.aiGoesFirst) {
      this.ai.takeTurn();
    }
  }

  Game.prototype.aiOwns = function(row, column) {
    var cell;
    cell = util.rowColumnToCell(row, column);
    return this.cells[cell] === 2;
  };

  Game.prototype.cellIsOpen = function(cell) {
    return this.cells[cell] === false;
  };

  Game.prototype.checkIfAboutToWinOrLose = function(player, lose) {
    var combination, nextMove, ret, winner, _i, _len, _ref, _ref1;
    if (lose == null) {
      lose = false;
    }
    winner = false;
    nextMove = false;
    _ref = data.combinations;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      combination = _ref[_i];
      ret = (_ref1 = this.combinationAboutToWIn(combination), winner = _ref1[0], nextMove = _ref1[1], _ref1);
      if (lose) {
        if (winner) {
          break;
        }
      } else {
        if (winner === player) {
          break;
        }
      }
    }
    return ret;
  };

  Game.prototype.checkIfWon = function() {
    var combination, winner, _i, _len, _ref;
    _ref = data.combinations;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      combination = _ref[_i];
      winner = this.combinationIsWIn(combination);
      if (winner) {
        return winner;
      }
    }
    return false;
  };

  Game.prototype.combinationAboutToWIn = function(combination) {
    var a, b, c, nextMove, winner;
    a = combination[0], b = combination[1], c = combination[2];
    winner = false;
    nextMove = false;
    if (this.cells[a] === this.cells[b] && this.cellIsOpen(c)) {
      winner = this.cells[a];
      nextMove = c;
    } else if (this.cells[b] === this.cells[c] && this.cellIsOpen(a)) {
      winner = this.cells[b];
      nextMove = a;
    } else if (this.cells[c] === this.cells[a] && this.cellIsOpen(b)) {
      winner = this.cells[c];
      nextMove = b;
    }
    return [winner, nextMove];
  };

  Game.prototype.combinationIsWIn = function(combination) {
    var a, b, c, winner;
    a = combination[0], b = combination[1], c = combination[2];
    winner = false;
    if (this.cells[a] !== false && this.cells[a] === this.cells[b] && this.cells[b] === this.cells[c]) {
      winner = this.cells[a];
    }
    return winner;
  };

  Game.prototype.gameOver = function(winner) {
    var message;
    this.isOver = true;
    this.winner = winner;
    message = '';
    if (winner === 1) {
      message = 'You won!';
    } else if (winner === 2) {
      message = 'You lost!';
    } else {
      message = 'Cats game!';
    }
    return $('#tictactoemessage').append(message);
  };

  Game.prototype.generateCellData = function() {
    var cells;
    cells = [];
    $('.tictactoe td.1, .tictactoe td.2, .tictactoe td.3').each(function(i, elem) {
      var ret;
      elem = $(elem);
      ret = false;
      return cells.push((function() {
        switch (elem.text()) {
          case 'X':
            return 1;
          case 'O':
            return 2;
          default:
            return false;
        }
      })());
    });
    return this.cells = cells;
  };

  Game.prototype.makeMove = function(row, column, xORo) {
    var elem;
    elem = util.getElem(row, column);
    elem.data('checked', true);
    elem.text(xORo);
    if (this.currentTurn === 1) {
      this.currentTurn = 2;
      this.player.myTurn = false;
      this.ai.myTurn = true;
    } else {
      this.currentTurn = 1;
      this.player.myTurn = true;
      this.ai.myTurn = false;
    }
    return this.generateCellData();
  };

  Game.prototype.playerOwns = function(row, column) {
    var cell;
    cell = util.rowColumnToCell(row, column);
    return this.cells[cell] === 1;
  };

  Game.prototype.spaceBetweenIsOpen = function(row1, column1, row2, column2) {
    var cell1, cell2, combination, _i, _len, _ref;
    cell1 = util.rowColumnToCell(row1, column1);
    cell2 = util.rowColumnToCell(row2, column2);
    _ref = data.combinations;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      combination = _ref[_i];
      if ((cell1 === combination[0] || cell1 === combination[2]) && (cell2 === combination[2] || cell2 === combination[0]) && this.cellIsOpen(combination[1])) {
        return true;
      }
    }
    return false;
  };

  Game.prototype.spaceIsOpen = function(row, column) {
    return this.cellIsOpen(util.rowColumnToCell(row, column));
  };

  Game.prototype.spacesLeft = function() {
    var cell, count, _i, _len, _ref;
    count = 0;
    _ref = this.cells;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      cell = _ref[_i];
      if (cell === false) {
        count++;
      }
    }
    return count;
  };

  Game.prototype.userClicked = function(e) {
    var column, elem, row, _ref;
    if (this.isOver) {
      return;
    }
    elem = $(e.target);
    _ref = util.getRowColumn(elem), row = _ref[0], column = _ref[1];
    this.player.takeTurn(row, column);
    return this.ai.takeTurn();
  };

  return Game;

})();
$(document).ready(function() {
  $('#tictactoereset').on('click', function() {
    return startGame();
  });
  $('.tictactoe td').on('click', function(e) {
    return data.game.userClicked(e);
  });
});