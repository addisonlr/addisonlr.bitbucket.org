/*global $, window, console, alert*/
function zipCode(zip) {
    'use strict';
    var contents = [],
        i,
        inputLine,
        url = 'http://www.uszip.com/zip/' + zip;
    console.log('zip code: ' + zip);
    
    $.get(url, function (data) {
        contents = data.split(/\n/);
        //console.log(contents);
        for (i = 0; i < 16; i += 1) {
            inputLine = contents[i];
            if (inputLine.startsWith('<title>')) {
                inputLine = $(inputLine).text().replace(' zip code', '').replace(',', '').trim();
                break;
            }
        }
        
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        if (inputLine !== '') {
            $('#output').html('<span class="larger">' + zip + '</span> - <span class="titled" title="Zone Improvement Plan">ZIP</span> code<hr> &#8259; ' + inputLine + '\nSource: <a target="_blank" href="' + url + '">' + url + '</a>');
        } else {
            $('#output').html('<span class="larger">No ZIP code found</span> - Error Message<hr>The answer to this question must have just zipped right by because it couldn\'t be found.');
        }
    }).error(function () {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">No ZIP code found</span> - Error Message<hr>The answer to this question must have just zipped right by because it couldn\'t be found.');
    });
}

function areaCode(code) {
    'use strict';
    console.log('Area Code: ' + code);
    
    $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
    $('#output').html('<span class="larger">Feature Temporarily Unavailable</span> - Error Message<hr>This feature is temporarily unavailable due to technical reasons');
    /*var contents = [],
        i,
        inputLine,
        url = 'http://www.allareacodes.com/' + code;
    
    $.get(url, function (data) {
        contents = data.split(/\n/);
        console.log(contents);
        for (i = 0; i < 101; i += 1) {
            break;
        }
    });*/
}