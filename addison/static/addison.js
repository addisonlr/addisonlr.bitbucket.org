var tictactoe;
var image;
var answer;
jQuery.prototype.center = function(offsetW, offsetH) {
    if (offsetW == null) {
        offsetW = 0;
    }
    if (offsetH == null) {
        offsetH = 0;
    }
    this.css({
        'position': 'absolute',
        'top': Math.max(0, (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop()) + offsetH,
        'left': Math.max(0, (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft()) + offsetW
    });
    return this;
};

var hideOverlay = function() {
    $('#overlay').hide();
    $('#overlayContent').hide();
    setTimeout(function() {
        $('#inputQuestion').focus();
    });
};

var showOverlay = function() {
    $('#overlay').show();
    $('#overlayContent').show().center();
};

var showFeatures = function() {
    $('#featureList').show();
};

var hideFeatures = function() {
    $('#featureList').hide();
    document.getElementById('inputQuestion').focus();
};

$(document).ready(function() {
    tictactoe = $('#tictactoe');
    image = $('#blank');
    answer = $('#answer');
    $('#featureList').hide();
    $('#answer').hide();
    $('#loading').hide();
    $('#2dgraph').hide();
    $('#pgraph').hide();
    $('#learning').hide();
    $('#fractal').hide();
    $('#inputQuestion').focus();
    $('#inputQuestion').on('keydown', function(e) {
        if (e.keyCode == 13) {
            var self = $(this);
            sendQuestion(self.val());
        }
    });

    $('#openOverlay').on('click', showOverlay);
    $('#overlay').on('click', hideOverlay);
    $('#x').on('click', hideOverlay);
    $('#features').on('click', showFeatures);
    $('html').on('keypress', hideFeatures);
    $('#clear').on('click', doFirst);
    $('#check').on('click', setUp);

    $(window).on('keydown', function(e) {
        var code = e.keyCode;
        if (code == 27) {
            hideOverlay();
        }
    });
});


var sendQuestion = function(question) {
    $.ajax({
        type: 'GET',
        url: location.href + 'question/' + question,
        beforeSend: function() {
            $('#map').hide();
            $('#answer').hide();
            $('#blank').hide();
            $('#source').hide();
            $('#loading').show();
            $('#loading').css('left', ($(window).width() / 2) - 50);
            $('#loading').css('top', ($(window).height() / 2) - 50);
            $('#loading').css('-webkit-animation', 'load 4s 5');
            $('#loading').css('animation', 'load 4s 5');
        },
        complete: function() {
            $('#loading').hide();
        }
    }).done(function(result) {
        if (result == 'tic-tac-toe') {
            $('#fractal').hide();
            $('#2dgraph').hide();
            $('#learning').hide();
            tictactoe.show();
            image.hide();
            answer.hide();
            startGame();
        } else if (result.substring(0, 9) == 'speechsyn') {
            var path = 'static/phonemes/joined.wav';
            var timestamp = '?t=' + (+ new Date);
            answer.show().text(result.replace('speechsyn', ''));
            $('#spoken').attr('src', path + timestamp).get(0).play();
        } else if (result.substring(0, 8) == 'function') {
            $('#learning').hide();
            if (result.substring(0, 14) == 'function graph') {
                setUp();
                $('#fractal').hide();
                $('#2dgraph').show();
                $('#pgraph').hide();
                $("<script id='testScript'/>").text(result + 'ctx.stroke();ctx.translate(-200, -200);\n}').appendTo('head');
                graph();
            } else {
                pSetUp();
                $('#fractal').hide();
                $('#pgraph').show();
                $('#2dgraph').hide();
                $("<script id='testScript'/>").text(result + 'ctx.stroke();ctx.translate(-200, -200);\n}').appendTo('head');
                pgraph();
            }
        } else if (result.substring(0, 7) == 'fractal') {
            $('#fractal').show();
            fSetUp();
            result = result.substring(7);
            var colors = JSON.parse(result);
            fgraph(colors);
        } else if (result == 'recognize') {
            $('#fractal').hide();
            $('#learning').show();
            doFirst();
        } else if (result.substring(0, 8) == 'location') {
            $('#fractal').hide();
            $('#learning').hide();
            $('#2dgraph').hide();
            $('#pgraph').hide();
            $('#map').show();
            var split = result.split(' ');
            map(split[1]);
        } else if (result == 'user-location') {
            $('#fractal').hide();
            $('#learning').hide();
            $('#2dgraph').hide();
            $('#pgraph').hide();
            $('#map').show();
            $('answer').hide();
            getLocation();
        } else if (result.substring(0, 9) == 'distance:') {
            $('#fractal').hide();
            $('#learning').hide();
            $('#2dgraph').hide();
            $('#pgraph').hide();
            $('#answer').show();
            $('#map').show();
            result = result.replace('distance:', '');
            var locations = result.split('&');
            var loc1 = locations[0];
            var loc2 = locations[1];
            getDistance(loc1, loc2);
        } else if (result.substring(0, 4) == 'http') {
            $('#fractal').hide();
            $('#learning').hide();
            $('#tictactoe').hide();
            $('#2dgraph').hide();
            $('#pgraph').hide();
            var split = result.split(/\n/);
            image.attr('src', split[0]);
            image.show();
            answer.show();
            result = result.replace(split[0] + '\n', '');
            answer.css('display', 'inline-block').text(result.substring(0, result.indexOf('Source: ')));
            $('#source').show();
            $('#source').attr({
                href: result.substring(result.indexOf('Source: ') + 8),
                target: '_blank'
            });
            $('#source').text(result.substring(result.indexOf('Source: ')));
            tictactoe.hide();
        } else {
            $('#fractal').hide();
            $('#learning').hide();
            $('#2dgraph').hide();
            $('#pgraph').hide();
            image.hide();
            tictactoe.hide();
            if (result.indexOf('Source: ') == -1) {
                //answer.sanswerhow();
                answer.css('display', 'inline-block').text(result);
            } else {
                answer.show();
                answer.css('display', 'inline-block').text(result.substring(0, result.indexOf('Source: ')));
                $('#source').show();
                $('#source').attr({
                    href: result.substring(result.indexOf('Source: ') + 8)
                });
                $('#source').show();
                $('#source').text(result.substring(result.indexOf('Source: ')));//.clone(true).appendTo(answer);
            }
        }
    });
}

function map(location) {
    document.getElementById('map').innerHTML = "<img src='http://maps.googleapis.com/maps/api/staticmap?center=" + location + "&size=400x300&markers=color:blue|" + location + "&sensor=false'>";
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    }
}

function showPosition(position) {
    var latlong = position.coords.latitude + ',' + position.coords.longitude;
    var img_url = 'http://maps.googleapis.com/maps/api/staticmap?center=' + latlong + '&visual_refresh=true&zoom=14&markers=color:blue|' + latlong + '&size=400x300&sensor=true';
    document.getElementById('map').innerHTML = "<img src='"+img_url+"'>";
}

function showError(error) {
    switch(error.code) {
    case error.PERMISSION_DENIED:
        //$('#answer').text(Request Denied);
        break;
    case error.POSITION_UNAVAILABLE:
        //answer.text(Location unavailable);
        break;
    case error.TIMEOUT:
        //answer.text(Request timed out);
        break;
    case error.UNKNOWN_ERROR:
        //answer.text(unknown error);
        break;
    }
}

function getDistance(firstPoint, secondPoint) {
    geocoder = new google.maps.Geocoder();
    var latlng1;
    var latlng2;
    if (firstPoint == 'current') {
        navigator.geolocation.getCurrentPosition(
            function(position) {
                latlng1 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                geocoder.geocode( { 'address': secondPoint}, function( results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        latlng2 = results[0].geometry.location;
                        var distance = google.maps.geometry.spherical.computeDistanceBetween(latlng1, latlng2);
                        distance = distance * 0.000621371;
                        distance = Math.round((distance)*100)/100;
                        document.getElementById('answer').innerHTML = 'You are <span style="font-weight:400;">' + distance + '</span> miles from ' + secondPoint.toTitleCase() + '.';
                        document.getElementById('map').innerHTML = "<img src='http://maps.googleapis.com/maps/api/staticmap?center=" + latlng1 + '|' + latlng2 + "&size=400x300&visual_refresh=true&path=color:0x0000ff|weight:5|" + latlng1 + "|" + latlng2 + "&markers=color:blue|" + latlng1 + '|' + latlng2 + "&sensor=true'>";
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            },
            function() {
                alert("geolocation not supported.");
            }
        );
    } else {
        geocoder.geocode( { 'address': firstPoint}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                latlng1 = results[0].geometry.location;
                geocoder.geocode( { 'address': secondPoint}, function( results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        latlng2 = results[0].geometry.location;
                        var distance = google.maps.geometry.spherical.computeDistanceBetween(latlng1, latlng2);
                        distance = distance * 0.000621371;
                        distance = Math.round((distance)*100)/100;
                        document.getElementById('answer').innerHTML = firstPoint.toTitleCase() + ' is <span style="font-weight:400;">' + distance + '</span> miles from ' + secondPoint.toTitleCase() + '.';
                        document.getElementById('map').innerHTML = "<img src='http://maps.googleapis.com/maps/api/staticmap?center=" + latlng1 + '|' + latlng2 + "&size=400x300&visual_refresh=true&path=color:0x0000ff|weight:5|" + latlng1 + "|" + latlng2 + "&markers=color:blue|" + latlng1 + '|' + latlng2 + "&sensor=true'>";
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
}

String.prototype.toTitleCase = function () {
    var A = this.split(' '), B = [];
    for (var i = 0; A[i] !== undefined; i++) {
            B[B.length] = A[i].substr(0, 1).toUpperCase() + A[i].substr(1);
    }
    return B.join(' ');
}
window.toTitleCase = function(str) {
    return "".toTitleCase.call(str);
}

function setUp() {
    var c = document.getElementById('2dgraph');
    var ctx = c.getContext('2d');
    ctx.fillStyle='#FFF';
    ctx.fillRect(0, 0, 399, 399);
    ctx.beginPath();
    ctx.moveTo(200, 0);
    ctx.lineTo(200, 399);
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(0, 200);
    ctx.lineTo(399, 200);
    ctx.stroke();
    ctx.translate(200, 200);
    for (var x = -200; x < 200; x = x + 10) {
        ctx.beginPath();
        ctx.moveTo(x, 200);
        ctx.lineTo(x, -200);
        ctx.strokeStyle = '#AAA';
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(x, 2);
        ctx.lineTo(x, -2);
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        ctx.font = '10px Arial';
        ctx.fillStyle = 'black';
        if (x % 20 == 0) {
            ctx.fillText(x/10, x, 0);
        }
    }
    for (var y = -200; y < 200; y = y + 10) {
        ctx.beginPath();
        ctx.moveTo(200, y);
        ctx.lineTo(-200, y);
        ctx.strokeStyle = '#AAA';
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(-2, y);
        ctx.lineTo(2, y);
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        ctx.font = '10px Arial';
        ctx.fillStyle = 'black';
        if (y % 20 == 0) {
            ctx.fillText(-1*(y/10), 0, y);
        }
    }
    ctx.strokeStyle='#444';
}

function pSetUp() {
    var c = document.getElementById('pgraph');
    var ctx = c.getContext('2d');
    ctx.fillStyle='#FFF';
    ctx.fillRect(0, 0, 399, 399);
    ctx.translate(200, 200);
    ctx.beginPath();
    for (var i = 10; i <= 200; i += 10) {
        ctx.arc(0, 0, i, 0, 2 * Math.PI);
    }
    ctx.moveTo(0, 200);
    ctx.lineTo(0, -200);
    ctx.moveTo(-200, 0);
    ctx.lineTo(200, 0);
    ctx.strokeStyle = '#AAA';
    ctx.stroke();
    ctx.font="10px Arial";
    ctx.fillStyle = 'black';
    for (var x = -200; x < 200; x += 20) {        
        ctx.fillText(x/20, x, 0);
    }
    for (var y = -200; y < 200; y += 20) {
        ctx.fillText(y/20, 0, -y);
    }
    ctx.strokeStyle='#444';
}

function fSetUp() {
    var c = document.getElementById('fractal');
    var ctx = c.getContext('2d');
    ctx.fillStyle='#FFF';
    ctx.fillRect(0, 0, 350, 200);
    ctx.stroke;
}

function fgraph(colors) {
    var c = document.getElementById('fractal');
    var ctx = c.getContext('2d');
    var imageData = ctx.getImageData(0, 0, 350, 200);
    var data = imageData.data;
    
    for (var i = 0; i < colors.length; i++) {
        data[i*4] = colors[i];
        data[i*4+1] = colors[i];
        data[i*4+2] = colors[i];
    }
    ctx.putImageData(imageData, 0, 0);
}