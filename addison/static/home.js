"use strict";
/*global $, document, window, location, alert, event, navigator, google, startGame, history, clearInterval*/
var ypts;
var showing = false;
var showing1 = false;
var pshowing = false;
var typesShowing = false;
var ticShowing = false;
var graphShowing = false;
var pongShowing = false;
var listening = false;
var split, startpt, examples, ex, source, path, timestamp, latLong, locations, geocoder, pt1, pt2, latlng1, latlng2, distance, i, x, y, ctx, weather, transcript, recognition, listener, speak, amount, clock;

//makes first letter of every word uppercase
String.prototype.toTitleCase = function () {
    var A = this.split(' '), B = [];
    for (i = 0; A[i] !== undefined; i += 1) {
        B[B.length] = A[i].substr(0, 1).toUpperCase() + A[i].substr(1);
    }
    return B.join(' ');
};

//checks if string contains a specific substring
String.prototype.contains = function (it) {
    return this.indexOf(it) !== -1;
};

//checks if string starts with specific substring
if (typeof String.prototype.startsWith !== 'function') {
    String.prototype.startsWith = function (it) {
        return this.slice(0, it.length) === it;
    };
}

//checks if string ends with specific substring
if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function (it) {
        return this.indexOf(it, this.length - it.length) !== -1;
    };
}

//checks if any index in arr is a substring of str
if (typeof String.prototype.any !== 'function') {
    String.prototype.any = function (str, arr) {
        if (arr.some(function (v) { return str.indexOf(v) >= 0; })) {
            return true;
        }
    };
}

window.toTitleCase = function (str) {
    return "".toTitleCase.call(str);
};

var replaceState = function (value) {
    history.pushState(null, null, location.origin + '/query/' + value);
};

var generateMapsQuery = function (center) {
    var query = "http://maps.googleapis.com/maps/api/staticmap?center=" + center + "&size=" + Math.floor($('#output').width() / 2) + "x200&scale=2&markers=color:blue|" + center + "&visual_refresh=true&sensor=false";
    return query;
};

var showHelpBox = function () {
    $('#helpinfobox').show();
    $('#helpinfobox').css('border-top', 'none');
    $('#help').css('border-bottom', '#8DCBE6 solid 1px').css('background-color', '#8DCBE6');
};
var hideHelpBox = function () {
    $('#helpinfobox').hide();
    $('#helpinfobox').css('border-top', '#ABADB3 solid 1px');
    $('#help').css('border-bottom', '#ABADB3 solid 1px').css('background-color', 'white');
};

var showDefEx = function () {
    ex = Math.floor((Math.random() * 11));
    $('#def').text(examples.definitions[ex]);
};
var hideDefEx = function () {
    $('#def').html('Definitions');
};
var showPeriodicEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#pertab').text(examples.pertab[ex]);
};
var hidePeriodicEx = function () {
    $('#pertab').html('Periodic Table (W.I.P.)');
};
var showEtyEx = function () {
    ex = Math.floor(Math.random() * 5);
    $('#ety').text(examples.etymology[ex]);
};
var hideEtyEx = function () {
    $('#ety').html('Etymology');
};
var showAntEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#ant').text(examples.antonyms[ex]);
};
var hideAntEx = function () {
    $('#ant').html('Antonyms & Synonyms');
};
var showRhymeEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#rhyme').text(examples.rhymes[ex]);
};
var hideRhymeEx = function () {
    $('#rhyme').html('Rhymes');
};
var showTransEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#trans').text(examples.translations[ex]);
};
var hideTransEx = function () {
    $('#trans').html('Single Word Translations');
};
var showAbbrEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#abbr').text(examples.abbrs[ex]);
};
var hideAbbrEx = function () {
    $('#abbr').html('Abbrs. & Acronyms');
};
var showRevAbbrEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#revabbr').text(examples.reverse[ex]);
};
var hideRevAbbrEx = function () {
    $('#revabbr').html('Reverse Abbreviations and Acronyms');
};
var showDiffEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#diff').text(examples.difference[ex]);
};
var hideDiffEx = function () {
    $('#diff').html('Difference Between (BETA)');
};
var showPhraseEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#phrase').text(examples.phrases[ex]);
};
var hidePhraseEx = function () {
    $('#phrase').html('Colloquial Phrases');
};
var showSumEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#sum').text(examples.summaries[ex]);
};
var hideSumEx = function () {
    $('#sum').html('Summaries & Information');
};
var showPicEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#pic').text(examples.pictures[ex]);
};
var hidePicEx = function () {
    $('#pic').html('Pictures');
};
var showTicEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#tic').text(examples.tic[ex]);
};
var hideTicEx = function () {
    $('#tic').html('Tic-Tac-Toe');
};
var showZipEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#zip').text(examples.zip[ex]);
};
var hideZipEx = function () {
    $('#zip').html('Cities in Zip/Area Codes');
};
var showStockEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#stock').text(examples.stock[ex]);
};
var hideStockEx = function () {
    $('#stock').html('Stock Information');
};
var showMappingEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#mapping').text(examples.location[ex]);
};
var hideMappingEx = function () {
    $('#mapping').html('Map & Location Features');
};
var showCalcEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#calc').text(examples.calculator[ex]);
};
var hideCalcEx = function () {
    $('#calc').html('Calculator');
};
var showGraphEx = function () {
    ex = Math.floor((Math.random() * 5));
    $('#graphex').text(examples.graphing[ex]);
};
var hideGraphEx = function () {
    $('#graphex').html('Graphing Calculator');
};
var showPongEx = function () {
    ex = Math.floor((Math.random() * 4));
    $('#pong').text(examples.pong[ex]);
};
var hidePongEx = function () {
    $('#pong').html('Pong');
};
var showConvertEx = function () {
    ex = Math.floor((Math.random() * 6));
    $('#convert').text(examples.convert[ex]);
};
var hideConvertEx = function () {
    $('#convert').html('Unit Conversions');
};

function map(location) {
    $('#output').css('left', '100%').show().animate({left: ($(window).width() / 5) - 24});
    showing = true;
    $('#output').html("<img id='map' src='" + generateMapsQuery(location) + "' />");
}

function showPosition(position) {
    latLong = position.coords.latitude + ',' + position.coords.longitude;
    $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
    $('#output').html("<img id='map' src='" + generateMapsQuery(latLong) + "' />");
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, function () { $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24}); $('#output').text('You could not be located'); });
    }
}

function getDistance(pt1, pt2) {
    geocoder = new google.maps.Geocoder();
    if (pt1 === 'current') {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                latlng1 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                geocoder.geocode({ 'address': pt2}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        latlng2 = results[0].geometry.location;
                        distance = Math.round((google.maps.geometry.spherical.computeDistanceBetween(latlng1, latlng2)) * 0.0621371) / 100;
                        var latlng = latlng1 + '|' + latlng2;
                        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                        $('#output').html("You are <span style='font-weight:400;'>" + distance + "</span> miles from " + pt2.toTitleCase() + ".<img id='image' src='" + generateMapsQuery(latlng) + "'>");
                    } else {
                        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                        $('#output').text('Geocode was not successful for the following reason: ' + status);
                    }
                });
            },
            function () {
                $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                $('#output').text("geolocation not supported.");
            }
        );
    } else {
        geocoder.geocode({ 'address': pt1}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                latlng1 = results[0].geometry.location;
                geocoder.geocode({ 'address': pt2}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        latlng2 = results[0].geometry.location;
                        var latlng = latlng1 + '|' + latlng2;
                        distance = Math.round((google.maps.geometry.spherical.computeDistanceBetween(latlng1, latlng2)) * 0.0621371) / 100;
                        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                        $('#output').html(pt1.toTitleCase() + " is <span style='font-weight:400;'>" + distance + "</span> miles from " + pt2.toTitleCase() + ".<img id='image' src='" + generateMapsQuery(latlng) + "'>");
                    } else {
                        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                        $('#output').text('Geocode was not successful for the following reason: ' + status);
                    }
                });
            } else {
                $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                $('#output').text('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
}

function showTime() {
    var d = new Date(), weekday = new Array(7), month = new Array(11);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";

            if (d.getHours() > 5 && d.getHours() < 16) {
                $('#output').css('background-image', '-webkit-gradient(linear,left top,left bottom,color-stop(0.36, #00BFFF),color-stop(1, #87CEFA),color-stop(1, #87CFEB)');
            } else if (d.getHours() === 16) {
                $('#output').css('background-image', '-webkit-gradient(linear,left bottom,left top,color-stop(0, #FFA600),color-stop(0.18, #FFD900),color-stop(1, #87CEFA)');
            } else {
                $('#output').css({'background-image': '-webkit-gradient(linear,left top,left bottom,color-stop(0, #000080),color-stop(0.67, #191970),color-stop(1, #00008B)', 'color': 'white'});
            }
            $('#output').html('<div id="hours">' + d.getHours() + '<span class="minute">:' + d.getMinutes() + '</span><span class="daymonthday"><br><span id="day">' + weekday[d.getDay()] + '</span><br><span>' + month[d.getMonth()] + ' <span id="date" class="larger">' + d.getDate() + '</span></span></div>');
}

function setUp() {
    ctx = document.getElementById('graph').getContext('2d');
    ctx.fillStyle = '#FFF';
    ctx.fillRect(0, 0, 400, 400);
    ctx.strokeRect(0, 0, 400, 400);
    ctx.beginPath();
    ctx.moveTo(200, 0);
    ctx.lineTo(200, 400);
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(0, 200);
    ctx.lineTo(400, 200);
    ctx.stroke();
    ctx.translate(200, 200);
    for (x = -200; x < 200; x = x + 10) {
        ctx.beginPath();
        ctx.moveTo(x, 200);
        ctx.lineTo(x, -200);
        ctx.strokeStyle = '#AAA';
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(200, x);
        ctx.lineTo(-200, x);
        ctx.strokeStyle = '#AAA';
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(x, 2);
        ctx.lineTo(x, -2);
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(2, x);
        ctx.lineTo(-2, x);
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        ctx.font = '10px Arial';
        ctx.fillStyle = 'black';
        if (x % 20 === -10 || x % 20 === 10) {
            ctx.fillText(x / 10, x, 0);
            ctx.fillText(-1 * (x / 10), 0, x);
        }
    }
    ctx.strokeStyle = '#444';
}

function pSetUp() {
    ctx = document.getElementById('graph').getContext('2d');
    ctx.fillStyle = '#FFF';
    ctx.fillRect(0, 0, 400, 400);
    ctx.strokeRect(0, 0, 400, 400);
    ctx.translate(200, 200);
    ctx.beginPath();
    for (i = 10; i <= 300; i += 10) {
        ctx.arc(0, 0, i, 0, 2 * Math.PI);
    }
    ctx.moveTo(0, 200);
    ctx.lineTo(0, -200);
    ctx.moveTo(-200, 0);
    ctx.lineTo(200, 0);
    ctx.strokeStyle = '#AAA';
    ctx.stroke();
    ctx.font = "10px Arial";
    ctx.fillStyle = 'black';
    for (x = -200; x < 200; x += 20) {
        ctx.fillText(x / 20, x, 0);
    }
    for (y = -200; y < 200; y += 20) {
        ctx.fillText(y / 20, 0, -y);
    }
    ctx.strokeStyle = '#444';
}

function unplot() {
    var ctx2 = document.getElementById('graphchair').getContext('2d');
    ctx2.clearRect(0, 0, 400, 400);
    $('#graphinfo').text('');
}

function plot() {
    var xpos = event.layerX, ypos = ypts[xpos] * -10, ctx2 = document.getElementById('graphchair').getContext('2d');
    $("#graphinfo").text('The Y position at ' + ((0.1 * xpos) - 20).toFixed(2) + ' is ' + (ypos / -10));
    ctx2.clearRect(0, 0, 400, 400);
    ctx2.beginPath();
    ctx2.moveTo(xpos, ypos + 193);
    ctx2.lineTo(xpos, ypos + 207);
    ctx2.stroke();
    ctx2.beginPath();
    ctx2.moveTo(xpos - 7, ypos + 200);
    ctx2.lineTo(xpos + 7, ypos + 200);
    ctx2.stroke();
}

var sendQuestion = function (question) {

    //animations when submitting a question that clears screen for new content (sliding animation)
    $('#output').animate({left: -2500}).hide();
    $('#output1').animate({left: -2500});
    $('#periodic').hide();
    $('#element').hide();
    $('#qtcont').animate({left: -2500}).hide();
    $('#tictactoe').animate({left: -2500}).hide();
    $('#graph').animate({left: -2500}).hide();
    $('#pong1').hide();
    $('#pong2').hide();
    clearInterval(pongTimer);

    window.clearInterval(clock);
    $('.ribbon').animate({left: -1000}, function () { $('.ribbon').hide(); });
    $('#loading').show().css('left', '100%');
    $('#loading').animate({left: ($(window).width() / 2.5) - 20});
    $('#mainui').animate({top: "25px"});
    $('#logo').css('position', 'absolute');
    $('#logo').animate({left: "25px", fontSize: "48px"});
    $('#logo').animate({top: "#25px"});

    $('#output').html('');
    main(question); //function in main.js
    $('#loading').animate({left: -500}, function () { $('#loading').hide(); });
    //$('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});

    /*$.ajax({
        type: 'GET',
        url: location.origin + '/question/' + question,
        beforeSend: function () {
        },
        complete: function () {
            $('#loading').animate({left: -500}, function () { $('#loading').hide(); });
        }
    }).done(function (result) {
        if (result === 'questiontypes') {
            $('#qtcont').show().css('left', '2000px');
            $('#qtcont').animate({left: ($(window).width() / 5) - 23});
            typesShowing = true;
        } else if (result === 'tootsie') {
            showing = true;
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            $('#output').html('<span class="larger">3</span> - Licks to get to center of a Tootsie Pop<hr><video autoplay><source src="/static/tootsiePop.mp4" type="video/mp4"></video><div class="quote">Boy: "How many licks does it take to get to the tootsie roll center of a tootsie pop?"</div>');
            setTimeout(function(){$('.quote').text('Owl: "A good question."'); setTimeout(function(){$('.quote').text('Owl: "Let\'s find out."'); setTimeout(function(){$('.quote').text('Owl: "One"'); setTimeout(function(){$('.quote').text('Owl: "Two"'); setTimeout(function(){$('.quote').text('Owl: "Three." (Crunch)'); setTimeout(function(){$('.quote').text('Owl: "Three."');},1000);},2000);},800);},1100);},2400);},4500);
        } else if (result === 'time') {
            showing = true;
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            showTime();
            clock = window.setInterval(function(){showTime();}, 1000);
        } else if (result === 'date') {
            showing = true;
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            var d = new Date();
            var month = new Array(11);
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            $('#output').html('<span class="larger">' + d.getDate() + '</span> - Date<hr>Today is the <span class="weighted">' + d.getDate() + '<sup>th</sup> of ' + month[d.getMonth()] + '</span>');
        } else if (result === 'day') {
            showing = true;
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            var d = new Date();
            var weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            $('#output').html('<span class="larger">' + weekday[d.getDay()] + '</span> - Day<hr>Today is <span class="weighted">' + weekday[d.getDay()] + '</span>');
        } else if (result === 'tic-tac-toe') {
            $('#tictactoe').show().css('left', '2000px');
            $('#tictactoe').animate({left: ($(window).width() / 2) - 150});
            ticShowing = true;
            startGame();
        } else if (result === 'pong') {
            initialize_pong();
            pongShowing = true;
        } else if (result.substring(0, 9) === 'speechsyn') {
            path = '/static/phonemes/joined.wav';
            timestamp = '?t=' + (+new Date());
            showing = true;
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            $('#output').text(result.replace('speechsyn', ''));
            document.getElementById('spoken').setAttribute('src', path + timestamp);
            document.getElementById('spoken').play();
        } else if (result.substring(0, 8) === 'location') {
            showing = true;
            split = result.split(' ');
            map(split[1]);
        } else if (result === 'user-location') {
            $('#output').css('left', '100%').show().animate({left: ($(window).width() / 5) - 24});
            showing = true;
            getLocation();
        } else if (result.substring(0, 9) === 'distance:') {
            $('#output').css('left', '100%');
            $('#output').show().animate({left: ($(window).width() / 5) - 24});
            showing = true;
            result = result.replace('distance:', '');
            locations = result.split('&');
            getDistance(locations[0], locations[1]);
        } else if (result === 'periodictable') {
            pshowing = true;
            $('#periodic').show();
        } else if (result.substring(0, 9) === 'graphrect') {
            $('#output').html('<canvas id="graph" width="400" height="400"></canvas><canvas onmousemove="plot()" onmouseout="unplot()" id="graphchair" width="400" height="400"></canvas><div id="graphinfo">Please mouse over the graph to see the Y value associated with a particular x value.</div><div id="yint"></div><div id="xint"></div>');
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            showing = true;
            setUp();
            result = JSON.parse(result.substring(9, result.length));
            ypts = result;
            var prev;
            ctx = document.getElementById('graph').getContext('2d');
            startpt = true;
            for (i = 0; i < result.length; i += 1) {
                if ((prev < 0 && 0 < result[i]) || (result[i] < 0 && 0 < prev)) {
                    //$('#xint').text('The X-Intercept is between ' + (((i - 1) * 0.1) - 20).toFixed(2) + ' & ' + ((i * 0.1) - 20).toFixed(2));
                }
                if (result[i] === 'undefined' || i === result.length - 1) {
                    ctx.stroke();
                    startpt = false;
                } else if (startpt) {
                    ctx.moveTo(-200, parseFloat(result[i], 10) * -10);
                    startpt = false;
                    prev = result[i];
                } else {
                    ctx.lineTo(i - 200, parseFloat(result[i], 10) * -10);
                    prev = result[i];
                }

                if (result[i] === 0) {
                    $('#xint').text('X-Intercept: x = ' + ((0.1 * i) - 20));
                }
            }
            $('#yint').text('Y-Intercept: ' + result[200]);
            ctx.translate(-200, -200);
        } else if (result.substring(0, 7) === 'graph3d') {
            result = JSON.parse(result.substring(8, result.length));
        } else if (result.substring(0, 8) === 'graphpol') {
            $('#output').html('<canvas id="graph" width="400" height="400"></canvas>').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            showing = true;
            pSetUp();
            result = JSON.parse(result.substring(8, result.length));
            startpt = true;
            ctx = document.getElementById('graph').getContext('2d');
            for (i = 0; i < result.length; i += 2) {
                if (result[i] === 'undefined' || i === result.length - 2) {
                    ctx.stroke();
                    startpt = false;
                } else if (startpt) {
                    ctx.beginPath();
                    ctx.moveTo(parseFloat(result[i]), parseFloat(result[i + 1]));
                    startpt = false;
                } else {
                    ctx.lineTo(parseFloat(result[i]), parseFloat(result[i + 1]));
                }
            }
            ctx.translate(-200, -200);
        } else if (result.substring(0, 7) === 'weather') {
            if (result === 'weather') {
                alert('weather');
            } else {
                split = result.split('-');
                $.getJSON('http://api.openweathermap.org/data/2.5/weather?q=' + split[1] + '&units=imperial&callback=?', function (data) {
                    window.weather = data;
                    $('#output').show().css('left', '100%').animate({
                        left: ($(window).width() / 5) - 24
                    }).html('<span class="larger">' + weather.name + '</span> - Weather<hr>&ensp;<span class="larger">&bull;</span>&ensp;Current Tempurature: ' + weather.main.temp + '&deg;F<br />&ensp;<span class="larger">&bull;</span>&ensp;High: ' + weather.main.temp_max + '&deg;F<br />&ensp;<span class="larger">&bull;</span>&ensp;Low: ' + weather.main.temp_min + '&deg;F<br />&ensp;<span class="larger">&bull;</span>&ensp;Humidity: ' + weather.main.humidity + '%');
                });
                showing = true;
            }
        } else if (result.substring(0, 4) === 'http') {
            if (speak === true) {
                path = '/static/phonemes/joined.wav';
                timestamp = '?t=' + (+new Date());
                $('#spoken').attr('src', path + timestamp).get(0).play();
                speak = false;
            }
            $('#output').show().css('left', '100%');
            $('#output').animate({left: ($(window).width() / 5) - 24});
            showing = true;
            split = result.split(/\n/);
            source = result.substring(result.indexOf('Source: ') + 8);
            result = result.replace(split[0] + '\n', '');
            result = result.substring(0, result.indexOf('Source:'));
            $('#output').html('<img id="image" src="' + split[0] + '" />' + result + '<br /><a href="' + source + '" target="_blank">Source: ' + source + '</a>');
        } else {
            /*if (speak === true) {
                path = '/static/phonemes/joined.wav';
                timestamp = '?t=' + (+new Date());
                $('#spoken').attr('src', path + timestamp).get(0).play();
                speak = false;
            }*//*
            if (result.indexOf('there is another') >= 0) {
                $('.output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                showing = true;
                showing1 = true;
                split = result.split('there is another');

                if (split[0].indexOf('Source: ') === -1) {
                    $('#output1').html(split[0]);
                } else {
                    $('#output1').html(split[0].substring(0, split[0].indexOf('Source: ')) + '<br /><a href="' + split[0].substring(split[0].indexOf('Source: ') + 8) + '" target="_blank">' + split[0].substring(split[0].indexOf('Source: ')) + '</a>');
                }
                if (split[1].indexOf('Source: ') === -1) {
                    $('#output').html(split[1]);
                } else {
                    $('#output').html(split[1].substring(0, split[1].indexOf('Source: ')) + '<br /><a href="' + split[1].substring(split[1].indexOf('Source: ') + 8) + '" target="_blank">' + split[1].substring(split[1].indexOf('Source: ')) + '</a>');
                }
            } else {
                $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                showing = true;
                $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                showing = true;
                if (result.indexOf('Source: ') === -1) {
                    $('#output').html(result);
                } else {
                    $('#output').html(result.substring(0, result.indexOf('Source: ')) + '<br /><a href="' + result.substring(result.indexOf('Source: ') + 8) + '" target="_blank">' + result.substring(result.indexOf('Source: ')) + '</a>');
                }
            }
        }
    });*/
};

$.fn.absPosCell = function () {
    var $el;
    return this.each(function () {
        $el = $(this);
        var newDiv = $("<div />", {
            "class": "innerWrapper",
            "css"  : {
                "height"  : $el.height(),
                "width"   : "100%",
                "position": "relative"
            }
        });
        $el.wrapInner(newDiv);
    });
};

$(document).ready(function () {
    $('#hydrogen').click(hydrogen);
    $('#helium').click(helium);
    $('#lithium').click(lithium);
    $('#beryllium').click(beryllium);
    $('#boron').click(boron);
    $('#carbon').click(carbon);
    $('#nitrogen').click(nitrogen);
    $('#oxygen').click(oxygen);
    $('#fluorine').click(fluorine);
    $('#neon').click(neon);
    $('#sodium').click(sodium);
    $('#magnesium').click(magnesium);
    $('#aluminium').click(aluminium);
    $('#silicon').click(silicon);
    $('#phosphorus').click(phosphorus);
    $('#sulfur').click(sulfur);
    $('#chlorine').click(chlorine);
    $('#argon').click(argon);
    $('#potassium').click(potassium);
    $('#calcium').click(calcium);
    $('#scandium').click(scandium);
    $('#titanium').click(titanium);
    $('#vanadium').click(vanadium);
    $('#chromium').click(chromium);
    $('#manganese').click(manganese);
    $('#iron').click(iron);
    $('#cobalt').click(cobalt);
    $('#nickel').click(nickel);
    $('#copper').click(copper);
    $('#zinc').click(zinc);
    $('#gallium').click(gallium);
    $('#germanium').click(germanium);
    $('#arsenic').click(arsenic);
    $('#selenium').click(selenium);
    $('#bromine').click(bromine);
    $('#krypton').click(krypton);
    $('#rubidium').click(rubidium);
    $('#strontium').click(strontium);
    $('#yttrium').click(yttrium);
    $('#zirconium').click(zirconium);
    $('#niobium').click(niobium);
    $('#molybdenum').click(molybdenum);
    $('#technetium').click(technetium);
    $('#ruthenium').click(ruthenium);
    $('#rhodium').click(rhodium);
    $('#palladium').click(palladium);
    $('#silver').click(silver);
    $('#cadmium').click(cadmium);
    $('#indium').click(indium);
    $('#tin').click(tin);
    $('#antimony').click(antimony);
    $('#tellurium').click(tellurium);
    $('#iodine').click(iodine);
    $('#xenon').click(xenon);
    $('#caesium').click(caesium);
    $('#barium').click(barium);
    $('#hafnium').click(hafnium);
    $('#tantalum').click(tantalum);
    $('#tungsten').click(tungsten);
    $('#rhenium').click(rhenium);
    $('#osmium').click(osmium);
    $('#iridium').click(iridium);
    $('#platinum').click(platinum);
    $('#gold').click(gold);
    $('#mercury').click(mercury);
    $('#thallium').click(thallium);
    $('#lead').click(lead);
    $('#bismuth').click(bismuth);
    $('#polonium').click(polonium);
    $('#astatine').click(astatine);
    $('#radon').click(radon);
    $('#francium').click(francium);
    $('#radium').click(radium);
    $('#rutherfordium').click(rutherfordium);
    $('#dubnium').click(dubnium);
    $('#seaborgium').click(seaborgium);
    $('#bohrium').click(bohrium);
    $('#hassium').click(hassium);
    $('#meitnerium').click(meitnerium);

    $("td").absPosCell();

    var input = $('#input');
    $.getJSON('/static/examples.json', function (data) {
        examples = data;
    });

    $('#helpinfobox').hide();
    $('#qtcont').hide();
    $('td').height($(window).height() / 15);
    $('#loading').hide();
    $('.output').hide();
    $('#graph').hide();
    $('#tictactoe').hide();
    $('#periodic').hide();
    $('#element').hide();
    $('#blur').hide();
    $('#pong1').hide();
    $('#pong2').hide();
    $('#contRec').click(function () { recognition.start(); });

    $('#help').mouseenter(showHelpBox);
    $('#help').mouseleave(function (e) {
        if (e.relatedTarget.id !== 'helpinfobox') {
            hideHelpBox();
        } else {
            showHelpBox();
        }
    });
    $('#helpinfobox').mouseleave(hideHelpBox);
    $('#def').mouseenter(showDefEx).mouseleave(hideDefEx).click(function () { input.val(examples.definitions[ex]); });
    $('#pertab').mouseenter(showPeriodicEx).mouseleave(hidePeriodicEx).click(function () { input.val(examples.pertab[ex]); });
    $('#ety').mouseenter(showEtyEx).mouseleave(hideEtyEx).click(function () { input.val(examples.etymology[ex]); });
    $('#ant').mouseenter(showAntEx).mouseleave(hideAntEx).click(function () { input.val(examples.antonyms[ex]); });
    $('#rhyme').mouseenter(showRhymeEx).mouseleave(hideRhymeEx).click(function () { input.val(examples.rhymes[ex]); });
    $('#trans').mouseenter(showTransEx).mouseleave(hideTransEx).click(function () { input.val(examples.translations[ex]); });
    $('#abbr').mouseenter(showAbbrEx).mouseleave(hideAbbrEx).click(function () { input.val(examples.abbrs[ex]); });
    $('#revabbr').mouseenter(showRevAbbrEx).mouseleave(hideRevAbbrEx).click(function () { input.val(examples.reverse[ex]); });
    $('#diff').mouseenter(showDiffEx).mouseleave(hideDiffEx).click(function () { input.val(examples.difference[ex]); });
    $('#phrase').mouseenter(showPhraseEx).mouseleave(hidePhraseEx).click(function () { input.val(examples.phrases[ex]); });
    $('#sum').mouseenter(showSumEx).mouseleave(hideSumEx).click(function () { input.val(examples.summaries[ex]); });
    $('#pic').mouseenter(showPicEx).mouseleave(hidePicEx).click(function () { input.val(examples.pictures[ex]); });
    $('#tic').mouseenter(showTicEx).mouseleave(hideTicEx).click(function () { input.val(examples.tic[ex]); });
    $('#zip').mouseenter(showZipEx).mouseleave(hideZipEx).click(function () { input.val(examples.zip[ex]); });
    $('#stock').mouseenter(showStockEx).mouseleave(hideStockEx).click(function () { input.val(examples.stock[ex]); });
    $('#mapping').mouseenter(showMappingEx).mouseleave(hideMappingEx).click(function () { input.val(examples.location[ex]); });
    $('#calc').mouseenter(showCalcEx).mouseleave(hideCalcEx).click(function () { input.val(examples.calculator[ex]); });
    $('#graphex').mouseenter(showGraphEx).mouseleave(hideGraphEx).click(function () { input.val(examples.graphing[ex]); });
    $('#pong').mouseenter(showPongEx).mouseleave(hidePongEx).click(function () { input.val(examples.pong[ex]); });
    $('#convert').mouseenter(showConvertEx).mouseleave(hideConvertEx).click(function () { input.val(examples.convert[ex]); });

    $('#blur').click(function () { $('#element').hide(); $('#blur').hide(); $('#periodict').css('text-shadow', '');});

    input.focus();
    input.on('keydown', function (e) {
        if (e.keyCode === 13) {
            sendQuestion(this.value);
            replaceState(this.value);
        }
    });
    $('html').on('keypress', function () { input.focus(); });

    window.addEventListener('popstate', function(e) {
        if (window.location.pathname.search('query') == -1)
            return;
        var pathArr = window.location.pathname.split('/');
        var newQuery = decodeURI(pathArr[pathArr.length - 1]);
        sendQuestion(newQuery);
        input.val(newQuery);
    });
});
