//stored pattern data
var hexagon = new Array(6, 290, 0.9, 0.9, 'hexagon');
var pentagon = new Array(5, 70, 0.8, 0.8, 'pentagon');
var square = new Array(4, 90, 1.0, 1.0, 'square'); //square data
var triangle = new Array(3, 120, 1.0, 1.0, 'triangle'); //triangle data
var star5 = new Array(5, 140, 1.0, 1.0, 'five-point star'); //five point star data
var upa = new Array(3, 190, 0.9, 0.75, 'capital A'); //capital A
var onen = new Array(2, 220, 1.0, 0.6, 'one');
var twon = new Array(2, 90, 1.0, 0.7, 'two');
var fourn = new Array(3, 180, 0.75, 0.8, 'four');
var fourc = new Array(3, 140, 0.95, 0.55, 'closed four');
var container = new Array(hexagon, pentagon, square, triangle, star5, upa, onen, twon, fourn, fourc);

var worker = new Worker('static/speed.js'); //calculates mouse speed
var timer; //polls speed at regular interval
var drawing; //true if currently drawing
var speed; //most recent speed
var oldspeed; //previous speed
var x; //current x position
var y; //current y position
var x2; //previous x position
var y2; //previous y position
//classification variables
var sides; //number of drawn sides
var xpoints = new Array(); //x points where cursor has stopped
var ypoints = new Array(); //y points where cursor has stopped
var angles = new Array(); //angles between lines
var sideLength = new Array(); //length of sides
var avgAngle = 0; //average angle
var angleUnif; //uniformity of angle measure
var lenUnit; //uniformity of side length

function doFirst() { //sets up drawing area
    drawing = false;
    avgAngle = xpoints.length = ypoints.length = angles.length = sides = 0;
    document.getElementById('sides').innerHTML = '';
    document.getElementById('ang').innerHTML = '';
    document.getElementById('aangles').innerHTML = '';
    document.getElementById('uangles').innerHTML = '';
    document.getElementById('usides').innerHTML = '';
    document.getElementById('result').innerHTML = 'Please draw something and remember to pause at all points.  Upon clicking check, the program will attempt to find a match.  To improve chances of recognition, verify the corrects number of sides using the chart to the bottom left.  Currently supported objects include: a square, a triangle, a five-point star, and the capital letter A.';
    var canvas = document.getElementById('recognizer');
    ctx = canvas.getContext('2d');
    ctx.fillStyle = 'FFF';
    ctx.fillRect(0, 0, 300, 300);
    ctx.fill()
    canvas.addEventListener('mousedown', start, false); //starts drawing on canvas
    canvas.addEventListener('mousemove', draw, false); //draws on current position
    canvas.addEventListener('mouseup', stop, false); //stops painting
}

function start(e) { //initializes graphics and records features of drawing
    xpoints[0] = x = x2 = e.layerX;
    ypoints[0] = y = y2 = e.layerY;
    ctx.fillStyle = '000';
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineWidth = 5;
    ctx.lineCap = 'round';
    drawing = true;
    timer = setInterval(getSpeed, 75); //gets current mouse speed every 100 ms while drawing
}

function draw(e) { //draws on canvas
    if (drawing) {
        x2 = x;
        y2 = y;
        x = e.layerX;
        y = e.layerY;
        ctx.lineTo(x, y);
        ctx.stroke();
    }
}

function stop(e) { //stop drawing
    drawing = false;
    clearInterval(timer);
    document.getElementById('sides').innerHTML = sides;
}

function setUp() { //makes current data valuable to learning algorithm and classifyer
    for (var i = 1; i < xpoints.length - 1; i++) {
        var a1 = Math.atan2(ypoints[i-1] - ypoints[i], xpoints[i-1] - xpoints[i]);
        var a2 = Math.atan2(ypoints[i] - ypoints[i+1], xpoints[i] - xpoints[i+1]);
        var a3 = (a1 - a2) * (180/Math.PI);
        if (a3 < 0) {
            angles[i-1] = Math.round((a3 + 360) * 100) / 100;
        } else {
            angles[i-1] = Math.round(a3 * 100) / 100;
        }
    }
    document.getElementById('ang').innerHTML = angles;
    for (var i = 0; i < angles.length; i++) {
        avgAngle += angles[i];
    }
    avgAngle = Math.round((avgAngle / angles.length) * 100) / 100;
    document.getElementById('aangles').innerHTML = avgAngle;
    
    angles.sort(function(a,b){return b-a});
    angleUnif = avgAngle / angles[0];
    document.getElementById('uangles').innerHTML = Math.round((angleUnif * 100) * 100) / 100 + '%';
    
    for (var i = 0; i < xpoints.length - 1; i++) {
        sideLength[i] = Math.sqrt(Math.pow(xpoints[i] - xpoints[i+1], 2) + Math.pow(ypoints[i] - ypoints[i+1], 2));
    }
    var lavg = 0;
    for (var i = 0; i < sideLength.length; i++) {
        lavg += sideLength[i];
    }
    sideLength.sort(function(a,b){return b-a});
    lenUnif = (lavg / sideLength.length) / sideLength[0];
    document.getElementById('usides').innerHTML = Math.round((lenUnif * 100) * 100) / 100 + '%';
    compare();
}

function compare() { //compares this data to stored data
    var chances = new Array();
    for (var i = 0; i < container.length; i++) { //loops through stored objects
        var one = Math.pow(container[i][0] - sides, 2);
        var two = Math.pow(container[i][1] - avgAngle, 2);
        var three = Math.pow(container[i][2] - angleUnif, 2);
        var four = Math.pow(container[i][3] - lenUnif, 2);
        var distance = Math.sqrt(one + two + three + four);
        chances.push(Math.abs(distance));
    }
    var i = chances.indexOf(Array.min(chances));
    var conf = (-0.04 * chances[i]) + 1;
    conf = Math.round((conf * 100) * 100) / 100;
    document.getElementById('result').innerHTML = 'You drew a ' + container[i][4] + '. Confidence: ' + Math.abs(conf) + '%';
}

worker.addEventListener('message', function(e) { //start worker monitoring
    console.log(e.data);
    if (e.data <= 0.04 && oldspeed > 0.05) { //if mouse has essentially stopped and just stopped rather than stayed stopped
        sides += 1; //one more side has been drawn
        xpoints[sides] = x;
        ypoints[sides] = y;
        ctx.fillStyle = 'black';
        ctx.font = '10px Arial';
        ctx.fillText('vertex ' + sides, x + 10, y + 10);
    }
    oldspeed = e.data; //sets old speed
}, false);

function getSpeed() { //gets current speed from worker
    worker.postMessage({'x2': x2, 'y2': y2, 'x': x, 'y': y}); //sends worker two point to calculate distance formula
}

function getDistance(point1x, point1y, point2x, point2y) {
    var xs = Math.pow(point2x - point1x, 2);
    var ys = Math.pow(point2y - point1y, 2);
    return Math.sqrt(xs + ys);
}

Array.min = function( array ){
    return Math.min.apply( Math, array );
};