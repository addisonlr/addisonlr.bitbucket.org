/*global $, console, alert, summarize, define, explain, expand, contract, rhyme, showTime, convert, areaCode, zipCode, calculate, etymologize, translate, antonyms, synonyms, elaborate*/

function whatWas(words, subject) {
    'use strict';
    summarize(subject);
}

function whatAre(words, subject) {
    'use strict';
    //TO-DO: implement stocks
    summarize(subject);
}

function whatDoes(word, subject) {
    'use strict';
    if (new RegExp('spiff (stand for|mean)').test(subject)) {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">SPIFF</span> - Acronym<hr>SPIFF stands for <span class="larger">S</span>emantic and <span class="larger">P</span>resentable <span class="larger">I</span>nformation <span class="larger">F</span>inder.');
        return;
    }
    if (new RegExp('^\\w+? mean').test(subject)) {
        define(word[2]);
        return;
    } else if (new RegExp('^(\\w+ ){2,}mean').test(subject)) {
        explain(subject.slice(0, subject.lastIndexOf(' ')));
    } else if (subject === 'mean') {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">What about mean?</span> - Sarcastic Retort<hr>Could that "question" have been more ambigous? <span class="weighted">You</span> really <span class="weighted">should be more specific</span>.');
        return;
    }
    
    if (new RegExp('[a-z]+? stand for').test(subject)) { //acronyms
        expand(word[2]);
    } else if (new RegExp('.*?stand for').test(subject)) {
        contract(subject.slice(0, subject.indexOf('stand for'))); //abbreviate
    }
    
    if (new RegExp('.+look like').test(subject)) { //images
        summarize(subject.slice(0, subject.indexOf(' look like')).replace(/(a |the )/, ''));
    }
    
    if (new RegExp('.+rhyme with').test(subject)) {
        rhyme(subject.slice(0, subject.indexOf('rhyme with')));
    }
    
    //TO-DO inline
}

function whatTime(word, sentence) {
    'use strict';
    var d = new Date(),
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    
    if (sentence.contains(' time ') && (sentence.contains(' it') || sentence.contains(' the '))) {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        showTime();
        return true;
    }
    if (sentence.contains(' is ') && sentence.contains(' date')) {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + d.getDate() + '</span> - Date<hr>Today is the <span class="weighted">' + d.getDate() + '<sup>th</sup> of ' + month[d.getMonth()] + '</span>');
        return true;
    }
    if (sentence.contains(' is ') && (sentence.contains(' day') || sentence.contains('today')) && !(new RegExp('.*\\d.*').test(sentence))) {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + weekday[d.getDay()] + '</span> - Day<hr>Today is <span class="weighted">' + weekday[d.getDay()] + '</span>.');
        return true;
    }
}

function whatIs(word, subject) {
    'use strict';
    var amount = subject.replace(/^(\d+\.\d+|\d+)( |)(\D+)( in | to )(\D+)/g, '$1').trim(),
        initial = subject.replace(/^(\d+\.\d+|\d+)( |)(\D+)( in | to )(\D+)/g, '$3').trim(),
        final = subject.replace(/^(\d+\.\d+|\d+)( |)(\D+)( in | to )(\D+)/g, '$5').trim(),
        mathKeyWords = ['+', '-', '*', '/', '^', 'sin(', 'cos(', 'tan(', 'csc(', 'sec(', 'cot(', 'sqrt(', 'abs(', '!', 'ln(', 'log('],
        weather,
        topic,
        detail;
    
    //conditions for conversions
    if (new RegExp('^(\\d+)( |)(\\D+)( in | to )(\\D+)').test(subject.replace('.', ''))) {
        convert(amount, initial, final, 'statement');
        return;
    }
    
    //TO-DO Implement suitable difference between replacement
    
    //conditions for area codes
    if (new RegExp('^(\\d{3})$').test(subject)) {
        areaCode(subject);
        return;
    }
    
    //conditions for zip codes
    if (new RegExp('^(\\d{5})$').test(subject)) {
        zipCode(subject);
        return;
    }
    
    //conditions for weather
    if (subject.startsWith('the ') && subject.contains(' weather')) {
        if (subject.contains(' in ')) {
            subject = subject.replace('the weather in ', '');
            subject = subject.replace(/ /g, '_');
            console.log('DEBUG: weather: \'' + subject + '\'');
            $.getJSON('http://api.openweathermap.org/data/2.5/weather?q=' + subject + '&units=imperial&callback=?', function (data) {
                weather = data;
                $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                $('#output').html('<span class="larger">' + weather.name + '</span> - Weather<hr><div class="weather"><div class="weathertop">' + weather.main.temp + '&#8457;</div><div class="weatherbottom">' + weather.main.temp_max + '&#8457;</div><div class="weatherbottom">' + weather.main.temp_min + '&#8457;</div><div>' + weather.main.humidity + '%</div></div>');
            });
            return;
        }
    }
    
    //conditions for calculator
    if (subject.any(subject, mathKeyWords)) {
        calculate(subject.trim(), false);
        return;
    }
    
    //TO-DO implement stock replacement
    
    //conditions for definitions
    if (subject.contains(' definition ') || subject.contains(' meaning ')) {
        define(subject.slice(subject.lastIndexOf(' ')));
        return;
    }
    
    //conditions for etymologies
    if (subject.contains(' etymology ') || subject.contains(' origin ')) {
        etymologize(subject.slice(subject.lastIndexOf(' ')));
        return;
    }
    
    //conditions for translations
    if (subject.contains(' in ')) {
        translate(word[2], subject.slice(subject.lastIndexOf(' ')));
        return;
    }
    
    //conditions for antonyms
    if (subject.startsWith('the opposite of ')) {
        antonyms(subject.slice(subject.lastIndexOf(' ')));
        return;
    }
    
    //conditions for synonyms
    if (new RegExp('^(the same as |another word for |another way of saying ).*').test(subject)) {
        synonyms(subject.slice(subject.lastIndexOf(' ')));
        return;
    }
    
    //conditions for abbrs and acronyms
    if (subject.endsWith(' short for')) {
        expand(word[2]);
        return;
    }
    
    //conditions for contractions
    if (subject.startsWith('short for ')) {
        contract(subject.replace('short for ', ''));
        return;
    }
    
    //conditions for contextual wikipedia query
    if (subject.contains(' in terms of ')) {
        subject = subject.replace(/( in terms of)(a | )/g, ' (') + ')';
        summarize(subject);
        return;
    }
    
    //conditions for elaborations
    if (new RegExp('.+\'s.*').test(subject)) {
        topic = subject.slice(0, subject.indexOf('\'')).trim();
        detail = subject.slice(subject.indexOf('\'') + 2).trim();
        elaborate(topic, detail, ' is ');
        return;
    }
    
    if (new RegExp('the .* of .*').test(subject)) {
        detail = subject.slice(subject.indexOf('the ') + 4, subject.indexOf(' of ')).replace(' ', '_').trim();
        topic = subject.slice(subject.indexOf(' of ') + 4).trim();
        elaborate(topic, detail);
        return;
    }
    
    //final attempt
    summarize(subject);
}