/*global $, window, console, getSummary*/
/*jslint regexp: true*/
function summarize(topic) {
    'use strict';
    topic = topic.replace(/%27/g, "'").replace('the ', '');
    var image = '',
        source = '',
        result,
        data;

    console.log('Wikipedia Summary: ' + topic);
    $.getJSON('http://en.wikipedia.org/w/api.php?action=query&format=json&redirects&rvsection=0&callback=?&inprop=url&', {titles: topic, prop: "revisions|images|info", rvprop: "content"}, function (data) {
        result = JSON.stringify(data);
        
        //removes \n's and double spaces
        result = result.replace(/\\n/g, '').replace(/ {2}/g, '');
        console.log(result);
        
        //gets article's image name
        image = result.slice(result.indexOf('|') + 1);
        image = image.slice(0, image.indexOf('"'));
        console.log('IMAGE:  ' + image);
        
        //if no image had been returned this tries to find one
        if ((image.contains('icon')) || ((!image.endsWith('jpeg') && !image.endsWith('jpg')) && !image.endsWith('png'))) {
            image = result.slice(result.indexOf('File:') + 5);
            image = image.slice(0, image.indexOf('|'));
            if (!image.endsWith('jpeg') && !image.endsWith('jpg')) {
                image = image.slice(0, image.indexOf('"'));
            }
            console.log('IMAGE2:  ' + image);
        }
        
        //get source of article
        source = result.slice(result.indexOf('fullurl') + 10);
        source = source.slice(0, source.indexOf('"'));
        console.log('SOURCE: ' + source);

        //removes data that is not for display
        result = result.replace('"*":"Unrecognized', '');
        result = result.slice(result.indexOf('\'\'\''), result.indexOf('"}],"im'));
        
        //removes groups of data that doesn't need to be displayed
        result = result.replace(/\(\{\{.*?\}\}\)/g, ''); //removes curly brackets in parentheses
        result = result.replace(/\{\{.*?\}\}/g, ''); //removes any remaining curly bracket groups
        console.log(result);
        result = result.replace(/\[\[[^\]]*?\|(.*?)\]\]/g, '$1');
        result = result.replace(/(\[\[)|(\]\])/g, ''); //removes only the [[ and ]] symbols
        result = result.replace(/<.*?\/.*?>/g, ''); //removes html stuff
        result = result.replace(/\'\'\'/g, '');
        result = result.replace(/\\"/g, '"');
        console.log(result);
        
        //gets images actual url to make it possible to display it
        if (image.endsWith('sv')) {
            image = image + 'g';
        }
        $.getJSON('http://en.wikipedia.org/w/api.php?action=query&format=json&titles=Image:' + image + '&&callback=?&prop=imageinfo&iiprop=url', function (imageURL) {
            image = JSON.stringify(imageURL);
            image = image.slice(image.indexOf('"url"') + 7);
            image = image.slice(0, image.indexOf('"'));
            $('#output').html('<span class="larger">' + topic.toTitleCase() + '</span> - Summary<hr><image id="image" src="' + image + '"/>' + result + '<br/><a href="' + source + '" target="_blank">' + source + '</a>');
        });

        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + topic.toTitleCase() + '</span> - Summary<hr><image id="image" src="' + image + '"/>' + result + '<br/><a href="' + source + '" target="_blank">' + source + '</a>');
    });
}

function elaborate(topic, detail, origin) {
    'use strict';
    var src = 'http://en.wikipedia.org/',
        result = '';
    
    console.log('Wikipedia Elaboration: Topic:' + topic + ' Detail:' + detail + ' Origin:' + origin);
    $.getJSON('http://en.wikipedia.org/w/api.php?action=query&format=json&redirects&rvsection=0&callback=?&inprop=url&', {titles: topic, prop: "revisions|info", rvprop: "content"}, function (data) {
        result = JSON.stringify(data);
        
        //removes \n's and double spaces
        result = result.replace(/\\n/g, '').replace(/ {2}/g, '');
        console.log(result);
        //result = result.replace(/.*?detail.*?\[\[(.*?)\]\].*?url\"\:\"(http.*?)\".*?/g, '$1 - $2');
        result = result.replace(new RegExp('.*?' + detail + '.*?\\[\\[(.*?)\\]\\].*?url\\"\\:\\"(http.*?)\\".*', 'g'), topic.toTitleCase() + '\'s ' + detail + ' ' + origin + ' <span class="weighted">$1</span>.<br/><a href="$2" target="_blank">Source: $2');
        console.log(result);
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + topic.toTitleCase() + '</span> - Elaboration<hr>' + result);
    });
}