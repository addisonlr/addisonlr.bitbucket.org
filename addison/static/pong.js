var bally = 190;
var ballx = 760
var dx = -1;
var dy = -1;
var playery = 160;
var lost = false;
var playing = false;
var pongTimer;
var lost = false;
var time = 0;

function redraw() {
    var ctx2 = document.getElementById('pong2').getContext('2d');
    
    ctx2.clearRect(0, 0, 800, 400);
    ctx2.fillStyle = '#32CD32';
    if (bally < 35) {
        ctx2.fillRect(20, 5, 10, 80);
    } else if (bally > 350) {
        ctx2.fillRect(20, 320, 10, 80);
    } else {
        ctx2.fillRect(20, (bally - 30), 10, 80); //opponent
    }
    ctx2.fillRect(780, playery, 10, 80); //player
    ctx2.fillStyle = '#317FCD';
    ctx2.fillRect(ballx, bally, 20, 20);
    ctx2.stroke();
    ctx2.fillStyle = '#CCC';
    ctx2.font = '16px Helvetica';
    ctx2.fillText(time + 's', 750, 30);
    ctx2.stroke();
}

function paddle_up() {
    if (playery > 10) {
        playery -= 10;
        redraw();
    }
}

function paddle_down() {
    if (playery < 320) {
        playery += 10;
        redraw();
    }
}

function erase_words() {
    var ctx = document.getElementById('pong1').getContext('2d');
    ctx.fillStyle = '#666';
    ctx.fillRect(0, 0, 800, 400);
    ctx.fillStyle = '#CCC';
    ctx.fillRect(395, 0, 2, 400);
    ctx.stroke();
}

function move() {
    time += .005;
    if (ballx < 31) {
        dx = Math.floor((Math.random()*2)+1);
        path = '/static/bounce.wav';
        timestamp = '?t=' + (+new Date());
        $('#spoken').attr('src', path + timestamp).get(0).play();
    } else if (ballx === 760) {
        if (bally > (playery - 10) && bally < (playery + 80)) {
            dx = Math.floor((Math.random()*-2)+1);
            path = '/static/bounce.wav';
            timestamp = '?t=' + (+new Date());
            $('#spoken').attr('src', path + timestamp).get(0).play();
        } else {
            playing = false;
            lost = true;
            clearInterval(pongTimer);
            initialize_pong();
        }
    }
    if (bally < 6) {
        dy = 1;
        path = '/static/bounce.wav';
        timestamp = '?t=' + (+new Date());
        $('#spoken').attr('src', path + timestamp).get(0).play();
    } else if (bally > 380) {
        dy = -1;
        path = '/static/bounce.wav';
        timestamp = '?t=' + (+new Date());
        $('#spoken').attr('src', path + timestamp).get(0).play();
    }
    ballx += dx;
    bally += dy;
    redraw();
}

function initialize_pong() {
    $('#pong1').show();
    $('#pong2').show();
    var ctx = document.getElementById('pong1').getContext('2d');
    var ctx2 = document.getElementById('pong2').getContext('2d');
    
    ctx.fillStyle = '#666';
    ctx.fillRect(0, 0, 800, 400);
    ctx.fillStyle = '#CCC';
    ctx.fillRect(395, 0, 2, 400);
    ctx.stroke();
    ctx.font = '32px Helvetica';
    ctx.fillText('PRESS SPACE', 285, 150);
    ctx.stroke();
    
    if (lost) {
        dx = -1;
        dy = -1;
        ballx = 760;
        bally = 190;
        playery = 160;
        path = '/static/gameover.wav';
        timestamp = '?t=' + (+new Date());
        $('#spoken').attr('src', path + timestamp).get(0).play();
        var ctx = document.getElementById('pong1').getContext('2d');
        ctx.font = '32px Helvetica';
        ctx.fillText('YOU LOST!', 285, 100);
        ctx.stroke();
        lost = false;
    }
    
    ctx2.fillStyle = '#32CD32';
    ctx2.fillRect(20, (bally - 30), 10, 80); //opponent
    ctx2.fillRect(780, playery, 10, 80); //player
    ctx2.fillStyle = '#317FCD';
    ctx2.fillRect(ballx, bally, 20, 20);
    ctx2.stroke();
    
    $('html').keydown(function (e) {
        if (!playing && e.keyCode === 32) {
            playing = true;
            erase_words();
            pongTimer = setInterval(function(){move();}, 5);
        }
        
        if (playing) {
        switch (e.which) {
            case 38:
                paddle_up();
                break;
            case 40:
                paddle_down();
                break;
            }
        }
    });
}

function pong_game() {
    alert('let the games begin');
}