/*global $, window, console, alert, startGame, initialize_pong, define, etymologize, convert, translate, antonyms, synonyms, expand, contract, rhyme, graph, calculate, areaCode, zipCode, summarize*/
function statementType(sentence) {
    'use strict';
    console.log('statement');
    
    var amount = sentence.replace(/^(\d+\.\d+|\d+)( |)(\D+)( in | to )(\D+)/g, '$1').trim(),
        initial = sentence.replace(/^(\d+\.\d+|\d+)( |)(\D+)( in | to )(\D+)/g, '$3').trim(),
        final = sentence.replace(/^(\d+\.\d+|\d+)( |)(\D+)( in | to )(\D+)/g, '$5').trim(),
        mathKeyWords = ['+', '-', '*', '/', '^', 'sin(', 'cos(', 'tan(', 'csc(', 'sec(', 'cot(', 'sqrt(', 'abs(', '!', 'ln(', 'log('];
    
    //conditions for tic tac toe
    if (sentence.contains('tic') && sentence.contains('tac') && sentence.contains('toe') && sentence.contains('play')) {
        $('#tictactoe').show().css('left', '2000px').animate({left: ($(window).width() / 2) - 150});
        startGame();
        return;
    }
    
    //conditions for pong
    if (sentence.contains('pong')) {
        initialize_pong();
        return;
    }
    
    //conditions for text to speech enginge
    if (sentence.startsWith('speak ') || sentence.startsWith('say ')) {
        alert('text to speech');
    }
    
    //conditions for definitions
    if (sentence.startsWith('defin')) {
        define(sentence.slice(sentence.lastIndexOf(' ') + 1));
        return;
    }
    
    //conditions for etymologies
    if (sentence.startsWith('etymolog') || sentence.startsWith('origin of ')) {
        etymologize(sentence.slice(sentence.lastIndexOf(' ') + 1));
        return;
    }
    
    //conditions for antonyms
    if ((sentence.startsWith('opposite') || sentence.startsWith('antonym')) && sentence.contains(' ')) {
        antonyms(sentence.slice(sentence.lastIndexOf(' ') + 1));
        return;
    }
    
    //conditions for synonyms
    if (sentence.startsWith('synonym') && sentence.contains(' ')) {
        synonyms(sentence.slice(sentence.lastIndexOf(' ') + 1));
        return;
    }
    
    //conditions for expanding abbreviations
    if (sentence.startsWith('expand') && sentence.contains(' ')) {
        expand(sentence.slice(sentence.indexOf(' ') + 1));
        return;
    }
    
    //conditions for contracting acronyms
    if (sentence.startsWith('contract') || sentence.startsWith('abbreviate')) {
        contract(sentence.slice(sentence.indexOf(' ') + 1));
        return;
    }
    
    //conditions for rhymes
    if (sentence.startsWith('rhyme') && sentence.contains(' ')) {
        rhyme(sentence.slice(sentence.lastIndexOf(' ') + 1));
        return;
    }
    
    //conditions for periodic table
    if (sentence.contains('periodic table')) {
        $('#periodic').show();
        return;
    }
    
    //conditions for conversions
    if (new RegExp('^(\\d+)( |)(\\D+)( in | to )(\\D+)').test(sentence.replace('.', ''))) {
        convert(amount, initial, final, 'statement');
        return;
    }
    
    //conditions for translation
    if (sentence.contains(' in ') && !sentence.contains(' terms of')) {
        translate(sentence.slice(0, sentence.indexOf(' ')), sentence.slice(sentence.lastIndexOf(' ') + 1));
        return;
    }
    
    //conditions for wikipedia image search
    if (sentence.startsWith('picture') || sentence.startsWith('image') || sentence.startsWith('show me') || sentence.startsWith('draw')) {
        summarize(sentence.slice(sentence.lastIndexOf(' ')));
        return;
    }
    
    //conditions for rectangular graphing
    if (sentence.startsWith('y=') || sentence.startsWith('y =') || sentence.startsWith('graph')) {
        graph(sentence);
        return;
    }
    
    //conditions for polar graphing
    if (sentence.startsWith('r=') || sentence.startsWith('r =')) {
        alert('statement polar graph');
    }
    
    //conditions for sequence recognition
    if (new RegExp('(^((-|)(\\d+?),){3,}(-|)(\\d+?)$)').test(sentence)) {
        alert('statement sequence recognition');
    }
    
    //conditions for calculator
    if (sentence.any(sentence, mathKeyWords)) {
        calculate(sentence.trim(), false);
        return;
    }
    
    //conditions for area codes
    if (new RegExp('^(\\d{3})$').test(sentence)) {
        areaCode(sentence);
        return;
    }
    
    //conditions for zip codes
    if (new RegExp('^(\\d{5})$').test(sentence)) {
        zipCode(sentence);
    }
    
    summarize(sentence);
    
    //IMPLEMENT error emails
    
    //Show error message because no result could be found
    $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
    $('#output').html('<span class="larger">No Available Answer</span> - Error Message<hr>The answer to that "question" is as hard to find as the location of Carmen Sandiego.');
}