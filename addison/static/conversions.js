/*global window, $, console, toMeter, toCelcius, toGram, toLiter, toDay, toDollar*/
var length = ['inch', 'inches', 'in', 'foot', 'feet', 'ft', 'yard', 'yards', 'yd', 'mile', 'miles', 'mi', 'millimeter', 'millimeters', 'mm', 'centimeter', 'centimeters', 'cm', 'meter', 'meters', 'm', 'kilometer', 'kilometers', 'km', 'astronomical unit', 'astronomical units', 'au'],
    inch = ['inch', 'inches', 'in'],
    foot = ['foot', 'feet', 'ft'],
    yard = ['yard', 'yards', 'yd'],
    mile = ['mile', 'miles', 'mi'],
    mm = ['millimeter', 'millimeters', 'ml'],
    cm = ['centimeter', 'centimeters', 'cm'],
    meter = ['meter', 'meters', 'm'],
    km = ['kilometer', 'kilometers', 'km'],
    au = ['astronomical unit', 'astronomical units', 'au'],
    temperature = ['fahrenheit', 'f', 'celcius', 'c', 'kelvin', 'k'],
    f = ['fahrenheit', 'f'],
    c = ['celcius', 'c'],
    k = ['kelvin', 'k'],
    weight = ['milligram', 'milligrams', 'mg', 'gram', 'grams', 'g', 'pound', 'pounds', 'lb', 'lbs', 'kilogram', 'kilograms', 'kg', 'ton', 'tons', 'troy', 'troy oz', 'troy ounce', 'troy ounces'],
    lbs = ['pound', 'pounds', 'lb', 'lbs'],
    mg = ['milligram', 'milligrams', 'mg'],
    gram = ['gram', 'g'],
    kg = ['kilogram', 'kilograms', 'kg'],
    ton = ['ton', 'tons'],
    troy = ['troy', 'troy oz', 'troy ounce', 'troy ounces'],
    volume = ['teaspoon', 'teaspoons', 'tsp', 'tablespoon', 'tablespoons', 'tbsp', 'oz', 'ounce', 'ounces', 'fl oz', 'cup', 'cups', 'pint', 'pints', 'pt', 'quart', 'quarts', 'qt', 'gallon', 'gallons', 'gal', 'millileter', 'millileters', 'ml', 'liter', 'liters', 'l'],
    tsp = ['teaspoon', 'teaspoons', 'tsp'],
    tbsp = ['tablespoon', 'tablespoons', 'tbsp'],
    oz = ['oz', 'ounce', 'ounces', 'fl oz'],
    cup = ['cup', 'cups'],
    pint = ['pint', 'pints', 'pt'],
    quart = ['quart', 'quarts', 'qt'],
    gal = ['gallon', 'gallons', 'gal'],
    ml = ['millileter', 'millileters', 'ml'],
    liter = ['liter', 'liters'],
    time = ['second', 'seconds', 's', 'millisecond', 'milliseconds', 'ms', 'minute', 'minutes', 'min', 'hour', 'hours', 'hr', 'day', 'days', 'dy', 'week', 'weeks', 'wk', 'month', 'months', 'year', 'years', 'yr', 'decade', 'decades', 'century', 'centuries'],
    sec = ['second', 'seconds', 's'],
    ms = ['millisecond', 'milliseconds', 'ms'],
    min = ['minute', 'minutes', 'min'],
    hour = ['hour', 'hours', 'hr'],
    day = ['day', 'days', 'dy'],
    week = ['week', 'weeks', 'wk'],
    month = ['month', 'months'],
    year = ['year', 'years', 'yr'],
    decade = ['decade', 'decades'],
    century = ['century', 'centuries'],
    currency = ['penny', 'pennies', 'cent', 'cents', 'nickel', 'nickels', 'dime', 'dimes', 'quarter', 'quarters', 'dollar', 'dollars', 'buck', 'bucks'],
    penny = ['penny', 'pennies', 'cent', 'cents'],
    nickel = ['nickel', 'nickels'],
    dime = ['dime', 'dimes'],
    quarter = ['quarter', 'quarters'],
    dollar = ['dollar', 'dollars', 'buck', 'bucks'],
    original;

function convert(amount, initial, final, source) {
    'use strict';
    console.log('Conversions: Amount:' + amount + ' Initial:' + initial + ' Final:' + final + ' Source:' + source);
    
    if (amount === '') {
        amount = '1';
    }
    original = parseFloat(amount);
    
    //checks for valid length conversion
    if ($.inArray(initial, length) !== -1 && $.inArray(final, length) !== -1) {
        amount = toMeter(amount, initial);
        
        if ($.inArray(final, inch) !== -1) {
            amount = (amount * 3.2808) * 12;
        } else if ($.inArray(final, foot) !== -1) {
            amount = amount * 3.2808;
        } else if ($.inArray(final, yard) !== -1) {
            amount = (amount * 3.2808) / 3;
        } else if ($.inArray(final, mile) !== -1) {
            amount = (amount * 3.2808) / 5280;
        } else if ($.inArray(final, mm) !== -1) {
            amount = amount * 1000;
        } else if ($.inArray(final, cm) !== -1) {
            amount = amount * 100;
        } else if ($.inArray(final, km) !== -1) {
            amount = amount / 1000;
        } else if ($.inArray(final, au) !== -1) {
            amount = amount / 149597870691;
        }
    } else if ($.inArray(initial, temperature) !== -1 && $.inArray(final, temperature) !== -1) {
        amount = toCelcius(amount, initial);
        
        if ($.inArray(final, f) !== -1) {
            amount = (amount * 1.8) + 32;
        } else if ($.inArray(final, k) !== -1) {
            amount = amount + 273;
        }
    } else if ($.inArray(initial, weight) !== -1 && $.inArray(final, weight) !== -1) {
        amount = toGram(amount, initial);
        
        if ($.inArray(final, lbs) !== -1) {
            amount = amount / 453.5924;
        } else if ($.inArray(final, mg) !== -1) {
            amount = amount * 1000;
        } else if ($.inArray(final, kg) !== -1) {
            amount = amount / 1000;
        } else if ($.inArray(final, ton) !== -1) {
            amount = amount / 1000000;
        } else if ($.inArray(final, troy) !== -1) {
            amount = amount * 0.032151;
        }
    } else if ($.inArray(initial, volume) !== -1 && $.inArray(final, volume) !== -1) {
        amount = toLiter(amount, initial);
        
        if ($.inArray(final, tsp) !== -1) {
            amount = (amount / 3.785) * 768;
        } else if ($.inArray(final, tbsp) !== -1) {
            amount = (amount / 3.785) * 256;
        } else if ($.inArray(final, oz) !== -1) {
            amount = (amount / 3.785) * 128;
        } else if ($.inArray(final, cup) !== -1) {
            amount = (amount / 3.785) * 16;
        } else if ($.inArray(final, pint) !== -1) {
            amount = (amount / 3.785) * 8;
        } else if ($.inArray(final, quart) !== -1) {
            amount = (amount / 3.785) * 4;
        } else if ($.inArray(final, gal) !== -1) {
            amount = amount / 3.785;
        } else if ($.inArray(final, ml) !== -1) {
            amount = amount * 1000;
        }
    } else if ($.inArray(initial, time) !== -1 && $.inArray(final, time) !== -1) {
        amount = toDay(amount, initial);
        
        if ($.inArray(final, ms) !== -1) {
            amount = amount * 86400000;
        } else if ($.inArray(final, sec) !== -1) {
            amount = amount * 86400;
        } else if ($.inArray(final, min) !== -1) {
            amount = amount * 1440;
        } else if ($.inArray(final, hour) !== -1) {
            amount = amount * 24;
        } else if ($.inArray(final, week) !== -1) {
            amount = amount / 7;
        } else if ($.inArray(final, month) !== -1) {
            amount = amount / 30;
        } else if ($.inArray(final, year) !== -1) {
            amount = amount / 365.242;
        } else if ($.inArray(final, decade) !== -1) {
            amount = amount / 3654.24;
        } else if ($.inArray(final, century) !== -1) {
            amount = amount / 36542.4;
        }
    } else if ($.inArray(initial, currency) !== -1 && $.inArray(final, currency) !== -1) {
        amount = toDollar(amount, initial);
        
        if ($.inArray(final, penny) !== -1) {
            amount = amount * 100;
        } else if ($.inArray(final, nickel) !== -1) {
            amount = amount * 20;
        } else if ($.inArray(final, dime) !== -1) {
            amount = amount * 10;
        } else if ($.inArray(final, quarter) !== -1) {
            amount = amount * 4;
        }
    } else {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + initial + ' and ' + final + ' aren\'t convertable</span> - Error Message<hr>Trying to answer that question is like comparing apples and oranges.');
        return;
    }
    
    if (amount.toString().contains('e+')) {
        amount = amount.toString().replace('e+', '&times;10<sup>') + '</sup>';
    }
    
    if (source === 'how') {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + amount.toString() + ' ' + final + '</span> - Unit Conversion<hr>There are <span class="weighted">' + amount.toString() + ' in a ' + final);
    } else {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + amount.toString() + ' ' + final + '</span> - Unit Conversion<hr>' + original + ' ' + initial + ' is ' + amount.toString() + ' ' + final);
    }
}

function toMeter(amount, initial) {
    'use strict';
    if ($.inArray(initial, inch) !== -1) {
        return (amount / 12) / 3.2808;
    } else if ($.inArray(initial, foot) !== -1) {
        return amount / 3.2808;
    } else if ($.inArray(initial, yard) !== -1) {
        return (amount * 3) / 3.2808;
    } else if ($.inArray(initial, mile) !== -1) {
        return (amount * 5280) / 3.2808;
    } else if ($.inArray(initial, mm) !== -1) {
        return amount / 1000;
    } else if ($.inArray(initial, cm) !== -1) {
        return amount / 100;
    } else if ($.inArray(initial, meter) !== -1) {
        return amount;
    } else if ($.inArray(initial, km) !== -1) {
        return amount * 1000;
    } else if ($.inArray(initial, au) !== -1) {
        return amount * 149597870691;
    }
}

function toCelcius(amount, initial) {
    'use strict';
    if ($.inArray(initial, f) !== -1) {
        return (amount - 32) / 1.8;
    } else if ($.inArray(initial, c) !== -1) {
        return amount;
    } else if ($.inArray(initial, k) !== -1) {
        return amount - 273;
    }
}

function toGram(amount, initial) {
    'use strict';
    if ($.inArray(initial, lbs) !== -1) {
        return amount * 453.5924;
    } else if ($.inArray(initial, mg) !== -1) {
        return amount / 1000;
    } else if ($.inArray(initial, gram) !== -1) {
        return amount;
    } else if ($.inArray(initial, kg) !== -1) {
        return amount * 1000;
    } else if ($.inArray(initial, ton) !== -1) {
        return amount * 1000000;
    } else if ($.inArray(initial, troy) !== -1) {
        return amount / 0.032151;
    }
}

function toLiter(amount, initial) {
    'use strict';
    if ($.inArray(initial, tsp) !== -1) {
        return (amount / 768) * 3.785;
    } else if ($.inArray(initial, tbsp) !== -1) {
        return (amount / 256) * 3.785;
    } else if ($.inArray(initial, oz) !== -1) {
        return (amount / 128) * 3.785;
    } else if ($.inArray(initial, cup) !== -1) {
        return (amount / 16) * 3.785;
    } else if ($.inArray(initial, pint) !== -1) {
        return (amount / 8) * 3.785;
    } else if ($.inArray(initial, quart) !== -1) {
        return (amount / 4) * 3.785;
    } else if ($.inArray(initial, gal) !== -1) {
        return amount * 3.785;
    } else if ($.inArray(initial, ml) !== -1) {
        return amount / 1000;
    } else if ($.inArray(initial, liter) !== -1) {
        return amount;
    }
}

function toDay(amount, initial) {
    'use strict';
    if ($.inArray(initial, sec) !== -1) {
        return amount / 86400;
    } else if ($.inArray(initial, ms) !== -1) {
        return amount / 86400000;
    } else if ($.inArray(initial, min) !== -1) {
        return amount / 1440;
    } else if ($.inArray(initial, hour) !== -1) {
        return amount / 24;
    } else if ($.inArray(initial, day) !== -1) {
        return amount;
    } else if ($.inArray(initial, week) !== -1) {
        return amount * 7;
    } else if ($.inArray(initial, month) !== -1) {
        return amount * 30;
    } else if ($.inArray(initial, year) !== -1) {
        return amount * 365.242;
    } else if ($.inArray(initial, decade) !== -1) {
        return amount * 3652.42;
    } else if ($.inArray(initial, century) !== -1) {
        return amount * 36524.2;
    }
}

function toDollar(amount, initial) {
    'use strict';
    if ($.inArray(initial, penny) !== -1) {
        return amount / 100;
    } else if ($.inArray(initial, nickel) !== -1) {
        return amount / 20;
    } else if ($.inArray(initial, dime) !== -1) {
        return amount / 10;
    } else if ($.inArray(initial, quarter) !== -1) {
        return amount / 4;
    } else if ($.inArray(initial, dollar) !== -1) {
        return amount;
    }
}