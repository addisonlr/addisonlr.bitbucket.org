/*global $, window, console*/
/*jslint regexp: true*/
function define(word) {
    'use strict';
    var result,
        speechPart = '';
    
    console.log('Wiktionary Definitions: \'' + word + '\'');
    $.getJSON('http://en.wiktionary.org/w/api.php?action=query&prop=revisions&format=json&rvprop=content&titles=' + word + '&redirects&callback=?', function (data) {
        result = JSON.stringify(data);
        
        //removes /n's, double spaces, and removes all characters before "*"
        result = result.replace(/\\n/g, '').replace(/ {2}/g, '');
        result = result.replace(/.*(===.*?===\{\{(head\||)en.*?)===.*/g, '$1');
        
        //gets part of speech
        speechPart = result.slice(3, result.lastIndexOf('==='));
        console.log('PART OF SPEECH: \'' + speechPart + '\'');
        
        //clean up result
        result = result.slice(result.lastIndexOf('===') + 3);
        result = result.replace(/\{\{[^\|]+\|[^\|]+\}\}/g, '');//gets rid of {'s that don't contains useful information
        result = result.replace(/(#: |)\{\{(usex|quote).*?#/g, '');
        result = result.replace(/\{\{[^\|]+\|/g, '(<span class="weighted">');
        result = result.replace(/\|[^\|]+\}\}/g, '</span>)');
        result = result.replace(/\[\[[^\|\]]+\|/g, '');
        result = result.replace(/\[|\]/g, '');
        result = result.replace(/#/g, '<br/> &#8259; ');
        result = result.replace('<br/>', '');
        result = result.replace('*', '<br/> &#8259; ');
        
        //displays result
        if (result.contains('}}')) { //if the call didn't return a real result
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            $('#output').html('<span class="larger">No Available Definition</span> - Error Message<hr>It seems the definition for that word couldn\'t be found.<br/>Did you check the <a href="http://phrontistery.info/ihlstart.html">International House of Logorrhea</a>?');
            return;
        }
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + word.toTitleCase() + '</span> - Definition<hr>' + result + '<br/><a href="http://en.wikipedia.org/">Source: www.wiktionary.org</a>');
    });
}

function etymologize(word) {
    'use strict';
    var result;
    
    console.log('Wiktionary Etymology: \'' + word + '\'');
    $.getJSON('http://en.wiktionary.org/w/api.php?action=query&prop=revisions&format=json&rvprop=content&titles=' + word + '&redirects&callback=?', function (data) {
        result = JSON.stringify(data);
        
        //removes /n's, double spaces, and removes all characters before "*", and deletes all text not in etymology section
        result = result.replace(/\\n/g, '').replace(/ {2}/g, '');
        result = result.slice(result.indexOf('Etymology===') + 12);
        result = result.slice(0, result.indexOf('==='));
        result = result.replace(/\(.*?\)/g, '');
        
        //replaces language codes with language name
        result = result.replace(/\{\{etyl\|grc\|.*?\}\}/g, '<span class="weighted">Ancient Greek</span>');
        result = result.replace(/\{\{etyl\|xno\|.*?\}\}/g, '<span class="weighted">Anglo-Norman</span>');
        result = result.replace(/\{\{etyl\|ar\|.*?\}\}/g, '<span class="weighted">Arabic</span>');
        result = result.replace(/\{\{etyl\|da\|.*?\}\}/g, '<span class="weighted">Danish</span>');
        result = result.replace(/\{\{etyl\|frk\|.*?\}\}/g, '<span class="weighted">Frankish</span>');
        result = result.replace(/\{\{etyl\|fr\|.*?\}\}/g, '<span class="weighted">French</span>');
        result = result.replace(/\{\{etyl\|de\|.*?\}\}/g, '<span class="weighted">German</span>');
        result = result.replace(/\{\{etyl\|got\|.*?\}\}/g, '<span class="weighted">Gothic</span>');
        result = result.replace(/\{\{etyl\|he\|.*?\}\}/g, '<span class="weighted">Hebrew</span>');
        result = result.replace(/\{\{etyl\|hi\|.*?\}\}/g, '<span class="weighted">Hindi</span>');
        result = result.replace(/\{\{etyl\|is\|.*?\}\}/g, '<span class="weighted">Icelandic</span>');
        result = result.replace(/\{\{etyl\|it\|.*?\}\}/g, '<span class="weighted">Italian</span>');
        result = result.replace(/\{\{etyl\|LL\.\|.*?\}\}/g, '<span class="weighted">Late Latin</span>');
        result = result.replace(/\{\{etyl\|la\|.*?\}\}/g, '<span class="weighted">Latin</span>');
        result = result.replace(/\{\{etyl\|ML\.\|.*?\}\}/g, '<span class="weighted">Medieval Latin</span>');
        result = result.replace(/\{\{etyl\|enm\|.*?\}\}/g, '<span class="weighted">Middle English</span>');
        result = result.replace(/\{\{etyl\|gmh\|.*?\}\}/g, '<span class="weighted">Middle High German</span>');
        result = result.replace(/\{\{etyl\|frr\|.*?\}\}/g, '<span class="weighted">North Frisian</span>');
        result = result.replace(/\{\{etyl\|no\|.*?\}\}/g, '<span class="weighted">Norwegian</span>');
        result = result.replace(/\{\{etyl\|ang\|.*?\}\}/g, '<span class="weighted">Old English</span>');
        result = result.replace(/\{\{etyl\|fro\|.*?\}\}/g, '<span class="weighted">Old French</span>');
        result = result.replace(/\{\{etyl\|ofs\|.*?\}\}/g, '<span class="weighted">Old Frisian</span>');
        result = result.replace(/\{\{etyl\|goh\|.*?\}\}/g, '<span class="weighted">Old High German</span>');
        result = result.replace(/\{\{etyl\|osx\|.*?\}\}/g, '<span class="weighted">Old Saxon</span>');
        result = result.replace(/\{\{etyl\|sa\|.*?\}\}/g, '<span class="weighted">Sanskrit</span>');
        result = result.replace(/\{\{etyl\|sco\|.*?\}\}/g, '<span class="weighted">Scots</span>');
        result = result.replace(/\{\{etyl\|es\|.*?\}\}/g, '<span class="weighted">Spanish</span>');
        result = result.replace(/\{\{etyl\|sv\|.*?\}\}/g, '<span class="weighted">Swedish</span>');
        result = result.replace(/\{\{etyl\|txb\|.*?\}\}/g, '<span class="weighted">Tocharian B</span>');
        result = result.replace(/\{\{etyl\|VL\.\|.*?\}\}/g, '<span class="weighted">Vulgar Latin</span>');
        result = result.replace(/\{\{etyl\|fy\|.*?\}\}/g, '<span class="weighted">West Frisian</span>');
        result = result.replace(/\{\{etyl\|showPongEx\|.*?\}\}/g, '<span class="weighted">Polish</span>');
        result = result.replace(/\{\{etyl\|gem-pro\|.*?\}\}/g, '<span class="weighted">Proto-Germanic</span>');
        result = result.replace(/\{\{etyl\|ine-pro\|.*?\}\}/g, '<span class="weighted">Proto-Indo-European</span>');
        result = result.replace(/\{\{etyl\|yi\|.*?\}\}/g, '<span class="weighted">Yiddish</span>');
        
        //cleans up and edits result
        console.log(result);
        result = result.replace(/\{\{[^\|]+\|[^\|]+\}\}/g, '');//gets rid of {'s that don't contains useful information
        //result = result.replace(/\{\{term\|/g, '');
        result = result.replace(/(\|)lang=.*?\}\}/g, '$1');
        result = result.replace(/\|\|(.*?)(\||\}\})/g, '<span class="italic"> ($1)</span>');
        result = result.replace(/\/t\|[^\|]+/g, '');
        result = result.replace(/\{\{term\|/g, '');
        result = result.replace(/\||\{\{term|\}\}|\*/g, '');
        result = result.replace(/tr=/g, '').replace(/sc=polytonic/g, '');
        
        //stylize result
        result = result.replace(/from/ig, '&rarr;').replace('&rarr;', '');
        result = result.replace(/, &rarr/g, ' &rarr');
        result = result.replace(/\.(| )(Cog)/, '<br/>$2').replace(/> </g, '><');
        //result = result.replace(/(<span class="weighted">.*?<\/span>)(.*?)( &rarr;)/g, '$1 <div class="outlined">$2</div>$3 ');
        result = result.replace(/\[\[|\]\]/g, '').replace(/"/g, '');
        result = result.slice(0, result.indexOf('Related to'));
        
        //display result
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + word.toTitleCase() + '</span> - Etymology<hr>' + result + '<br/><a href="http://en.wiktionary.org/">Source: www.wikipedia.org</a>');
        if (result.contains('Unrecognized parameter')) { //if the call didn't return a real result
            $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
            $('#output').html('<span class="larger">No Available Etymology</span> - Error Message<hr>It seems the etymology for that word couldn\'t be found. Did you check your spleling?');
            return;
        }
    });
}

function translate(word, language) {
    'use strict';
    var result,
        helpText = '';
    
    console.log('Wiktionary Translations: \'' + word + '\'; \'' + language + '\'');
    $.getJSON('http://en.wiktionary.org/w/api.php?action=query&prop=revisions&format=json&rvprop=content&titles=' + word + '&redirects&callback=?', function (data) {
        result = JSON.stringify(data);
        
        //removes /n's, double spaces, and removes all characters before "*"
        result = result.replace(/\\n/g, '').replace(/ {2}/g, '');
        result = result.slice(result.indexOf('===Translations===') + 18);
        result = result.slice(result.indexOf('{'));
        if (result.contains('==')) {
            result = result.slice(0, result.indexOf('=='));
        }
        
        //cleans up result
        console.log('DEBUGGING DATA:' + result);
        //result = rsult.replace(/\{\{trans-top\|(.*?)\}\}.*?Spanish: .*?\{\{[^}]+\|([^\}]*?)\}\}(,|\*).*?\{\{trans-bottom\}\}/g, '$1$2<br/>');
        result = result.replace(new RegExp('{{trans-top\\|(.*?)}}.*?' + language.toTitleCase() + ': .*?{{[^}]+\\|([^}]*?)}}(,|\\*).*?{{trans-bottom}}', 'g'), ' &bull; in terms $1 - $2<br/>');
        if (result.contains('checktrans')) {
            result = result.slice(0, result.indexOf('{{checktrans'));
        }
        if (result.contains('tr=')) {
            result = result.slice(0, result.lastIndexOf('<br/>')).replace(/tr=/g, 'transliteration: ');
            helpText = '<div class="titledHelp" title="Transliterations are used to ease your pronunciation of the results.">?</div>';
        }
        
        //displays result
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        if (result.contains('}}')) { //if the call didn't return a real result
            $('#output').html('<span class="larger">No Translation Could be Found</span> - Error Message<hr>Did vous check your spelling?');
            return;
        }
        $('#output').html(helpText + '<span class="larger">' + word.toTitleCase() + '</span> in <span class="larger">' + language.toTitleCase() + '</span> - Translation<hr>' + result + '<br/><a href="http://en.wikipedia.org/">Source: www.wiktionary.org</a>');
    });
}

function antonyms(word) {
    'use strict';
    var url = 'http://www.synonyms.net/antonyms/' + word,
        contents,
        i,
        inputLine;
    
    console.log('Antonyms: \'' + word + '\'');
    $.get(url, function (data) {
        contents = data.split(/\n/);
        for (i = 0; i < 150; i += 1) {
            inputLine = contents[i];
            if (inputLine.trim().startsWith('<div class="rc5">')) {
                break;
            }
        }
        inputLine = inputLine.slice(inputLine.indexOf('Antonyms:') + 9);
        inputLine = inputLine.slice(0, inputLine.indexOf('</p>'));
        inputLine = inputLine.replace(/<.*?>/g, '');
        
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        if (inputLine !== '' && !inputLine.contains('Princeton\'s Word')) {
            $('#output').html('<span class="larger">' + word.toTitleCase() + '</span> - Antonyms<hr> &bull; ' + inputLine + '\nSource: <a target="_blank" href="' + url + '">' + url + '</a>');
        } else {
            $('#output').html('<span class="larger">No antonyms could be found</span> - Error Message<hr>A question could be lost. Did you ignore your typos?');
        }
    }).error(function () {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">No antonyms could be found</span> - Error Message<hr>A question could be lost. Did you ignore your typos?');
    });
}

function synonyms(word) {
    'use strict';
    var url = 'http://www.synonyms.net/antonyms/' + word,
        contents,
        i,
        inputLine;
    
    console.log('Antonyms: \'' + word + '\'');
    $.get(url, function (data) {
        contents = data.split(/\n/);
        for (i = 0; i < 150; i += 1) {
            inputLine = contents[i];
            if (inputLine.trim().startsWith('<div class="rc5">')) {
                break;
            }
        }
        inputLine = inputLine.slice(inputLine.indexOf('Synonyms:') + 9);
        inputLine = inputLine.slice(0, inputLine.indexOf('</p>'));
        inputLine = inputLine.replace(/<.*?>/g, '');
        
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        if (inputLine !== '' && !inputLine.contains('rc5')) {
            $('#output').html('<span class="larger">' + word + '</span> - Synonyms<hr> &bull; ' + inputLine + '\nSource: <a target="_blank" href="' + url + '">' + url + '</a>');
        } else {
            $('#output').html('<span class="larger">No synonyms could be found</span> - Error Message<hr>Apologies and regrets.  No synonyms could be found or located.');
        }
    }).error(function () {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">No synonyms could be found</span> - Error Message<hr>Apologies and regrets.  No synonyms could be found or located.');
    });
}

function expand(abbr) {
    'use strict';
    var url = 'http://www.abbreviations.com/' + abbr,
        contents,
        i,
        inputLine,
        abbrList;
    
    console.log('Expand: \'' + abbr + '\'');
    $.get(url, function (data) {
        contents = data.split(/\n/);
        for (i = 110; i < 150; i += 1) {
            inputLine = contents[i];
            if (inputLine.toLowerCase().contains(abbr)) {
                break;
            }
        }
        inputLine = inputLine.replace(/<a.*?\/a>/g, '');
        inputLine = inputLine.replace(/<.*?>/g, '');
        inputLine = inputLine.replace(/ &raquo; &nbsp;&nbsp;&nbsp;/g, '<br/>');
        abbrList = inputLine.split('<br/>');
        inputLine = abbrList.filter(function (elem, pos) {
            return abbrList.indexOf(elem) === pos;
        }).join('<br/> &bull; ');
        
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        if (inputLine.trim() !== '' && !inputLine.contains('Discuss these')) {
            $('#output').html('<span class="larger">' + abbr.toUpperCase() + '</span> - Acronyms<hr>' + abbr.toUpperCase() + ' can stand for:<br/> &bull; ' + inputLine.slice(0, inputLine.lastIndexOf('<br/>')) + '\nSource: <a target="_blank" href="' + url + '">' + url + '</a>');
        } else {
            $('#output').html('<span class="larger">No acronyms could be found</span> - Error Message');
        }
    }).error(function () {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">No acronyms could be found</span> - Error Message');
    });
}

function contract(phrase) {
    'use strict';
    var url = 'http://www.abbreviations.com/abbreviation/' + phrase,
        contents,
        i,
        inputLine,
        abbrList;
    
    console.log('Abbreviate: \'' + phrase + '\'');
    $.get(url, function (data) {
        contents = data.split(/\n/);
        for (i = 110; i < 150; i += 1) {
            inputLine = contents[i];
            if (inputLine.toLowerCase().trim().contains(phrase)) {
                break;
            }
        }
        
        inputLine = inputLine.slice(0, inputLine.indexOf('</p>'));
        console.log(inputLine);
        inputLine = inputLine.replace(/<.*?>/g, '');
        inputLine = ' &bull; ' + inputLine.replace(phrase.toTitleCase(), '');
        
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        if (inputLine.trim() !== '' && !inputLine.contains('Discuss these')) {
            $('#output').html('<span class="larger">' + phrase.toTitleCase() + '</span> - Abbreviation<hr>' + inputLine + '\nSource: <a target="_blank" href="' + url + '">' + url + '</a>');
        } else {
            $('#output').html('<span class="larger">No abbreviations could be found</span> - Error Message<hr>Don\'t u h8 it when u can\'t find things?');
        }
    }).error(function () {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">No abbreviations could be found</span> - Error Message<hr>Don\'t u h8 it when u can\'t find things?');
    });
}

function explain(phrase) {
    'use strict';
    var url = 'http://www.phrases.net/psearch/' + phrase.replace(/ /g, '%20'),
        contents,
        i,
        inputLine;
    
    console.log('Phrase: \'' + phrase + '\'');
    $.get(url, function (data) {
        contents = data.split(/\n/);
        for (i = 0; i < 150; i += 1) {
            inputLine = contents[i];
            if (inputLine.trim().startsWith('<table class="tdata">')) {
                break;
            } else if (i === 119) {
                $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
                $('#output').html('<span class="larger">No explanation could be found</span> - Error Message<hr>Well, this search certainly messed up.');
            }
        }
        inputLine = inputLine.slice(0, inputLine.indexOf('</a></td>')).trim().toLowerCase();
        console.log(inputLine);
        inputLine = inputLine.replace(/<\/td>/g, '\n &bull; ');
        inputLine = inputLine.replace(/<.*?>/g, '');
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">' + phrase.toTitleCase() + '</span> - Phrase<hr> &bull; ' + inputLine + '\nSource: <a target="_blank" href="' + url + '">' + url + '</a>');
    }).error(function () {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">No explanation could be found</span> - Error Message<hr>Well, this search certainly messed up.');
    });
}

function rhyme(word) {
    'use strict';
    var url = 'http://www.rhymes.net/rhyme/' + word,
        contents,
        i,
        inputLine;
    
    console.log('Rhymes: \'' + word + '\'');
    $.get(url, function (data) {
        contents = data.split(/\n/);
        for (i = 80; i < 150; i += 1) {
            inputLine = contents[i];
            if (inputLine.trim().startsWith('<div class="row">')) {
                break;
            }
        }
        inputLine = inputLine.replace(/<.*?>/g, '');
        inputLine = inputLine.replace(/(\d+ (Syllables|Syllable))/g, '<br/> &bull; $1<br/>');
        inputLine = inputLine.replace('<br/>', '');
        
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        if (inputLine.trim() !== '') {
            $('#output').html('<span class="larger">' + word + '</span> - Rhymes<hr>' + inputLine + '\nSource: <a target="_blank" href="' + url + '">' + url + '</a>');
        } else {
            $('#output').html('<span class="larger">No rhymes could be found</span> - Error Message<hr>It should be a crime, there wasn\'t a rhyme, Let\'s hope you don\'t work overtime.');
        }
    }).error(function () {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        $('#output').html('<span class="larger">No rhymes could be found</span> - Error Message<hr>It should be a crime. there wasn\'t a rhyme. Let\'s hope you don\'t work overtime.');
    });
}