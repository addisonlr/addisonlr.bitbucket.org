/*global window, console, $, summarize, orderOfOps, factorial, calculate*/
/*jslint regexp: true*/
function calculate(equation, graphing) {
    'use strict';
    
    var steps = '',
        original = '',
        ops,
        parts,
        part,
        part2;
    
    equation = equation.replace(/ /g, '');
    equation = equation.replace(/\+-/g, '-'); //plus a negative
    equation = equation.replace(/--/g, '+'); // minus a negative
    equation = equation.replace(/(\+|-|\/|\*|\^)/g, ' $1 ');
    equation = equation.replace(/(((\*|\/|\^) {2}-) )/g, '$2'); //remove space after - to prevent errored subtraction
    equation = equation.replace(/\( - /g, '(-');
    equation = equation.replace(/\(/g, ' (');
    equation = equation.replace(/(\d)([a-zA-Z]|\()/g, '$1 * $2');
    equation = equation.replace(/([a-zA-Z])(\d)/g, '$1 * $2');
    equation = equation.replace(/pi|π/g, Math.PI.toString());
    equation = equation.replace(/e/g, Math.E.toString());
    if (equation.startsWith(' - ')) {
        equation = equation.replace(' - ', '-');
    }
    equation = ' ' + equation + ' ';
    equation = equation.replace(/ +/g, ' ');
    equation = equation.replace(/(\d+)(\!)/g, '$2 $1');
    console.log('CALCUALTOR: \'' + equation + '\'');
    steps += equation + '<br/>';
    original = equation.replace(/\*/g, '&#x00D7').replace(/\//g, '&#x00F7').replace(/sqrt/g, '&radic;');
    
    while (equation.contains('(')) {
        part = equation.replace(/.*?(\(([^(]+?)\)).*$/, '$1').trim();
        part2 = equation.replace(/.*?(\(([^(]+?)\)).*$/, '$2').trim();
        
        part2 = orderOfOps(part2, part2.split(' '));
        equation = equation.replace(part, part2);
        console.log('CURRENT EQUATION: \'' + equation + '\'');
        steps += equation + '<br/>';
    }
    parts = equation.trim().split(' ');
    
    equation = orderOfOps(equation, parts);
    steps += equation;
    
    if (graphing) {
        return equation;
    }
    $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
    $('#output').html('<span class="larger">' + equation + '</span> - Calculation<hr>' + steps);
}

function orderOfOps(equation, parts) {
    'use strict';
    
    var i = 1,
        index;
    while (parts.length !== 1) {
        if (equation.contains('^')) {
            index = $.inArray('^', parts);
            equation = equation.replace(/( |)(\d+ \^ \d+)/, '$1' + Math.pow(parts[index - 1], parts[index + 1]).toString());
            parts = equation.trim().split(' ');
        } else if (equation.contains('*') || equation.contains('/')) {
            for (i; i < parts.length; i += 2) {
                if (parts[i] === '*') {
                    equation = equation.replace(parts[i - 1] + ' * ' + parts[i + 1], (parts[i - 1] * parts[i + 1]).toString());
                    parts = equation.trim().split(' ');
                    i -= 2;
                } else if (parts[i] === '/') {
                    try {
                        equation = equation.replace(parts[i - 1] + ' / ' + parts[i + 1], (parts[i - 1] / parts[i + 1]).toString());
                        parts = equation.trim().split(' ');
                        i -= 2;
                    } catch (error) {
                        summarize('divide by zero error');
                    }
                }
            }
        } else if (equation.contains('+') || equation.contains('-')) {
            for (i = 1; i < parts.length; i += 2) {
                if (parts[i] === '+') {
                    equation = equation.replace(parts[i - 1] + ' + ' + parts[i + 1], (parseFloat(parts[i - 1]) + parseFloat(parts[i + 1])).toString());
                    parts = equation.trim().split(' ');
                    i -= 2;
                } else if (parts[i] === '-') {
                    equation = equation.replace(parts[i - 1] + ' - ' + parts[i + 1], (parts[i - 1] - parts[i + 1]).toString());
                    parts = equation.trim().split(' ');
                    i -= 2;
                }
            }
        } else if (equation.any(equation, ['sin', 'cos', 'tan', 'csc', 'sec', 'cot', 'sqrt', 'abs', '!', 'log', 'ln'])) {
            for (i = 0; i < parts.length; i += 1) {
                if (parts[i] === 'sin') {
                    equation = equation.replace('sin ' + parts[i + 1], Math.sin(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'cos') {
                    equation = equation.replace('cos ' + parts[i + 1], Math.cos(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'tan') {
                    equation = equation.replace('tan ' + parts[i + 1], Math.tan(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'asin') {
                    equation = equation.replace('asin ' + parts[i + 1], Math.asin(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'acos') {
                    equation = equation.replace('acos ' + parts[i + 1], Math.acos(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'atan') {
                    equation = equation.replace('atan ' + parts[i + 1], Math.atan(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'arcsin') {
                    equation = equation.replace('arcsin ' + parts[i + 1], Math.asin(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'arccos') {
                    equation = equation.replace('arccos ' + parts[i + 1], Math.acos(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'arctan') {
                    equation = equation.replace('arctan ' + parts[i + 1], Math.atan(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'csc') {
                    equation = equation.replace('csc ' + parts[i + 1], (1 / Math.sin(parts[i + 1])).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'sec') {
                    equation = equation.replace('sec ' + parts[i + 1], (1 / Math.cos(parts[i + 1])).toString());
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'cot') {
                    equation = equation.replace('cot ' + parts[i + 1], (1 / Math.tan(parts[i + 1])).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'sqrt') {
                    try {
                        equation = equation.replace('sqrt ' + parts[i + 1], Math.sqrt(parts[i + 1]).toString());
                        parts = equation.trim().split(' ');
                    } catch (err) {
                        summarize('imaginary number');
                    }
                } else if (parts[i] === 'abs') {
                    equation = equation.replace('abs ' + parts[i + 1], Math.abs(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === '!') {
                    equation = equation.replace(parts[i - 1] + ' !', factorial(parts[i - 1]));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'log') {
                    equation = equation.replace('log ' + parts[i + 1], (Math.log(parts[i + 1]) / Math.LN10).toFixed(5));
                    parts = equation.trim().split(' ');
                } else if (parts[i] === 'ln') {
                    equation = equation.replace('ln ' + parts[i + 1], Math.log(parts[i + 1]).toFixed(5));
                    parts = equation.trim().split(' ');
                }
            }
        }
    }
    return equation;
}

function factorial(number) {
    'use strict';
    var temp = 1,
        i = 2;
    for (i; i <= number; i += 1) {
        temp *= i;
    }
    return temp.toString();
}

function graph(equation) {
    'use strict';
    var replaced = '',
        ys = [],
        width = $('#output').width() / 2,
        i = -width;
    
    equation = equation.replace(/(\d)([a-zA-Z])/g, '$1*$2');
    equation = equation.replace(/([a-zA-Z])(\d)/g, '$1*$2');
    
    for (i; i < width; i += 1) {
        replaced = equation.replace(/x/g, i.toString());
        try {
            ys.push(calculate(replaced, true));
        } catch (error) {
            ys.push('undefined');
        }
    }
    console.log(ys);
}