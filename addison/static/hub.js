/*global $, document, window, console, alert, specialCase, isQuestion, getLocation, setTimeout, getDistance, statementType, whoWas, whatWas, whatAre, whatDoes, whatTime, whatIs, howDo*/

function main(sentence) {
    "use strict";
    var word = [],
        subject,
        patt,
        loc1,
        loc2;
    sentence = sentence.toLowerCase().trim();
    word = sentence.split(' ');
    
    if (word.length > 2) {
        subject = sentence.replace(word[0] + ' ' + word[1] + ' ', '');
    } else {
        subject = '';
    }
    subject = subject.trim();
    
    //Routes to appropriate answer method
    if (specialCase(sentence)) {
        $('#output').show().css('left', '100%').animate({left: ($(window).width() / 5) - 24});
        return;
    }
    
    //if sentence is a question
    if (isQuestion(word[0]) !== -1) {
        console.log('question');
        if (word[0] === 'who') {
            if (word[1] === 'was') {
                whoWas(word, subject);
            } else if (word[1] === 'were') {
                alert('who.were');
            } else if (word[1] === 'are') {
                alert('who.are');
            } else if (word[1] === 'is') {
                alert('who.is');
            } else {
                alert('inline');
            }
        } else if (word[0] === 'what') {
            if (whatTime(word, sentence)) {
                return;
            } else if (word[1] === 'was') {
                whatWas(word, subject);
            } else if (word[1] === 'are') {
                whatAre(word, subject);
            } else if (word[1] === 'does') {
                whatDoes(word, subject);
            } else if (word[1] === 'is') {
                whatIs(word, subject);
            } else if ((word[1] === 'may' || word[1] === 'can') && sentence.contains(' i ')) {
                $('#qtcont').show().css('left', '2000px');
                $('#qtcont').animate({left: ($(window).width() / 5) - 23});
            } else if (sentence.contains('rhyme')) {
                alert('rhymes');
            } else if ($.inArray('questions', word) !== -1 && $.inArray('i', word) !== -1) {
                $('#qtcont').show().css('left', '2000px');
                $('#qtcont').animate({left: ($(window).width() / 5) - 23});
            } else {
                console.log('what inline');
            }
        } else if (word[0] === 'where') {
            if (word[1] === 'does') {
                alert('where.does');
            } else if (word[1] === 'is') {
                alert('where.is');
            } else if (word[1] === 'am') {
                getLocation(); //gets the users location and displays it
            } else {
                alert('where inline');
            }
        } else if (word[0] === 'when') {
            if (word[1] === 'did') {
                alert('when.did');
            } else if (word[1] === 'was') {
                alert('when.was');
            } else {
                alert('when inline');
            }
        } else if (word[0] === 'how') {
            if (word[1] === 'did') {
                alert('how.did');
            } else if (word[1] === 'was') {
                alert('how.was');
            } else if (word[1] === 'do') {
                howDo(word, subject);
            } else if (word[1] === 'many') {
                alert('conversions');
            } else if (word[1] === 'far') {
                patt = new RegExp('^(away is |is ).*(from ).*$');
                if (patt.test(subject)) {
                    loc1 = subject.substring(subject.indexOf('is ') + 3, subject.indexOf('from '));
                    loc2 = subject.substring(subject.indexOf('from ') + 5);
                    getDistance(loc1, loc2);
                } else if (new RegExp('^(away is |is |away am i from |am i from ).*$').test(subject)) {
                    if (subject.contains(' is ')) {
                        loc2 = subject.substring(subject.indexOf('is ') + 3);
                    } else if (subject.contains(' from ')) {
                        loc2 = subject.substring(subject.indexOf('from ') + 5);
                    }
                    getDistance('current', loc2);
                }
            } else {
                alert('how inline');
            }
        } else if (word[0] === 'is') {
            alert('is inline');
        } else if (word[1] === 'much') {
            statementType(sentence);
        } else {
            alert('else inline');
        }
    } else {
        statementType(sentence);
    }
}

function specialCase(sentence) {
    "use strict";
    if (sentence === 'xkcdrandom') {
        $('#output').html('This feature has not yet been converted to JavaScript. Apologies for any inconveniences.');
        return true;
    }
    if (sentence.contains(' wood ') && sentence.contains(' chuck')) {
        $('#output').html('<span class="larger">Amount of wood a woodchuck could chuck</span> - Hypothetical<hr><img src="/static/woodchuckgraph.png" id="graph"></img><span class="weighted italic">Assuming to chuck means to throw,</span>  one could assume that an average woodchuck could chuck 3 times its own weight. (The average woodchuck weighs between 4 & 9 lbs.)<br/>This could be modeled by the equation y=(6.5*3)x, where x is the number of days since the woodchuck started chucking wood.');
        return true;
    }
    if (sentence.contains(' chicken ') && sentence.contains(' road')) {
        $('#output').html('<span class="larger">Chicken crossing a road</span> - Reason<hr><img id="image" style="width:50%;" src="http://upload.wikimedia.org/wikipedia/commons/9/95/Why_did_the_chicken_cross_the_road%3F.JPG"></img>There is a dispute as to the answer. <a href="http://yonilerner.org/query/hobbes">Hobbes</a> answers that the chicken was born bad, and its farmer failed to keep law and order.  <a href="http://yonilerner.org/query/john%20locke">Locke</a> answers the chicken was rebelling angainst its farmer\'s unjust rules. <br>The <a href="http://yonilerner.org/query/nahmanides">Ramban</a> disagrees; He answer that the chicken crossed the road to get to the other side.');
        return true;
    }
    if (sentence.contains(' six afraid of seven') || sentence.contains(' 6 afraid of 7')) {
        $('#output').html('<span class="larger">Six being afraid of seven</span> - Reason<hr><span class="weighted">It wasn\'t.</span> Numbers are not sentient and therefore do not possess the ability to fear.');
        return true;
    }
    if (sentence.contains(' joke')) {
        $('#output').html('<span class="larger">Bar Joke</span> - Humor<hr><span class="titled" title="set-up">A <a href="http://www.yonilerner.org/query/wavefunction" target="_blank">wavefunction</a> walks into a bar and then collapses.</span> It procedes to scream, <a class="titled" title="punchline" href="http://www.yonilerner.org/query/wave%20function%collapse" target="_blank">"What are you looking at!"</a>');
        return true;
    }
    if (sentence.contains('carmen san')) {
        $('#output').html('<span class="larger">Carmen Sandiego</span> - Location<hr>You know, after decades of searching you\'d think someone would have found her by now.');
        return true;
    }
    if (sentence.contains('when') && sentence.contains('you ') && sentence.contains(' created')) {
        $('#output').html('<span class="larger">February 10 2013</span> - Date of Creation<hr>S.P.I.F.F. was first <span class="weighted">backed up to the internet on February 10 2013</span>. It started earlier as an idea, and remained as such for a couple of weeks. It was not until April 20 that S.P.I.F.F. was released on the internet. The name <span class="weighted">S.P.I.F.F was first thought of and implemented November 7 2013.</span>');
    }
    
    if (sentence.contains('how many licks') && sentence.contains('tootsie')) {
        //tootsie pop easter egg
        $('#output').html('<span class="larger">3</span> - Licks to get to center of a Tootsie Pop<hr><video autoplay><source src="/static/tootsiePop.mp4" type="video/mp4"></video><div class="quote">Boy: "How many licks does it take to get to the tootsie roll center of a tootsie pop?"</div>');
        setTimeout(function () {$('.quote').text('Owl: "A good question."'); setTimeout(function () {$('.quote').text('Owl: "Let\'s find out."'); setTimeout(function () {$('.quote').text('Owl: "One"'); setTimeout(function () {$('.quote').text('Owl: "Two"'); setTimeout(function () {$('.quote').text('Owl: "Three." (Crunch)'); setTimeout(function () {$('.quote').text('Owl: "Three."'); }, 1000); }, 2000); }, 800); }, 1100); }, 2400); }, 4500);
    }
}

//checks if the string is a question
function isQuestion(firstWord) {
    "use strict";
    var qWords = ['who', 'what', 'where', 'when', 'why', 'how', 'did', 'was', 'were', 'are', 'do', 'does', 'is', 'will', 'would'];
    return $.inArray(firstWord, qWords);
}