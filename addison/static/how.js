/*global console, translate*/
function howDid(word, subject) {
    'use strict';
    //return inline(word, subject, 'how');
    console.log('howDid');
}

function was(word, subject) {
    'use strict';
    //return inline(word, subject, 'how');
    console.log('howWas');
}
    
function howDo(word, subject) {
    'use strict';
    if (new RegExp('^(you |i )(say).+(in ).+$').test(subject)) {
        translate(word[4], word[6]);
    }
}