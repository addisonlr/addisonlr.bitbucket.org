from flask import Flask
from routes import site

def setup_app():
    app = Flask('addison')
    app.register_blueprint(site)

    app.secret_key = 'debug'

    app.debug = True
    app.config['PROPAGATE_EXCEPTIONS'] = False
    app.config['PRESERVE_CONTEXT_ON_EXCEPTION'] = False

    return app
