from search import inline
import wikipedia, re

def was(word, subject):
    if re.match(r'^.+\'s.+$', subject): #any char one or more times followed by 's - Who was albert einstein's wife
        topic = subject[:subject.index("'")].replace(' ', '_').lstrip().rstrip() #part before 's - albert einstein
        detail = subject[subject.index("'") + 2:].lstrip().rstrip() #part after 's - wife
        if wikipedia.elaborate(topic, detail, ' was '):
            return wikipedia.elaborate(topic, detail, ' was ')
        elif inline(word, subject):
            return inline(word, subject)
        else:
            return
    elif wikipeida.summarize(subject):
        return wikipedia.summarize(subject)
    else:
        return
        
def were(word, subject):
    if re.match(r'^(his |her ).+', subject):
        return wikipedia.elaborate(wikipedia.topic, re.sub('(his |her )', '', subject).lstrip().rstrip(), ' were ')
    elif re.match(r'^.+\'s.+$', subject):
        topic = subject[:subject.index("'")].replace(' ', '_').lstrip().rstrip()
        detail = subject[subject.index("'") + 2:].lstrip().rstrip()
        return wikipedia.elaborate(topic, detail, ' were ')
    else:
        return wikipedia.summarize(subject)
        
def are(word, subject):
    if re.match(r'^(his |her ).+', subject):
        return wikipedia.elaborate(wikipedia.topic, re.sub('(his |her )', '', subject).lstrip().rstrip(), ' are ')
    elif re.match(r'^.+\'s.+$', subject):
        topic = subject[:subject.index("'")].replace(' ', '_').lstrip().rstrip()
        detail = subject[subject.index("'") + 2:].lstrip().rstrip()
        return wikipedia.elaborate(topic, detail, ' are ')
    elif wikipedia.summarize(subject):
        return wikipedia.summarize(subject)
    else:
        inline(word, subject)
        
def is_(word, subject):
    if re.match(r'^(his |her ).+', subject):
        return wikipedia.elaborate(wikipedia.topic, re.sub('(his |her )', '', subject).lstrip().rstrip(), ' is ')
    elif re.match(r'^.+\'s.+$', subject):
        topic = subject[:subject.index("'")].replace(' ', '_').lstrip().rstrip()
        detail = subject[subject.index("'") + 2:].lstrip().rstrip()
        return wikipedia.elaborate(topic, detail, ' is ')
    elif wikipedia.summarize(subject):
        return  wikipedia.summarize(subject)
    else:
        return inline(word, subject)