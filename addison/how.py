import words, re
from search import inline

def did(word, subject):
    return inline(word, subject, 'how')

def was(word, subject):
    return inline(word, subject, 'how')
    
def do_(word, subject):
    if re.match(r'^(you |i )(say).+(in ).+$', subject):
        return words.translate(word[4], word[6])