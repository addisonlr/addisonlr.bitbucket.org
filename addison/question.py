#return true if sentence is a question
def is_question(first_word):
    
    q_words = ['who', 'what', 'where', 'when', 'why', 'how', 'did', 'was', 'were', 'are', 'do', 'does', 'is', 'will', 'would']
    return first_word in q_words