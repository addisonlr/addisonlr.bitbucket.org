import wikipedia, re

def inline(word, subject, origin = 'nvm'):
    if origin == 'is': #if yes/no question
        subject = subject[3:]#get rid of 'is '
        if wikipedia.inline_search(word[1], subject.replace(word[1] + ' ', ''), 'is'): #if question can be answered
            return wikipedia.inline_search(word[1], subject.replace(word[1] + ' ', ''), 'is') #then return the answer
    
    elif origin == 'how' and word[2].startswith(('is', 'was')):
        if wikipedia.inline_search(subject.replace(word[2] + ' ', ''), word[1], 'how'):
            return wikipedia.inline_search(subject.replace(word[2] + ' ', ''), word[1], 'how')
        
    elif origin == 'who':
        if wikipedia.inline_search(subject, word[1], origin):
            return wikipedia.inline_search(subject, word[1], origin)
    
    elif origin == 'when':
        if word[1] == 'was' and word[2] == 'the':
            if wikipedia.inline_search(subject.replace('the ', '' ,1), 'was', 'when'):
                return wikipedia.inline_search(subject.replace('the', '', 1), 'was', 'when')
    elif origin == 'why':
        if subject.startswith('the '):
            if ' a ' in subject:
                if wikipedia.inline_search(subject[:subject.index(' a ')], subject[subject.index(' a '):] + ' because', 'why'):
                    return wikipedia.inline_search(subject[:subject.index(' a ')], subject[subject.index(' a '):] + ' because', 'why')
                    
            else:
                if wikipedia.inline_search(subject[:subject.index(word[5]) - 1], subject[subject.index(word[5]):]):
                    return wikipedia.inline_search(subject[:subject.index(word[5]) - 1], subject[subject.index(word[5]):])
    elif subject.startswith('the '):
        if '\'s' in subject:
            word[3] = word[3].replace('\'s', '')
            if wikipedia.inline_search(word[2] + ' ' + word[3], subject[subject.index(' ', subject.index(' ') + 1):].lstrip(), origin):
                return wikipedia.inline_search(word[2] + ' ' + word[3], subject[subject.index(' ', subject.index(' ') + 1):].lstrip(), origin)
            
        elif 'of the' in subject or 'in the' in subject:
            print 'of the'
            try:
                topic = subject[subject.index('of the') + 7:]
                detail = subject[subject.index('the') + 4:subject.index('of the')]
            except:
                topic = subject[subject.index('in the') + 7:]
                detail = subject[subject.index('the') + 4:subject.index('in the')]
            if wikipedia.inline_search(topic, detail, origin):
                return wikipedia.inline_search(topic, detail, origin)
                
        else:
            if wikipedia.inline_search(word[3], subject[subject.index(word[4]):], 'how'):
                return wikipedia.inline_search(word[3], subject[subject.index(word[4]):], 'how')
        
    else: #if subject doesn't start with the
        if '\'s' in subject:
            topic = next(obj for obj in word if '\'s' in obj)
            if wikipedia.inline_search(topic.replace('\'s', ''), subject.replace(topic, ''), origin):
                return wikipedia.inline_search(topic.replace('\'s', ''), subject.replace(topic, ''), origin)
            else:
                topic = word[word.index(topic) - 1] + ' ' + topic
                if wikipedia.inline_search(topic.replace('\'s', ''), subject.replace(topic, ''), origin):
                    return wikipedia.inline_search(topic.replace('\'s', ''), subject.replace(topic, ''), origin)
                    
        else: #if it doesn't contain 's
            topic = word[2]
            print 'w2: ' + topic
            if wikipedia.inline_search(topic, subject.replace(topic + ' ', ''), origin):
                return wikipedia.inline_search(topic, subject.replace(topic + ' ', ''), origin)
            else:
                topic = word[2] + ' ' + word[3]
                if wikipedia.inline_search(topic, subject.replace(topic + ' ', ''), origin):
                    return wikipedia.inline_search(topic, subject.replace(topic + ' ', ''), origin)
        
    if ' of ' in subject:
        details = ''
        if '\'s' in subject:
            topic = subject[subject.rindex(' of ') + 4:subject.index('\'s')]
            details = subject.replace(subject[subject.rindex(' of ') + 4:subject.index('\'s') + 2], '')
        else:
            if ' of the ' in subject:
                for i in range(len(word)):
                    if word[i] == 'the':
                        topic = word[i] + ' ' + word[i + 1]
                        break
                
                details = subject.replace(topic, '')
            else:
                topic = subject[subject.rindex(' of ') + 4:subject.index(' ', subject.rindex(' of ') + 4)]
                details = subject.replace(topic, '')
            
        details = ' '.join(details.split())
        topic = topic.rstrip().lstrip()
        details = details.rstrip().lstrip()
        if wikipedia.inline_search(topic, details, origin):
            return wikipedia.inline_search(topic, details, origin)