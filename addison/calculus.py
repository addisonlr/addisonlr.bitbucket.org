import re, math

def derive(function):
    section = ''
    '''function = function.strip()
    function = function.replace('+-', '-') #plus a negative is the same a minus
    function = function.replace('--', '+') #minus a negative is the same as plus
    function = function.replace('+', ' + ')
    function = function.replace('-', ' - ')
    function = function.replace('*', ' * ')
    function = function.replace('/', ' / ')
    function = function.replace('^', ' ^ ')
    function = function.replace('*  - ', '* -') #remove space after negative to prevent substraction
    function = function.replace('/  - ', '/ -')
    function = function.replace('^  - ', '^ -')
    function = function.replace('( - ', '(-')
    function = function.replace('pi', str(math.pi))
    function = function.replace('e', str(math.e))
    if function.startswith(' -'):
        function = re.sub(r'- ', '-', function, 1)
    function = ' ' + function + ' '
    function = function.replace('  ', ' ') #remove double whitespace
    function = re.sub(r'(\d)(x|\()', '\g<1> * \g<2>', function) #insert multiplication where it is implied
    function = re.sub(r'(\d)([a-zA-Z])', '\g<1> * \g<2>', function)
    function = re.sub(r'(x)(\d)', '\g<1>x * ', function)
    function = re.sub(r'(\d+)(\!)', '\g<2>' + '\g<1>', function)
    function = function.encode('utf-8')
    parts = function.split()'''
    
    middle = function.replace('x', '( x + h )')
    middle = '(( ' + middle + ' ) - ( ' + function + ' ))/h'
    
    #distribute
    sec = re.search('\d\(.*?\)', middle)
    if sec:
        section = sec.group(0);
        distributed = distribute(section)
        middle = middle.replace(section, distributed)
        
    #cancel out
    parts = middle.replace('(', '').replace(')', '').replace('/h', '').split()
    for i in range(len(parts) - 1):
        if parts[i] == '-':
            parts[i] = '-' + parts[i+1]
            del(parts[i+1])
    print parts
    
    for i in range(len(parts) - 1):
        print parts[i], i
        if parts[i].startswith('-'):
            print 'neg'
            for j in range(len(parts)):
                print parts[j]
                if parts[j] == parts[i].replace('-', ''):
                    print 'opp'
                    parts[j] = '0'
                    parts[i] = '0'
                    break
        else:
            print 'else'
            for j in range(len(parts)):
                print parts[j]
                if parts[j] == '-' + parts[i]:
                    print 'opp'
                    parts[j] = '0'
                    parts[i] = '0'
                    break
    print parts
    derivative = ''
    derivative = ' ' + ' '.join(parts)
    derivative = derivative.replace(' 0 ', '')
    print derivative
    return function + '<br>' + middle

def distribute(section):
    if '*' not in section:
        parts = section.split()
        del parts[-1]
        out = parts[0].replace('(', '')
    distributed = ''
    for i in range(1, len(parts)):
        if parts[i] != '+' and parts[i] != '-':
            distributed += out + parts[i]
        else:
            distributed += ' ' + parts[i] + ' '
    return distributed