import re, requests, traceback
from requests.exceptions import RequestException
from flask import g

def stocks(stock, time):
    print 'finance.stocks(' + stock + ', ' + time + ')'
    try:
        req = requests.get('http://finance.yahoo.com/q?s=' + stock.upper() + '&ql=1')
        contents = req.text.splitlines()
        
        input_line = ''
        for i in range(50, len(contents) - 1):
            if contents[i].strip().startswith('</div><div id="yfi_broker_banners"'):
                input_line = contents[i]
                break

        ticker = re.sub(r'.*\((\D{3,5})\).*', '\g<1>', input_line) #gets ticker inside () from 3 to 5 chars long
        input_line = input_line.replace('<p>', '\n').replace('-', '- ').replace('|', ' - ') #formatting
        input_line = re.sub(r'%\)', '%)\n', input_line)
        input_line = input_line.replace('alt="Up">', '>&#9650;').replace('alt="Down">', '>&#9660;') #adds up or down triangle
        
        ret = 'http://chart.finance.yahoo.com/z?s=' + ticker + '&t=' + time + '&q=l\n'
        ret += re.sub(r'(\<.*?\>)', '', input_line) + 'Source: ' + str(req.url)
        return ret
    except:
        print 'There was an error in finance.stocks()'
        traceback.print_exc()