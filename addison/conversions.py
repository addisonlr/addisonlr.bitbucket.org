from utils import send_no_answer_email
import re

#length
inch = ' inch ', ' inches ', ' in '
foot = ' foot ', ' feet ', ' ft '
yard = ' yard ', ' yards ', ' yd '
mile = ' mile ', ' miles ', ' mi '
mm = ' millimeter ', ' millimeters ', ' ml '
cm = ' centimeter ', ' centimeters ', ' cm '
meter = ' meter ', ' meters ', ' m '
km = ' kilometer ', ' kilometers ', ' km '
au = ' astronomical unit ', ' astronomical units ', ' au '

#temperature
f = ' fahrenheit ', ' f '
c = ' celcius ', ' c '
k = ' kelvin ', ' k '

#mass
lbs = ' pound ', ' pounds ', ' lb ', ' lbs '
mg = ' milligram ', ' milligrams ', ' mg '
gram = ' gram ', ' g '
kg = ' killogram ', ' killograms ', ' kg '
ton = ' ton ', ' tons '
troy = ' troy ', ' troy oz ', ' troy ounce ', ' troy ounces '

#volume
tsp = ' teaspoon ', ' teaspoons ', ' tsp '
tbsp = ' tablespoon ', ' tablespoons ', ' tbsp '
oz = ' oz ', ' ounce ', ' ounces '
cup = ' cup ', ' cups '
pint = ' pint ', ' pints ', ' pt '
quart = ' quart ', ' quarts ', ' qt '
gal = ' gallon ', ' gallons ', ' gal '
ml = ' millileter ', ' millileters ', ' ml '
liter = ' liter ', ' liters ', ' l '

#time
sec = ' second ', ' seconds ', ' s '
ms = ' millisecond ', ' milliseconds ', ' ms '
min = ' minute ', ' minutes ', ' min '
hour = ' hour ', ' hours ', ' hr '
day = ' day ', ' days ', ' dy '
week = ' week ', ' weeks ', ' wk '
month = ' month ', ' months '
year = ' year ', ' years ', ' yr '
decade = ' decade ', ' decades '
century = ' century ', ' centuries '

#currency
penny = ' penny ', ' pennies ', ' cent ', ' cents '
nickel = ' nickel ', ' nickels '
dime = ' dime ', ' dimes '
quarter = ' quarter ', ' quarters '
dollar = ' dollar ', ' dollars ', ' buck ', ' bucks '

def convert(amount, initial, final, source = 'what'):
    print 'conversion', amount, initial, final
    initial = ' ' + initial + ' '
    final = ' ' + final + ' '
    if amount == '':
        amount = '1'
    original = amount
    amount = float(amount)
    
    length = ' inch ', ' inches ', ' in ', ' foot ', ' feet ', ' ft ', ' yard ', ' yards ', ' yd ', ' mile ', ' miles ', ' mi ', ' millimeter ', ' millimeters ', '  mm ', ' centimeter ', ' centimeters ', ' cm ', ' meter ', ' meters ', ' m ', ' kilometer ', ' kilometers ', ' km ', ' astronomical unit ', ' astronomical units ', ' au '
    temperature = ' fahrenheit ', ' f ', ' celcius ', ' c ', ' kelvin ', ' k '
    weight = ' milligram ', ' milligrams ', ' mg ', ' gram ', ' grams ', ' g ', ' pound ', ' pounds ', ' lb ', ' lbs ', ' killogram ', ' killograms ', ' kg ', ' ton ', ' tons ', ' troy ', ' troy oz ', ' troy ounce ', ' troy ounces '
    volume = ' teaspoon ', ' teaspoons ', ' tsp ', ' tablespoon ', ' tablespoons ', ' tbsp ', ' oz ', ' ounce ', ' ounces ', ' cup ', ' cups ', ' pint ', ' pints ', ' pt ', ' quart ', ' quarts ', ' qt ', ' gallon ', ' gallons ', ' gal ', ' millileter ', ' millileters ', ' ml ', ' liter ', ' liters ', ' l '
    time = ' second ', ' seconds ', ' s ', ' millisecond ', ' milliseconds ', ' ms ', ' minute ', ' minutes ', ' min ', ' hour ', ' hours ', ' hr ', ' day ', ' days ', ' dy ', ' week ', ' weeks ', ' wk ', ' month ', ' months ', ' year ', ' years ', ' yr ', ' decade ', ' decades ', ' century ', ' centuries '
    currency = ' penny ', ' pennies ', ' cent ', ' cents ', ' nickel ', ' nickels ', ' dime ', ' dimes ', ' quarter ', ' quarters ', ' dollar ', ' dollars ', ' buck ', ' bucks '
    
    if initial in length and final in length:
        amount = to_meter(amount, initial)
        
        if final in inch:
            amount = to_inch(amount)
        elif final in foot:
            amount = to_foot(amount)
        elif final in yard:
            amount = to_yard(amount)
        elif final in mile:
            amount = to_mile(amount)
        elif final in mm:
            amount = to_mm(amount)
        elif final in cm:
            amount = to_cm(amount)
        elif final in km:
            amount = to_km(amount)
        elif final in au:
            amount = to_au(amount)
    elif initial in temperature and final in temperature:
        amount = to_celcius(amount, initial)
        
        if final in f:
            amount = to_f(amount)
        elif final in k:
            amount = to_k(amount)
    elif initial in weight and final in weight:
        amount = to_gram(amount, initial)
        
        if final in lbs:
            amount = to_lbs(amount)
        if final in mg:
            amount = to_mg(amount)
        if final in kg:
            amount = to_kg(amount)
        if final in ton:
            amount = to_ton(amount)
        if final in troy:
            amount = to_troy(amount)
    elif initial in volume and final in volume:
        amount = to_liter(amount, initial)
        
        if final in tsp:
            amount = to_tsp(amount)
        if final in tbsp:
            amount = to_tbsp(amount)
        if final in oz:
            amount = to_oz(amount)
        if final in cup:
            amount = to_cup(amount)
        if final in pint:
            amount = to_pint(amount)
        if final in quart:
            amount = to_quart(amount)
        if final in gal:
            amount = to_gal(amount)
        if final in ml:
            amount = to_ml(amount)
    elif initial in time and final in time:
        amount = to_day(amount, initial)
        
        if final in ms:
            amount = to_ms(amount)
        if final in sec:
            amount = to_sec(amount)
        if final in min:
            amount = to_min(amount)
        if final in hour:
            amount = to_hour(amount)
        if final in week:
            amount = to_week(amount)
        if final in month:
            amount = to_month(amount)
        if final in year:
            amount = to_year(amount)
        if final in decade:
            amount = to_decade(amount)
        if final in century:
            amount = to_century(amount)
    elif initial in currency and final in currency:
        amount = to_dollar(amount, initial)
        
        if final in penny:
            amount = to_penny(amount)
        if final in nickel:
            amount = to_nickel(amount)
        if final in dime:
            amount = to_dime(amount)
        if final in quarter:
            amount = to_quarter(amount)
    else:
        return '<span class="larger">Error</span> - Conversion<hr>These units <span class="weighted">could not be converted</span>. Please make sure that the units are in fact convertable. If it still does not work, the units might not be supported.'
        send_no_answer_email('conversion: ' + str(amount) + ', ' + initial + ', ' + final)
    
    initial = initial.replace(' ton', ' metric ton')
    initial = initial.replace(' oz', ' fl. oz.')
    initial = initial.replace(' ounce', ' fluid ounce')
    final = final.replace(' ton', ' metric ton')
    final = final.replace(' oz', ' fl. oz.')
    final = final.replace(' ounce', ' fluid ounce')
    
    if 'e+' in str(amount):
        amount = str(amount).replace('e+', '&times;10<sup>') + '</sup>'
    
    if source == 'how':
        return '<span class="larger">' + str(amount) + final + '</span>- Conversion<hr>There are <span class="weighted">' + str(amount) + final + ' in a ' + initial + '</span>'
    return '<span class="larger">' + str(amount) + final + '</span>- Conversion<hr>' + original + initial + 'is ' + str(amount) + final

def to_meter(amount, initial):
    if initial in inch:
        return (amount / 12) /3.2808
    if initial in foot:
        return amount / 3.2808
    if initial in yard:
        return (amount * 3) / 3.2808
    if initial in mile:
        return (amount * 5280) / 3.2808
    if initial in mm:
        return amount / 1000
    if initial in cm:
        return amount / 100
    if initial in meter:
        return amount
    if initial in km:
        return amount * 1000
    if initial in au:
        return amount * 149597870691
    
def to_inch(amount):
    return (amount * 3.2808) * 12

def to_foot(amount):
    return amount * 3.2808

def to_yard(amount):
    return (amount * 3.2808) / 3

def to_mile(amount):
    return (amount * 3.2808) / 5280

def to_mm(amount):
    return amount * 1000

def to_cm(amount):
    return amount * 100

def to_m(amount):
    return amount

def to_km(amount):
    return amount / 1000

def to_au(amount):
    return amount / 149597870691

def to_celcius(amount, initial):
    if initial in f:
        return (amount - 32) / 1.8
    if initial in c:
        return amount
    if initial in k:
        return amount -273
    
def to_f(amount):
    return (amount * 1.8) + 32

def to_k(amount):
    return amount + 273

def to_gram(amount, initial):
    if initial in lbs:
        return amount * 453.5924
    if initial in mg:
        return amount / 1000
    if initial in gram:
        return amount
    if initial in kg:
        return amount * 1000
    if initial in ton:
        return amount * 1000000
    if initial in troy:
        return amount / 0.032151
    
def to_lbs(amount):
    return amount / 453.5924

def to_mg(amount):
    return amount * 1000

def to_kg(amount):
    return amount / 1000

def to_ton(amount):
    return amount / 1000000

def to_troy(amount):
    return amount * 0.032151

def to_liter(amount, initial):
    if initial in tsp:
        return (amount / 768) * 3.785
    if initial in tbsp:
        return (amount / 256) * 3.785
    if initial in oz:
        return (amount / 128) * 3.785
    if initial in cup:
        return (amount / 16) * 3.785
    if initial in pint:
        return (amount / 8) * 3.785
    if initial in quart:
        return (amount / 4) * 3.785
    if initial in gal:
        return amount * 3.785
    if initial in ml:
        return amount / 1000
    if initial in l:
        return amount
    
def to_tsp(amount):
    return (amount / 3.785) * 768

def to_tbsp(amount):
    return (amount / 3.785) * 256

def to_oz(amount):
    return (amount / 3.785) * 128

def to_cup(amount):
    return (amount / 3.785) * 16

def to_pint(amount):
    return (amount / 3.785) * 8

def to_quart(amount):
    return (amount / 3.785) * 4

def to_gal(amount):
    return amount / 3.785

def to_ml(amount):
    return amount * 1000

def to_day(amount, initial):
    if initial in sec:
        return amount / 86400
    if initial in ms:
        return amount / 86400000
    if initial in min:
        return amount / 1440
    if initial in hour:
        return amount / 24
    if initial in day:
        return amount
    if initial in week:
        return amount * 7
    if initial in month:
        return amount * 30
    if initial in year:
        return amount * 365.242
    if initial in decade:
        return amount * 3652.42
    if initial in century:
        return amount * 36524.2
    
def to_ms(amount):
    return amount * 86400000

def to_sec(amount):
    return amount * 86400

def to_min(amount):
    return amount * 1440

def to_hour(amount):
    return amount * 24

def to_week(amount):
    return amount / 7

def to_month(amount):
    return amount / 30

def to_year(amount):
    return amount / 365.242

def to_decade(amount):
    return amount / 3654.24

def to_century(amount):
    return amount / 36542.4

def to_dollar(amount, initial):
    if initial in penny:
        return amount / 100
    if initial in nickel:
        return amount / 20
    if initial in dime:
        return amount / 10
    if initial in quarter:
        return amount / 4
    if initial in dollar:
        return amount
    
def to_penny(amount):
    return amount * 100

def to_nickel(amount):
    return amount * 20

def to_dime(amount):
    return amount * 10

def to_quarter(amount):
    return amount * 4