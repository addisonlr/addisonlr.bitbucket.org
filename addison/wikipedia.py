import urllib2, re, requests, traceback, wikipedia, speech
from requests.exceptions import RequestException
from wikis import Wikis
topic = ''

def summarize(topic):
    print 'summarize', topic
    #subject = re.sub('_', ' ', topic)
    try:
        if re.match(r'.+( in terms of)( | a).+', topic):
            topic = re.sub(r'( in terms of)( a | )', ' (', topic) + ')'
        
        topic_spaces = topic
        topic_spaces = re.sub(r'(\(.*?\))', '', topic_spaces)
        split_topic = topic_spaces.split()
        topic = re.sub(' ', '_', topic)
        topic = re.sub(r'^(an_|a_|A_)', '', topic)
        wikipedia.topic = 'hello'
        
        req = requests.get('http://www.en.wikipedia.org/wiki/' + topic)
        
        if req.status_code == 404:
            print '404', topic
            if topic.startswith(('the_', 'The_')):
                return wikipedia.summarize(topic[4:].replace('_', ' '))
            elif re.match(r'^.*(_of_).*$', topic) and '_(' not in topic and '_the_' not in topic:
                top = re.sub(r'(.*_of_)', '', topic)
                top = re.sub(' ', '_', top)
                det = re.sub(r'(the_|_of.*)', '', topic)
                return wikipedia.elaborate(top, det, ' is ')
            else:
                old = topic #used to check if it is already uppercase
                topic = topic.title()
                if old != topic:
                    return wikipedia.summarize(topic)
                elif re.match(r'^(\w+$)', topic):
                    pass
                    #w = Words()
                    #w.define words[2]
                elif '_(' in topic.lower():
                    topic = topic.lower()
                    sp = topic.split('_')
                    topic = ''
                    for i in range(len(sp)):
                        if i == 0:
                            topic += sp[i].title()
                        elif i == len(sp) - 1:
                            topic += '_' + sp[i].title()
                        else:
                            topic += '_' + sp[i]
                    return wikipedia.summarize(topic)
                else:
                    pass
        contents = req.iter_lines()
        
        disambiguate = False
        contains_words = False
        toc = False
        options = ""
        input_line = ""
        for input_line in contents:
            input_line = input_line.decode('utf-8')
            if disambiguate:
                if input_line.startswith('<table id="toc'):
                    toc = True
                elif (input_line.startswith('<li')) and not toc:
                    print input_line
                    options = options + input_line + '\n'
                elif input_line.startswith('</ul>'):
                    if toc:
                        pass
                    else:
                        break
                elif input_line.startswith('</table>') and toc:
                    toc = False
            elif input_line.startswith('<p>') and not input_line.startswith('<p><a href') and not 'mbox-text-span' in input_line:
                if any(i in re.sub(r'<.*?>', '', input_line) for i in split_topic) or any(i in re.sub(r'<.*?>', '', input_line.lower()) for i in split_topic):
                    print 'contains'
                    contains_words = True
                else:
                    contains_words = False
                    
                if contains_words:
                    if input_line.find('may refer to') != -1:
                        disambiguate = True
                        options = options + input_line + '\n'
                    elif input_line.find("<b>"):
                        break
        
        if disambiguate:
            if options.endswith('\n'):
                options = options[:len(options) - 1]
            input_line = u'\n\u2022'.join(options.split('\n'))
        
        input_line = input_line.replace('</span>', '.')
        input_line = re.sub(r'(\<.*?\>)', '', input_line)
        if not disambiguate:
            input_line = re.sub(r'(\(.*?\))', '', input_line)
        input_line = re.sub(r'(\[.*?\])', '', input_line)
        input_line = re.sub(r'(\&.+\;)', '', input_line)
        while '..' in input_line:
            input_line = input_line.replace('..', '.')
        input_line = input_line.replace(',.', ',')
        #speech.tokenize(input_line);
        
        wik = Wikis()
        if wikipedia.show(topic):
            ret = wikipedia.show(topic)
            ret += input_line
            if not disambiguate:
                ret += '\n'
            ret += 'Source: ' + str(req.url)
        else:
            ret = input_line
            if not disambiguate:
                ret += '\n'
            ret += 'Source: ' + str(req.url)
        
        #check to see if it is a valid return
        if 'is .' in ret:
            pass
        elif 'Other reasons this message may be displayed:' in ret:
            pass
        elif not input_line:
            pass
        else:
            return ret
    except:
        traceback.print_exc()
        
def elaborate(topic, detail, origin):
    print 'elaboration'
    print 'Topic: ' + topic
    print 'Detail: ' + detail
    wikipedia.topic = topic
    description = ''
    split_detail = detail.split()
    
    try:
        req = requests.get('http://www.en.wikipedia.org/wiki/' + topic)
        if req.status_code == 404:
            print 'Elaboration 404 error'
            if 'the_' in topic:
                topic = topic.replace('the_', '')
                return wikipedia.elaborate(topic, detail, origin)
            else:
                return wikipedia.inline_search(topic, detail, origin)
                
        contents = req.iter_lines()
        
        input_line = ''
        contains_words = False
        info_box = False
        skip = False
        skipped = False
        for input_line in contents:
            input_line = input_line.decode('utf-8')
            if '<table class="infobox' in input_line: #if the line is the start of the infobox
                print 'info box'
                info_box = True
            
            if info_box:
                if input_line.startswith('<td') or input_line.startswith('<th') or input_line.startswith('<li'):
                    if contains_words or skipped: #if this line is immediately after a line that contains the words of the detail
                        input_line = re.sub(r'(\<.*?\>)', '', input_line)
                        input_line = re.sub(r'(\[.*?\])', '', input_line)
                        if input_line == '&#160;-&#160;':
                            skip = True
                        input_line = re.sub(r'(\&.+\;)', '', input_line)
                        if re.match(r'.', input_line):
                            if skipped:
                                break
                            elif skip:
                                skipped = True
                            else:
                                break
                    
                    if not re.sub(r'(\<.*?\>)', '', input_line):
                        pass
                    elif all(i in input_line.lower() for i in split_detail):
                        contains_words = True
                        description = input_line
                        description = re.sub(r'(\<.*?\>)', '', description)
                        description = re.sub(r'(\[.*?\])', '',      description)
                        description = re.sub(r'(\&.+\;)', '', description)
                    else:
                        contains_words = False
             
        if '<' in input_line:
            input_split = input_line.split('<')
            input_line = ''
            for i in range(len(input_split)):
                input_split[i] = '<' + input_split[i]
                input_line = input_line + input_split[i]
            
        input_line = re.sub(r'(\<(.*?\>))', '', input_line)
        input_line = re.sub(r'(\[.*?\])', '', input_line)
        input_line = re.sub(r'(\&.+\;)', '', input_line)
        input_line = input_line.replace('<', '')
        #speech.tokenize(input_line);
        
        if 'when was ' in origin:
            ret = re.sub('_', ' ', topic.title()) + origin.replace(' when', '') + description.lower() + ' on ' + input_line.replace(')', ') ')
            
        elif 'when did' in origin:
            ret = re.sub('_', ' ', topic.title()) + description.lower() + ' on ' + input_line.replace(')', ') ')
            
        else:
            ret = re.sub('_', ' ', topic.title()) + '\'s ' + description.lower().lstrip().rstrip() + origin + input_line + '.'
            
        ret += '\n'
        ret += 'Source: ' + str(req.url)
        if input_line.strip():
            return '<span class="larger">' + topic.title().replace('_', ' ') + '</span> - ' + detail.title() + '<hr>' + ret
        elif wikipedia.inline_search(topic, detail, origin):
            return wikipedia.inline_search(topic, detail, origin)
        else:
            pass
    except:
        traceback.print_exc()
        
def show(topic, base='http://en.wikipedia.org/wiki/'):
    wikipedia.topic = topic
    
    try:
        topic = re.sub(' ', '_', topic)
        topic = re.sub('^(a_|an_)', '', topic)
        
        req = requests.get(base + topic)
        contents = req.iter_lines()
        
        input_line = ''
        for input_line in contents:
            if '<img alt=' in input_line:
                if int(re.sub(r'\D', '', input_line[input_line.index('width="') + 7 : input_line.index('"', input_line.index('width="') + 7)])) > 150:
                    input_line = input_line[input_line.index('src=') + 5 : input_line.index('"', input_line.index('src=') + 5)]
                    return 'http:' + input_line + '\n'
                    
    except:
        return False
        
def inline_search(topic, detail, origin = 'nvm'):
    print 'Origin: ' + origin
    print 'Topic: ' + topic
    print 'Detail: ' + detail
    try:
        topic = topic.replace(' ', '_')
        req = requests.get('http://en.wikipedia.org/wiki/' + topic)
        #error handling
        if req.status_code == 404:
            if 'the_' in topic:
                return wikipedia.inline_search(re.sub(r'the_', '', topic, 1), detail, origin)
            old = topic #used to check if it is already uppercase
            topic = topic.title()
            if old != topic:
                return wikipedia.inline_search(topic, detail)
                
        contents = req.iter_lines()
        
        input_line = ''
        main_text = False
        keywords = detail.split()
        for input_line in contents:
            input_line = input_line.decode('utf-8')
            if input_line.startswith('<p>'):
                main_text = True
                
            if main_text:
                if all(i in re.sub(r'(\<.*?\>)', '', input_line.lower()) for i in keywords):
                #if all(i in input_line.lower() for i in keywords):
                    if not input_line.startswith('<li class="toclevel-'):
                        break
        
        input_line = re.sub(r'<.*?>', '', input_line)
        input_line = re.sub(r'\[.*?\]', '', input_line)
        input_line = re.sub(r'(\&.*?;)', ' ', input_line)
        
        input_line = input_line.replace('U.S.', 'US')
        sentences = input_line.replace('U\.S\.', 'US').split('. ')
        time = [' second', ' minute', ' hour', ' day', ' week', ' month', ' year']
        for sentence in sentences:
            if all(i in sentence.lower() for i in keywords):
                if origin == 'how':
                    if any(t in sentence.lower() for t in time) and 'from' not in sentence:
                        pass
                    else:
                        break
                elif origin == 'when':
                    if re.match(r'.*\d.*', sentence):
                        break
                    else:
                        pass
                else:
                    break
                
        input_line = sentence
        if input_line.lower().startswith('it '):
            input_line = re.sub(r'$(It|it).', topic.title, input_line)
            
        #speech.tokenize(input_line);
                
        ret = input_line.lstrip().rstrip() + '.\n'
        ret += 'Source: ' + str(req.url)
        if 'other reasons this ' in input_line.lower():
            pass
            
        elif 'if a page was recently created here' in input_line.lower():
            pass
        
        elif input_line:
            return ret
    except:
        print traceback.print_exc()