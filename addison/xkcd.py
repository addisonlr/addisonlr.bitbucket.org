import words, re, requests

def random():
    req = requests.get('http://dynamic.xkcd.com/random/comic')
    comic_id_str = req.request.url.split('/')[len(req.request.url.split('/')) - 2]
    return '<iframe src=%s width=1500 height=1000></iframe>' % ('http://explainxkcd.com/wiki/index.php/' + comic_id_str)
    comic_full_name = comic_id_str['query']['pages'].items()[0][1]['revisions'][0]['*']
    comic_name_unparsed = comic_full_name[comic_full_name.find('['):comic_full_name.rfind(']') + 1]
    comic_name_unparsed = comic_name_unparsed[comic_name_unparsed.rfind('[') + 1:comic_name_unparsed.find(']')]
    comic_name_parsed = comic_name_unparsed.replace(' ', '_')

    url = 'http://explainxkcd.com/wiki/api.php?'
    queryStr = 'format=json&action=query&rvprop=content&prop=revisions'
    queryStr += comic_name_parsed
    req2 = requests.get(url + queryStr)
    return inline
