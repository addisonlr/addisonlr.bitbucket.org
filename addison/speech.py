import re, speech, shutil, wave

phonetic = []
skip = -2 #used to know when to make a letter silent
vowels = 'a','e','i','o','u'
alveolars = 'n', 't', 'd', 's', 'z', 'l'
stops = 'p', 'b', 't', 'd', 'k', 'g'
syllabics = 'k'
constants = 'b', 'd', 'f', 'l', 'n', 'p', 'r', 't', 'v', 'z'
oolets = 'm', 'p', 'b', 'f', 'v', 'ch', 'dge', 'sh', 'ge'
consonants = 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'

def tokenize(passage):
    del phonetic[:]
    passage = re.sub(r'(,|;|:|-|\.\.\.|\.|\?|\!|\(|\))', ' \g<1>', passage)
    passage = passage.replace('  ', ' ') #removes double spaces
    passage = passage.replace('bb', 'b')
    passage = passage.replace('dd', 'd')
    passage = passage.replace('ph', 'f')
    passage = passage.replace('kn', 'n')
    passage = passage.replace('nn', 'n')
    passage = passage.replace('rr', 'r')
    
    #convert numbers into words
    passage = speech.parse_nums(passage)
    
    wait = 0 #number of iterations to skip
    for word in passage.split(): #loops through words
        print word
        if speech.special_word(word):
            continue
        for i, v in enumerate(word): #loops through letters
            if wait != 0:
                wait -= 1
            elif skip == 200:
                phonetic.append('silent')
                speech.skip += 1
            else:
                speech.skip += 1
                wait = speech.parse(i, v, word)
            
        phonetic.append('space')
        
    for i, v in enumerate(phonetic):
        if i != len(phonetic)-1:
            if v == 'au':
                if phonetic[i+1] == 'f':
                    phonetic[i] = 'auf'
                    del(phonetic[i+1])
            elif v == 'b':
                if phonetic[i+1] == 'a2' or phonetic[i+1] == 'au':
                    phonetic[i] = 'ba2'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'e':
                    phonetic[i] = 'be'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ee':
                    phonetic[i] = 'bee'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'oe':
                    phonetic[i] = 'boe'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'u':
                    phonetic[i] = 'bu'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ur':
                    phonetic[i] = 'bur'
                    del(phonetic[i+1])
            elif v == 'd':
                if phonetic[i+1] == 'a2' or phonetic[i+1] == 'au':
                    phonetic[i] = 'da2'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'i':
                    phonetic[i] = 'di'
                    del(phonetic[i+1])
            elif v == 'ee':
                if phonetic[i+1] == 'ng':
                    phonetic[i] = 'eeng'
                    del(phonetic[i+1])
            elif v == 'f':
                if phonetic[i+1] == 'e':
                    phonetic[i] = 'fe'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ur':
                    phonetic[i] = 'fur'
                    del(phonetic[i+1])
            elif v == 'h':
                if phonetic[i+1] == 'ee':
                    phonetic[i] = 'hee'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'i':
                    phonetic[i] = 'hi'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ue':
                    phonetic[i] = 'hue'
                    del(phonetic[i+1])
            elif v == 'k':
                if phonetic[i+1] == 'a2' or phonetic[i+1] == 'au':
                    phonetic[i] = 'ka2'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'eigh':
                    phonetic[i] = 'keigh'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'u':
                    phonetic[i] = 'ku'
                    del(phonetic[i+1])
            elif v == 'l':
                if phonetic[i+1] == 'a1':
                    phonetic[i] = 'la1'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'a2':
                    phonetic[i] = 'la2'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'a3':
                    phonetic[i] = 'la3'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'eigh':
                    phonetic[i] = 'leigh'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'e':
                    phonetic[i] = 'le'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ee':
                    phonetic[i] = 'lee'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'air':
                    phonetic[i] = 'lair'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'eye':
                    phonetic[i] = 'leye'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'au':
                    phonetic[i] = 'lau'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'oe':
                    phonetic[i] = 'loe'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'u':
                    phonetic[i] = 'lu'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ur':
                    phonetic[i] = 'lur'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ue':
                    phonetic[i] = 'lue'
                    del(phonetic[i+1])
            elif v == 'm':
                if phonetic[i+1] == 'eigh':
                    phonetic[i] = 'meigh'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'or':
                    phonetic[i] = 'mor'
                    del(phonetic[i+1])
            elif v == 'n':
                if phonetic[i+1] == 'or':
                    phonetic[i] = 'nor'
                    del(phonetic[i+1])
            elif v == 'p':
                if phonetic[i+1] == 'a2':
                    phonetic[i] = 'pa2'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'u':
                    phonetic[i] = 'pu'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ju':
                    phonetic[i] = 'pju'
                    del(phonetic[i+1])
            elif v == 'r':
                if phonetic[i+1] == 'a3':
                    phonetic[i] = 'ra3'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'au':
                    phonetic[i] = 'rau'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ee':
                    phonetic[i] = 'ree'
                    del(phonetic[i+1])
            elif v == 's':
                if phonetic[i+1] == 'ee':
                    phonetic[i] = 'see'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'i':
                    phonetic[i] = 'si'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'u':
                    phonetic[i] = 'su'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 't':
                    phonetic[i] = 'st'
                    del(phonetic[i+1])
            elif v == 't':
                if phonetic[i+1] == 'eigh':
                    phonetic[i] == 'teigh'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'oo':
                    phonetic[i] = 'too'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ue':
                    phonetic[i] = 'tue'
                    del(phonetic[i+1])
            elif v == 'th':
                if phonetic[i+1] == 'a3':
                    phonetic[i] = 'tha3'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'u':
                    phonetic[i] = 'thu'
                    del(phonetic[i+1])
            elif v == 'w':
                if phonetic[i+1] == 'a2':
                    phonetic[i] = 'wa2'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'e':
                    phonetic[i] = 'we'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'i':
                    phonetic[i] = 'wi'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'u':
                    phonetic[i] = 'wu'
                    del(phonetic[i+1])
                elif phonetic[i+1] == 'ur':
                    phonetic[i] = 'wur'
                    del(phonetic[i+1])
    
    shutil.copy("addison/static/joined.wav", "addison/static/phonemes/joined.wav")
    shutil.copy("addison/static/phonemes/joined.wav", "addison/static/joined.wav")
    for i, v in enumerate(phonetic):
        if v != 'unknown':
            if i == 0:
                continue
            elif i == 1:
                infiles = ["addison/static/phonemes/" + phonetic[0] + ".wav", "addison/static/phonemes/" + v + ".wav"]
            else:
                infiles = ["addison/static/phonemes/joined.wav", "addison/static/phonemes/" + v + ".wav"]
            outfile = "addison/static/phonemes/joined.wav"

            data= []
            for infile in infiles:
                w = wave.open(infile, 'rb')
                data.append( [w.getparams(), w.readframes(w.getnframes())] )
                w.close()

            output = wave.open(outfile, 'wb')
            output.setparams(data[0][0])
            output.writeframes(data[0][1])
            output.writeframes(data[1][1])
            output.close()
        
    print passage 
    return 'speechsyn' + str(phonetic)
  
def parse_nums(passage):
    suffix = ['', 'thousand', 'milleein', 'billeein', 'trilleein', 'qwadrilleein', 'qwintilleein', 'sextilleein', 'septilleein', 'octilleein', 'nahnilleein', 'decilleein']
    for i in passage.split():
        if re.match(r'.*(\d).*', i):
            temp = re.sub(r'\D*(\d+)\D*', '\g<1>', i)
            length = len(temp)
            word = ''
            for p, n in enumerate(temp):
                if temp == '0':
                    word = 'zeero'
                    break
                if (length - p) % 3 == 0:
                    if n == '1':
                        word += ' wun hundrid '
                    elif n == '2':
                        word += ' two hundrid '
                    elif n == '3':
                        word += ' three hundrid '
                    elif n == '4':
                        word += ' for hundrid '
                    elif n == '5':
                        word += ' feyev hundrid '
                    elif n == '6':
                        word += ' six hundrid '
                    elif n == '7':
                        word += ' seven hundrid '
                    elif n == '8':
                        word += ' eight hundrid '
                    elif n == '9':
                        word += ' nine hundrid '
                    elif n == '0':
                        continue
                elif (length - p) % 3 == 2:
                    if n != '1':
                        if n == '2':
                            word += ' twenty '
                        elif n == '3':
                            word += ' thirty '
                        elif n == '4':
                            word += ' forty '
                        elif n == '5':
                            word += ' fifty '
                        elif n == '6':
                            word += ' sixty '
                        elif n == '7':
                            word += ' seventy '
                        elif n == '8':
                            word += ' eighty '
                        elif n == '9':
                            word += ' neyenty '
                elif (length - p) % 3 == 1:
                    if temp[p-1] == '1' and p != 0:
                        if n == '1':
                            word += ' eleven '
                        if n == '2':
                            word += ' twelve '
                        elif n == '3':
                            word += ' thirteen '
                        elif n == '4':
                            word += ' forteen '
                        elif n == '5':
                            word += ' fifteen '
                        elif n == '6':
                            word += ' sixteen '
                        elif n == '7':
                            word += ' seventeen '
                        elif n == '8':
                            word += ' eighteen '
                        elif n == '9':
                            word += ' nineteen '
                        elif n == '0':
                            word += ' ten '
                    elif n == '1':
                        word += 'one'
                    elif n == '2':
                        word += 'two'
                    elif n == '3':
                        word += 'three'
                    elif n == '4':
                        word += 'for'
                    elif n == '5':
                        word += 'feyeve'
                    elif n == '6':
                        word += 'six'
                    elif n == '7':
                        word += 'seven'
                    elif n == '8':
                        word += 'eight'
                    elif n == '9':
                        word += 'nine'
                    word += suffix[(length-p-1)/3]
            print 'w', word
            passage = re.sub(temp, word, passage, 1)
        
    return passage

def special_word(word):
    specials = 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'people'
    ees = 'b', 'd', 'p', 't', 'v', 'z'
    ehs = 'f', 'l', 'm', 'n', 's'
    if word in ees:
        phonetic.append(str(word))
        phonetic.append('ee')
    elif word in ehs:
        phonetic.append('e')
        phonetic.append(str(word))
    elif word == 'c':
        phonetic.append('s')
        phonetic.append('ee')
    elif word == 'e':
        phonetic.append('ee')
    elif word == 'g':
        phonetic.append('dge')
        phonetic.append('ee')
    elif word == 'h':
        phonetic.append('eigh')
        phonetic.append('t')
        phonetic.append('ch')
    elif word == 'j':
        phonetic.append('dge')
        phonetic.append('eigh')
    elif word == 'k':
        phonetic.append('k')
        phonetic.append('eigh')
    elif word == 'o':
        phonetic.append('oe')
    elif word == 'q':
        phonetic.append('k')
        phonetic.append('ue')
    elif word == 'r':
        phonetic.append('au')
        phonetic.append('r')
    elif word == 'u':
        phonetic.append('ju')
    elif word == 'w':
        phonetic.append('d')
        phonetic.append('u')
        phonetic.append('b')
        phonetic.append('l')
        phonetic.append('ju')
    elif word == 'x':
        phonetic.append('e')
        phonetic.append('k')
        phonetic.append('s')
    elif word == 'y':
        phonetic.append('w')
        phonetic.append('eye')
    elif word == 'people':
        phonetic.append('p')
        phonetic.append('ee')
        phonetic.append('p')
        phonetic.append('l')
        phonetic.append('silent')
        
    if word in specials:
        return True
    else:
        return False

def parse(i, v, word):
    four = word[i:i+4]
    if i >= 2:
        two = word[i-2:i]
    else:
        two = ''
    print 'two:', two, 'four:', four

    #check all four letter phonemes
    if four == 'eigh':
        phonetic.append('eigh')
        return 3
    if four == 'ient':
        phonetic.append('ee')
        phonetic.append('i')
        phonetic.append('n')
        phonetic.append('t')
        return 3
        
    #check all three letter phonemes
    if four[:3] == 'air':
        phonetic.append('air')
        return 2
    if four[:3] == 'ere':
        if 'h' in word:
            phonetic.append('ear')
            return 2
        elif i+3 == len(word)-1:
            phonetic.append('ur')
            phonetic.append('silent')
            return 2
        else:
            phonetic.append('air')
            return 2
    if four[:3] == 'are':
        if i != 0:
            phonetic.append('air')
            return 2
        elif word == 'are':
            phonetic.append('au')
            phonetic.append('r')
            return 2
    if four[:3] == 'ary':
        phonetic.append('air')
        phonetic.append('ee')
        return 2
    if four[:3] == 'and':
        phonetic.append('a3')
        phonetic.append('n')
        phonetic.append('d')
        return 2
    if four[:3] == 'ear':
        if any(alveolar in word for alveolar in alveolars) or word == 'ear':
            phonetic.append('ear')
            return 2
        else:
            phonetic.append('ur')
            return 2
    if four[:3] == 'eye':
        phonetic.append('eye')
        return 2
    if four[:3] == 'ing' and i != 0:
        phonetic.append('ee')
        phonetic.append('ng')
        return 2
    if four[:3] == 'qui':
        five = word[i:i+5]
        phonetic.append('k')
        phonetic.append('w')
        phonetic.append('eye')
        if five[4:] == 'e':
            speech.skip = 199
        return 2
    if four[:3] == 'ure':
        phonetic.append('ure')
        return 2
    if four[:3] == 'our':
        if word != 'our':
            phonetic.append('ure')
            return 2
    if four[:3] == 'dge':
        phonetic.append('dge')
        return 2
        
    #check all two letter phonemes
    if four[:2] == 'ai' or four[:2] == 'ay':
        phonetic.append('eigh')
        return 1
    if four[:2] == 'at':
        if four[2:3] == 'e':
            phonetic.append('eigh')
            speech.skip = 199
            return 0
        phonetic.append('a3')
        return 0
    if four[:2] == 'an':
        phonetic.append('a3')
        phonetic.append('n')
        return 1
    if four[:2] == 'cc':
        if re.match(r'.*(ee|ss).*', word):
            phonetic.append('k')
            phonetic.append('s')
            return 1
        phonetic.append('k')
        return 1
    if four[:2] == 'ee':
        phonetic.append('ee')
        return 1
    if four[:2] == 'ea':
        five = word[i:i+5]
        if five == 'ealth':
            phonetic.append('e')
        elif i+1 == len(word)-1 and 'r' in word:
            phonetic.append('ee')
            phonetic.append('a1')
        elif four == 'eate' and two[1:2] != 'b':
            phonetic.append('ee')
            phonetic.append('eigh')
        else:
            phonetic.append('ee')
        return 1
    if four[:2] == 'ed' and i+1 == len(word)-1:
        phonetic.append('silent')
        phonetic.append('d')
        return 1
    if four[:2] == 'ew':
        phonetic.append('ue')
        return 1
    if four[:2] == 'ff':
        phonetic.append('f')
        return 1
    if four[:2] == 'ie':
        if four[:3].startswith(('ies', 'iev')):
            phonetic.append('ee')
            phonetic.append(str(four[2:3]))
            return 2
        phonetic.append('eye')
        return 1
    if four[:2] == 'au':
        phonetic.append('au')
        return 1
    if four[:2] == 'oa' or four[:2] == 'oe':
        phonetic.append('oe')
        return 1
    if four[:2] == 'ow':
        phonetic.append('oe')
        if four[2:3] == 'e':
            speech.skip = 200
        return 1
    #if four[:2] == 'in':
        #phonetic.append('i')
        #phonetic.append('n')
        #return 1
    if four[:2] == 'ur':
        phonetic.append('ur')
        return 1
    if four[:2] == 'er':
        if four[2:3] == 'i':
            phonetic.append('e')
            phonetic.append('r')
            phonetic.append('i')
            return 2
        phonetic.append('ur')
        return 1
    if four[:2] == 'ir':
        if i == 0:
            phonetic.append('eye')
            phonetic.append('r')
            if four[2:3] == 'e':
                speech.skip = 200
        elif four[2:3] == 'e':
            phonetic.append('eye')
            phonetic.append('r')
            speech.skip = 200
        else:
            phonetic.append('ur')
        return 1
    if four[:2] == 'oo':
        if any(ls in word for ls in oolets) or word.endswith('oo'):
            phonetic.append('ue')
            return 1
        else:
            phonetic.append('oo')
            return 1
    if four[:2] == 'or':
        phonetic.append('or')
        if four[2:3] == 'e':
            speech.skip = 200
        return 1
    if four[:2] == 'ou':
        if i == 0 or 'r' in word[:i] or any(stop in word[:i] for stop in stops):
            phonetic.append('ou1')
            return 1
        elif four[2:4] != 'gh':
            phonetic.append('ue')
            return 1
        else:
            phonetic.append('oo')
            return 1
    if four[:2] == 'oy':
        phonetic.append('oy')
        return 1
    if four[:2] == 'ue':
        phonetic.append('ue')
        return 1
    if four[:2] == 'gh':
        if two == 'ou' or two == 'ei':
            phonetic.append('silent')
            return 1
        elif two == 'au':
            phonetic.append('f')
            return 1
        else:
            phonetic.append('g')
            return 1
    if four[:2] == 'gy':
        if not word.endswith('gy'):
            phonetic.append('dge')
            return 1
    if four[:2] == 'ck':
        #phonetic.append('silent')
        phonetic.append('k')
        return 1
    if four[:2] == 'el':
        if i+1 == len(word) - 1:
            phonetic.append('l')
            return 1
    #if four[:2] == 'ld':
        #phonetic.append('d')
        #return 1
    if four[:2] == 'le':
        if i != 0 and four[2:3] == 'd':
            phonetic.append('l')
            return 1
    if four[:2] == 'll':
        phonetic.append('l')
        return 1
    if four[:2] == 'ng':
        phonetic.append('ng')
        return 1
    if four[:2] == 'ss':
        phonetic.append('s')
        return 1
    if four[:2] == 'sh':
        phonetic.append('sh')
        return 1
    if four[:2] == 'ti':
        if four == 'tion':
            phonetic.append('sh')
            phonetic.append('i')
            phonetic.append('n')
            return 3
        #if i+3 == len(word)-1:
            #phonetic.append('sh')
            #return 1
    if four[:2] == 'tt':
        phonetic.append('t')
        return 1
    if four[:2] == 'pp':
        phonetic.append('p')
        return 1
    if four[:2] == 'pt':
        if i == 0:
            phonetic.append('t')
            return 1
    if four[:2] == 'mm':
        phonetic.append('m')
        return 1
    if four[:2] == 'ch':
        phonetic.append('ch')
        return 1
    if four[:2] == 'th':
        phonetic.append('th')
        return 1
    if four[:2] == 've':
        if word.endswith('ve'):
            phonetic.append('v')
            return 1
        else:
            phonetic.append('v')
            return 0
    if four[:2] == 'ge':
        if word.endswith('ge'):
            phonetic.append('ge')
            return 1
    if four[:2] == 'wh':
        if four[2:3] == 'o':
            phonetic.append('h')
            return 1
        else:
            phonetic.append('w')
            return 1
    if four[:2] == 'zz':
        phonetic.append('t')
        phonetic.append('z')
        return 1
        
    #one letter phonemes
    if v == 'a':
        if four[2:3] == 'e' and word.endswith('er') and word.index('a') <= len(word) - 4:
            phonetic.append('a3')
            return 0
        elif four[3:4] == 'e':
            phonetic.append('a3')
            return 0
        elif four[2:3] == 'e' or four[2:3] == 'i' or word == 'a' or (four[2:3] == 'o' and i != 0):
        #elif four[2:3] == 'e' or word == 'a' or (four[2:3] == 'o' and i != 0):
            phonetic.append('eigh')
            #speech.skip = 199
            return 0
        elif i == len(word) - 1:
            phonetic.append('a1')
            return 0
        elif 'r' in word:
            phonetic.append('a2')
            return 0
        elif any(stop in word for stop in stops):
            phonetic.append('a3')
            return 0
        elif i == 0:
            if four[1:2] == 'n':
                phonetic.append('a3')
                return 0
            else:
                phonetic.append('a1')
                return 0
        else:
            phonetic.append('au')
            return 0
    if any(syllabic in v for syllabic in syllabics):
        #phonetic.append('silent')
        phonetic.append(str(v))
        return 0
    if any(constant in v for constant in constants):
        phonetic.append(str(v))
        return 0
    if v == 'c':
        before = two[1:2]
        after = four[1:2]
        #if ((any(vowel in two[1:2] for vowel in vowels) and any(vowel in four[1:2] for vowel in vowels)) and before != after) or four[1:2] == 'e': #if surrounded by vowels
        if four[1:2] == 'e' or four[1:2] == 'i' or four[1:2] == 'y':
            phonetic.append('s')
            return 0
        else:
            phonetic.append('k')
            return 0
    if v == 'e':
        cut = word[:i] + word[i+1:]
        print cut
        if word == 'the':
            phonetic.append('u')
            return 0
        elif i == len(word) - 1 and not any(vowel in cut for vowel in vowels):
            phonetic.append('ee')
            return 0
        elif four[2:3] == v and four[1:2].startswith(('s', 'z', 'r', 'd', 'c', 'j')):
            phonetic.append('ee')
            return 0
        elif i == len(word) - 1 and word != 'the':
            phonetic.append('silent')
            return 0
        else:
            phonetic.append('e')
            return 0
    if v == 'g':
        #if hardsounds append dge
        phonetic.append('g')
        return 0
    if v == 'h':
        if any(consonant in two[1:2] for consonant in consonants) or i == 0:
            phonetic.append('h')
            return 0
        else:
            phonetic.append('silent')
            return 0
    if v == 'i':
        cut = word[:i] + word[i+3:]
        #if any(vowel in four[2:3] for vowel in vowels) and i+2 != len(word) - 1:
            #phonetic.append('eye')
            #return 0
        if word == 'i':
            phonetic.append('eye')
            return 0
        elif i == 0:
            if any(vowel in four[2:3] for vowel in vowels):
                phonetic.append('eye')
            else:
                phonetic.append('i')
            return 0
        elif four[2:4] == 'ed':
            phonetic.append('eye')
            return 0
        elif any(consonant in two[1:2] for consonant in consonants) and any(consonant in four[1:2] for consonant in consonants):
            phonetic.append('i')
            return 0
        elif four[2:3] == 'i':
            phonetic.append('i')
            return 0
        elif four[:3] == 'igh':
            phonetic.append('eye')
            return 2
        elif word == 'i' or i == len(word) - 1 or (any(vowel in four[2:3] for vowel in vowels) and not four[1:2].startswith(('v', 'f'))):
            phonetic.append('eye')
            speech.skip = 199
            return 0
        elif four[2:3] == 'e' and all(place in alveolars for place in cut):
            phonetic.append('eye')
            speech.skip = 199
            return 0
        else:
            phonetic.append('i')
            return 0
    if v == 'j':
        phonetic.append('dge')
        return 0
    if v == 'm':
        if i == len(word) - 1:
            phonetic.append('m_end')
        else:
            phonetic.append('m')
        return 0
    if v == 'o':
        cut = word[:i] + word[i+1:]
        #if i == len(word) - 1 and not any(vowel in cut for vowel in vowels): #if end of word and no other vowels in word
        if not any(vowel in cut for vowel in vowels) and 'n' in word: #if no other vowels in word
            phonetic.append('oe')
            return 0
        elif not any(vowel in cut for vowel in vowels) and i == len(word)-1:
            phonetic.append('ue')
            return 0
        elif four[2:3] == 'a' or four[2:3] == 'i' or four[2:3] == 'u' or i == len(word) - 1: #if the second next letter is a
            phonetic.append('oe')
            return 0
        elif four[2:3] == 'e': #if the next letter is not v and the second next is e
            if four[1:2] != 'v':
                phonetic.append('oe')
            else:
                phonetic.append('au')
            speech.skip = 199
            return 0
        else:
            phonetic.append('au')
            return 0
    if v == 'q':
        phonetic.append('k')
        return 0
    if v == 's':
        if any(vowel in two[1:2] for vowel in vowels) and any(vowel in four[1:2] for vowel in vowels):
            if i+1 == len(word)-1:
                phonetic.append('z')
                return 0
            else:
                phonetic.append('s')
                return 0
        else:
            phonetic.append('s')
            return 0
    if v == 'u':
        if i == 0:
            if any(vowel in four[2:3] for vowel in vowels): #if the second next letter is a vowel
                if four[2:3] == 'e':
                    speech.skip = 199
                phonetic.append('ju')
                return 0
            else:
                phonetic.append('u')
                return 0
                
        elif any(stop in two[1:2] for stop in stops) and any(stop in four[1:2] for stop in stops):
            phonetic.append('ju')
            return 0
        elif four[2:3] == 'e':
            phonetic.append('ue')
            return 0
        else:
            phonetic.append('u')
            return 0
    if v == 'w':
        if four[1:2] == 'o':
            phonetic.append('silent')
            return 0
        phonetic.append('w')
        return 0
    if v == 'x':
        phonetic.append('k')
        phonetic.append('s')
        return 0
    if v == 'y':
        if i == 0 or two[1:2] == 'e':
            phonetic.append('y')
            return 0
        elif i == len(word) - 1 and any(vowel in word for vowel in vowels):
            phonetic.append('ee')
            return 0
        elif i == len(word) - 1:
            phonetic.append('eye')
            return 0
        else:
            phonetic.append('i')
            return 0
    if v == ',':
        phonetic.append('comma')
        return 0
    if v == ';':
        phonetic.append('scolon')
        return 0
    if v == ':':
        phonetic.append('colon')
        return 0
    if v == '-':
        phonetic.append('dash')
        return 0
    if four[:3] == '...':
        phonetic.append('elipsis')
        return 2
    if v == '.' or v == '?' or v == '!':
        phonetic.append('fullstop')
        return 0
        
    phonetic.append('unknown')
    return 0