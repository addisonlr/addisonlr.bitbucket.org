from search import inline
import wikipedia, re

def did(word, subject):
    return wikipedia.elaborate(subject.replace(word[len(word) - 1], ''), word[len(word) - 1], ' when did ')
    
    if inline(word, subject):
        return inline(word, subject)
    
def was(word, subject):
    if wikipedia.elaborate(subject.replace(word[len(word) - 1], ''), word[len(word) -1], ' when was '):
        return wikipedia.elaborate(subject.replace(word[len(word) - 1], ''), word[len(word) - 1], ' when was ')
    
    if inline(word, subject, 'when'):
        return inline(word, subject, 'when')