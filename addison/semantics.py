import re, requests, traceback, semantics
from requests.exceptions import RequestException
from flask import g

def diffBet(first_det, second_det, section, switch):
    print 'diffbet', first_det, second_det
    if switch:
        req = requests.get('http://www.differencebetween.net/' + section + '/difference-between-' + first_det.replace(' ', '-') + '-and-' +second_det.replace(' ', '-') + '/')
        if req.status_code == 404:
            if section == 'miscellaneous':
                return semantics.diffBet(first_det, second_det, 'technology', False)
            elif section == 'technology':
                return semantics.diffBet(first_det, second_det, 'object', False)
            elif section == 'object':
                return semantics.diffBet(first_det, second_det, 'science', False)
            elif section == 'science':
                return semantics.diffBet(first_det, second_det, 'language', False)
            elif section == 'language':
                return semantics.diffBet(first_det, second_det, 'buisness', False)
        
    else:
        req = requests.get('http://www.differencebetween.net/' + section + '/difference-between-' + second_det.replace(' ', '-') + '-and-' +first_det.replace(' ', '-') + '/')
        if req.status_code == 404:
            return semantics.diffBet(first_det, second_det, section, True)
        
    contents = req.iter_lines()
    summarizing = False
    summary = ''
    for input_line in contents:
        input_line = input_line.decode('utf-8')
        if summarizing:
            if input_line.startswith(('<p', '<li>', '1', '2', '3', '4', '5', '6', '7', '8', '9')):
                summary = summary + re.sub(r'<.*?>', '', input_line) + '\n'
            elif '<br />' in input_line:
                break
            
        if input_line.startswith('<p') and 'Summary:' in input_line:
            summarizing = True
            
    print str(req.url)
    return summary