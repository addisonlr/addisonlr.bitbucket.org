from utils import send_no_answer_email
import re, math, json
import mathematics

def calculate(equation, graphing=False):
    steps = ''
    equation = equation.strip()
    equation = equation.replace('+-', '-') #plus a negative is the same a minus
    equation = equation.replace('--', '+') #minus a negative is the same as plus
    equation = equation.replace('+', ' + ')
    equation = equation.replace('-', ' - ')
    equation = equation.replace('*', ' * ')
    equation = equation.replace('/', ' / ')
    equation = equation.replace('^', ' ^ ')
    equation = equation.replace('*  - ', '* -') #remove space after negative to prevent substraction
    equation = equation.replace('/  - ', '/ -')
    equation = equation.replace('^  - ', '^ -')
    equation = equation.replace('( - ', '(-')
    equation = equation.replace('pi', str(math.pi))
    equation = equation.replace('e', str(math.e))
    if equation.startswith(' -'):
        equation = re.sub(r'- ', '-', equation, 1)
    equation = ' ' + equation + ' '
    equation = equation.replace('  ', ' ') #remove double whitespace
    equation = re.sub(r'(\d)(x|\()', '\g<1> * \g<2>', equation) #insert multiplication where it is implied
    equation = re.sub(r'(\d)([a-zA-Z])', '\g<1> * \g<2>', equation)
    equation = re.sub(r'(x)(\d)', '\g<1>x * ', equation)
    equation = re.sub(r'(\d+)(\!)', '\g<2>' + '\g<1>', equation)
    equation = equation.encode('utf-8')
    parts = equation.split()
    print equation
    steps += equation + '\n'
    original = equation.replace('*', '&#x00D7').replace('/', '&#x00F7').replace('sqrt', '&radic;')
    
    while '(' in equation: #calculates parts inside parens first
        part = equation[equation.rindex('(', 0, equation.index(')')):equation.index(')') + 1]
        part2 = part[1:len(part) - 1]
        part2 = re.sub(r'(asin|acos|atan|sin|cos|tan)(-\d|\d)', '\g<1> \g<2>', part2)
        while ' ' in part2:
            ops = part2.lstrip().split()
            if ops[1] == '+':
                part2 = part2.replace(ops[0] + ' ' + ops[1] + ' ' + ops[2], str(float(ops[0]) + float(ops[2])))
            elif ops[1] == '-':
                part2 = part2.replace(ops[0] + ' ' + ops[1] + ' ' + ops[2], str(float(ops[0]) - float(ops[2])))
            elif ops[1] == '*':
                part2 = part2.replace(ops[0] + ' ' + ops[1] + ' ' + ops[2], str(float(ops[0]) * float(ops[2])))
            elif ops[1] == '/':
                try:
                    part2 = part2.replace(ops[0] + ' ' + ops[1] + ' ' + ops[2], str(float(ops[0]) / float(ops[2])))
                except ZeroDivisionError:
                    return 'http://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Hyperbola_one_over_x.svg/220px-Hyperbola_one_over_x.svg.png\nIn mathematics, division by zero is division where the divisor is zero. Such a division can be formally expressed as a/0 where a is the dividend . Whether this expression can be assigned a well-defined value depends upon the mathematical setting. In ordinary arithmetic, the expression has no meaning, as there is no number which, multiplied by 0, gives a , and so division by zero is undefined. Since any number multiplied by zero is zero, the expression 0/0 has no defined value and is called an indeterminate form. Historically, one of the earliest recorded references to the mathematical impossibility of assigning a value to a/0 is contained in George Berkeley\'s criticism of infinitesimal calculus in The Analyst.\nSource: http://en.wikipedia.org/wiki/Division_by_zero'
            elif ops[1] == '^':
                part2 = part2.replace(ops[0] + ' ' + ops[1] + ' ' + ops[2], str(float(ops[0]) ** float(ops[2])))
            elif ops[0] == 'asin':
                part2 = part2.replace(ops[0] + ' ' + ops[1], str(math.asin(float(ops[1]))))
            elif ops[0] == 'acos':
                part2 = part2.replace(ops[0] + ' ' + ops[1], str(math.acos(float(ops[1]))))
            elif ops[0] == 'atan':
                part2 = part2.replace(ops[0] + ' ' + ops[1], str(math.atan(float(ops[1]))))
            elif ops[0] == 'sin':
                part2 = part2.replace(ops[0] + ' ' + ops[1], str(math.sin(float(ops[1]))))
            elif ops[0] == 'cos':
                part2 = part2.replace(ops[0] + ' ' + ops[1], str(math.cos(float(ops[1]))))
            elif ops[0] == 'tan':
                part2 = part2.replace(ops[0] + ' ' + ops[1], str(math.tan(float(ops[1]))))
        equation = equation.replace(part, part2)
        parts = equation.strip().split()
        print equation
        steps += equation + '\n'
    
    while any(a in equation for a in ('si', 'cos', 'ta', 'as', 'ac', 'at', 'sec', 'csc', 'cot', 'sqrt', 'abs', '!', 'log', 'ln')):
        try:
            if 'as' in equation:
                part = equation[equation.rindex('asin'):equation.index(' ', equation.rindex('asin'))]
                equation = equation.replace(part, str(math.asin(float(part.replace('asin', '')))))
            elif 'ac' in equation:
                part = equation[equation.rindex('acos'):equation.index(' ', equation.rindex('acos'))]
                equation = equation.replace(part, str(math.acos(float(part.replace('acos', '')))))
            elif 'at' in equation:
                part = equation[equation.rindex('atan'):equation.index(' ', equation.rindex('atan'))]
                equation = equation.replace(part, str(math.atan(float(part.replace('atan', '')))))
            elif 'si' in equation:
                part = equation[equation.rindex('sin'):equation.index(' ', equation.rindex('sin'))]
                equation = equation.replace(part, str(math.sin(float(part.replace('sin', '')))))
            elif 'cos' in equation:
                part = equation[equation.rindex('cos'):equation.index(' ', equation.rindex('cos'))]
                equation = equation.replace(part, str(math.cos(float(part.replace('cos', '')))))
            elif 'ta' in equation:
                part = equation[equation.rindex('tan'):equation.index(' ', equation.rindex('tan'))]
                equation = equation.replace(part, str(math.tan(float(part.replace('tan', '')))))
            elif 'csc' in equation:
                part = equation[equation.rindex('csc'):equation.index(' ', equation.rindex('csc'))]
                equation = equation.replace(part, str(1/math.sin(float(part.replace('csc', '')))))
            elif 'sec' in equation:
                part = equation[equation.rindex('sec'):equation.index(' ', equation.rindex('sec'))]
                equation = equation.replace(part, str(1/math.cos(float(part.replace('sec', '')))))
            elif 'cot' in equation:
                part = equation[equation.rindex('cot'):equation.index(' ', equation.rindex('cot'))]
                equation = equation.replace(part, str(1/math.tan(float(part.replace('cot', '')))))
            elif 'sqrt' in equation:
                part = equation[equation.rindex('sqrt'):equation.index(' ', equation.rindex('sqrt'))]
                try: #catches domain errors
                    equation = equation.replace(part, str(math.sqrt(float(part.replace('sqrt', '')))))
                except ValueError:
                    return 'http://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/ImaginaryUnit5.svg/220px-ImaginaryUnit5.svg.png\nSorry, complex numbers are not currently supported.\n\nThe imaginary unit or unit imaginary number, denoted as i., is a mathematical concept which extends the real number system R. to the complex number system C., which in turn provides at least one root for every polynomial P. The imaginary unit\'s core property is that i2 = -1. The term "imaginary" is used because there is no real number having a negative square.\nSource: http://en.wikipedia.org/wiki/Imaginary_unit'
            elif 'abs' in equation:
                part = equation[equation.rindex('abs'):equation.index(' ', equation.rindex('abs'))]
                equation = equation.replace(part, str(math.fabs(float(part.replace('abs', '')))))
            elif '!' in equation:
                part = equation[equation.rindex('!'):equation.index(' ', equation.rindex('!'))]
                equation = equation.replace(part, str(math.factorial(float(part.replace('!', '')))))
            elif 'ln' in equation:
                try:
                    part = equation[equation.rindex('ln'):equation.index(' ', equation.rindex('ln'))]
                    equation = equation.replace(part, str(math.log(float(part.replace('ln', '')))))
                except:
                    return '<span class="larger">' + original + ' = undefined' + '</span><hr>' + steps.replace('*', '&#x00D7').replace('/', '&#x00F7').replace('sqrt', '&radic;')
            elif 'log' in equation:
                try:
                    part = equation[equation.rindex('log'):equation.index(' ', equation.rindex('log'))]
                    equation = equation.replace(part, str(math.log(float(part.replace('log', '')), 10)))
                except:
                    return '<span class="larger">' + original + ' = undefined' + '</span><hr>' + steps.replace('*', '&#x00D7').replace('/', '&#x00F7').replace('sqrt', '&radic;')
        except ZeroDivisionError:
            return '<span class="larger">Division By Zero</span> - Calculation Error<hr><img id="image" src="http://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Hyperbola_one_over_x.svg/220px-Hyperbola_one_over_x.svg.png"></img>In mathematics, division by zero is division where the divisor is zero. Such a division can be formally expressed as a/0 where a is the dividend . Whether this expression can be assigned a well-defined value depends upon the mathematical setting. In ordinary arithmetic, the expression has no meaning, as there is no number which, multiplied by 0, gives a , and so division by zero is undefined. Since any number multiplied by zero is zero, the expression 0/0 has no defined value and is called an indeterminate form. Historically, one of the earliest recorded references to the mathematical impossibility of assigning a value to a/0 is contained in George Berkeley\'s criticism of infinitesimal calculus in The Analyst.\nSource: http://en.wikipedia.org/wiki/Division_by_zero'
        parts = equation.strip().split()
        print equation
        steps += equation + '\n'
    
    while len(parts) != 1:
        for i, c in enumerate(equation):
            if '^' in equation:
                if c == '^':
                    part = equation[equation.rindex(' ', 0, equation.index(c) - 1) + 1:equation.index(' ', equation.index(c) + 2)]
                    operators = part.lstrip().split()
                    try:
                        equation = equation.replace(part, str(float(operators[0]) ** float(operators[2])))
                    except ValueError:
                        equation = equation.replace(part, str(complex(operators[0]) ** float(operators[2])))
                    parts = equation.strip().split()
                    print equation
                    steps += equation + '\n'
            elif any(a in equation for a in ('*', '/')):
                if c == '*':
                    part = equation[equation.rindex(' ', 0, equation.index(c) - 1) + 1:equation.index(' ', equation.index(c) + 2)]
                    operators = part.lstrip().split()
                    equation = equation.replace(part, str(float(operators[0]) * float(operators[2])))
                    parts = equation.strip().split()
                    print equation
                    steps += equation + '\n'
                elif c == '/':
                    part = equation[equation.rindex(' ', 0, equation.index(c) - 1) + 1:equation.index(' ', equation.index(c) + 2)]
                    operators = part.lstrip().split()
                    try:
                        equation = equation.replace(part, str(float(operators[0]) / float(operators[2])))
                    except:
                        return 'http://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Hyperbola_one_over_x.svg/220px-Hyperbola_one_over_x.svg.png\nIn mathematics, division by zero is division where the divisor is zero. Such a division can be formally expressed as a/0 where a is the dividend . Whether this expression can be assigned a well-defined value depends upon the mathematical setting. In ordinary arithmetic, the expression has no meaning, as there is no number which, multiplied by 0, gives a , and so division by zero is undefined. Since any number multiplied by zero is zero, the expression 0/0 has no defined value and is called an indeterminate form. Historically, one of the earliest recorded references to the mathematical impossibility of assigning a value to a/0 is contained in George Berkeley\'s criticism of infinitesimal calculus in The Analyst.\nSource: http://en.wikipedia.org/wiki/Division_by_zero'
                    parts = equation.strip().split()
                    print equation
                    steps += equation + '\n'
                    
            else:
                if c == '+':
                    part = equation[equation.rindex(' ', 0, equation.index(c) - 1) + 1:equation.index(' ', equation.index(c) + 2)]
                    operators = part.lstrip().split()
                    equation = equation.replace(part, str(float(operators[0]) + float(operators[2])))
                    parts = equation.strip().split()
                    print equation
                    steps += equation + '\n'
                    break #needed to allow addition of >= 4 of the same number
                elif c == '-' and i != 1 and '- ' in equation:#and not equation.lstrip().startswith('-'):
                    part = equation[equation.rindex(' ', 0, equation.index(c + ' ') - 1) + 1:equation.index(' ', equation.index(c + ' ') + 2)]
                    operators = part.lstrip().split()
                    equation = equation.replace(part, str(float(operators[0]) - float(operators[2])))
                    parts = equation.strip().split()
                    print equation
                    steps += equation + '\n'
                    
    equation = '%.5f' % float(equation)
    print equation
    while equation.endswith(('0', '.')) and '.' in equation:
        equation = equation[:len(equation) - 1]
    if equation == '-0':
        if graphing:
            return equation
        return '<span class="larger">' + original + ' = ' + equation + '</span><hr>' + steps.replace('*', '&#x00D7').replace('/', '&#x00F7'.replace('sqrt', '&radic;'))
    else:
        if graphing:
            return equation
        return '<span class="larger">' + original + ' = ' + equation + '</span><hr>' + steps.replace('*', '&#x00D7').replace('/', '&#x00F7').replace('sqrt', '&radic;')
        
def graph(equation):
    equation = re.sub(r'(\d)(x)', '\g<1>*x', equation)
    equation = re.sub(r'(\d)([a-zA-Z])', '\g<1> * \g<2>', equation)
    equation = re.sub(r'(x)(\d)', '\g<1>x * ', equation)
    replaced = ''
    graph = []
    for i in range(-200, 200):
        replaced = equation.replace('x', str(float(i) / float(10)))
        try:
            graph.append(str(float(mathematics.calculate(replaced, True))))
        except:
            graph.append('undefined')
        
    return 'graphrect' + json.dumps(graph)

def three_d_graph(equation):
    equation = re.sub(r'(\d)(x)', '\g<1>*x', equation)
    equation = re.sub(r'(\d)([a-zA-Z])', '\g<1> * \g<2>', equation)
    equation = re.sub(r'(x)(\d)', '\g<1>x * ', equation)
    replaced = ''
    graph = []
    for i in range(-100, 100):
        for i in range(-100, 100):
            replaced = equation.replace('x', str(float(i) / float(10)))
            try:
                graph.append(str(float(mathematics.calculate(replaced, True))))
            except:
                graph.append('undefined')
        
    return 'graph3d' + json.dumps(graph)
    
def polar_graph(equation):
    equation = re.sub(r'(\d)(x)', '\g<1> * x', equation)
    equation = re.sub(r'(\d)([a-zA-Z])', '\g<1> * \g<2>', equation)
    equation = re.sub(r'(x)(\d)', '\g<1>x * ', equation)
    replaced = ''
    graph = []
    for i in range(0, 1460):
        replaced = equation.replace('x', str(math.radians(i)))
        try:
            radius = float(mathematics.calculate(replaced, True))
            graph.append(str(20 * radius * math.cos(math.radians(i))))
            graph.append(str(-20 * radius * math.sin(math.radians(i))))
        except:
            graph.append('undefined')
            graph.append('undefined')
            
    return 'graphpol' + json.dumps(graph)
    
def fractal(equation):
    equation = equation.replace('z=', '')
    color_list = []
    power = re.sub(r'.*(\^)(\d+).*', '\g<2>', equation)
    for y in range(200): #iterate through rows
        for x in range(350): #iterate from left to right
            c = complex((x - 250) / 100.0, (y - 100) / 100.0)
            z = complex(0, 0)
            iteration = 0
            while z.real**2 + z.imag**2 < 4 and iteration < 510: #while z remains bounded and has not iterated more than 1000 times
                #z = mathematics.calculate(equation.replace('z', str(z)))
                z = z**int(power) + c
                iteration += 1
                
            color = int(float(255 - (iteration/510.0 * 255.0)))
            color_list.append(color)
            print color
    return 'fractal' + json.dumps(color_list)
    
def pattern(numbers):
    original = numbers
    numbers.reverse() #reverses numbers to ease future calculations
    print numbers
    
    power = 1 #used to return calculations to the proper value
    while all(int(math.sqrt(math.fabs(numbers[i])) + 0.5) ** 2 == numbers[i] for i in range(len(numbers))): #while are numbers are perfect squares
        if not all(int(math.sqrt(i)) == i for i in numbers): #
            power *= 2
            for i in range(len(numbers)):
                numbers[i] = int(math.sqrt(numbers[i]))
    print numbers, power
    
    recalculate = False #only true for oscillating sequences
    differences = [None] * len(numbers) #list containing all differences
    differences[0] = numbers
    for x in range(1, len(numbers)):
        differences[x] = [differences[x - 1][y] - differences[x - 1][y + 1] for y in range(len(differences[x - 1]) - 1)]
    print 'Differences:', differences
    
    quotients = [None] * len(numbers) #list containing all quotients
    quotients[0] = numbers
    for x in range(1, len(numbers)):
        temp = []
        for y in range(len(quotients[x - 1]) - 1):
            if quotients[x - 1][y + 1] == 0:
                pass
            else:
                temp.append(quotients[x - 1][y] / float(quotients[x - 1][y + 1]))
        quotients[x] = temp
    print 'Quotients:', quotients
    
    if differences[0][2:] == differences[1][:-1]:
        print 'recursive'
        answer = differences[0]
        answer.reverse()
        for i in range(len(numbers) - 1, 2499):
            if -2147483648 <= answer[len(answer) - 1] + answer[len(answer) - 2] ** power <= 2147483648:
                answer.append(answer[i] + answer[i - 1])
        for i in range(len(answer)):
            answer[i] **= power        
        return 'This is a recursive sequence. The following is ' + str(len(answer)) + ' elements:\n' + str(answer)
    elif all(x == differences[1][0] for x in differences[1]):
        print 's1'
        answer = differences[0]
        answer.reverse()
        for i in range(len(numbers) - 1, 2499):
            if -2147483648 <= (answer[i] + differences[1][0]) ** power <= 2147483648:
                answer.append(answer[i] + differences[1][0])
            else:
                break
        for i in range(len(answer)):
            answer[i] = answer[i] ** power
        return 'The following is ' + str(len(answer)) + ' elements:\n' + str(answer)
    elif all(differences[1][x] == differences[1][0] for x in range(0, len(differences), 2)) and all(differences[1][x] == differences[1][1] for x in range(1, len(differences) - 1, 2)):
        print 'oscillating'
        answer = differences[0]
        answer.reverse()
        for i in range(len(numbers) - 1, 2499):
            if -2147483648 <= answer[len(answer) - 1] ** power <= 2147483648:
                if i % 2 == 0:
                    answer.append(answer[len(answer) - 1] + differences[1][0])
                else:
                    answer.append(answer[len(answer) - 1] + differences[1][1])
        for i in range(len(answer)):
            answer[i] **= power
        return 'This is an oscillating sequence. To see what an oscillating function looks like please search, "10sin(x)" for a graphical representation. The following is ' + str(len(answer)) + ' elements:\n' + str(answer)
    elif all(x == differences[2][0] for x in differences[2]):
        print 's2'
        answer = differences[0]
        answer.reverse()
        for i in range(len(numbers) - 1, 2499):
            if -2147483648 <= (answer[i] + differences[1][0] + differences[2][0]) ** power <= 2147483648:
                answer.append(answer[i] + differences[1][0] + differences[2][0])
            else:
                break
        for i in range(len(answer)):
            answer[i] = answer[i] ** power
        return 'The following is ' + str(len(answer)) + ' elements:\n' + str(answer)
    elif all(x == quotients[1][0] for x in quotients[1]):
        print 'd1'
        answer = quotients[0]
        answer.reverse()
        for i in range(len(numbers) - 1, 2499):
            if -2147483648 <= (answer[i] * quotients[1][0]) ** power <= 2147483648:
                answer.append(answer[i] * quotients[1][0])
            else:
                break
        for i in range(len(answer)):
            answer[i] = answer[i] ** power
        return 'The following is ' + str(len(answer)) + ' elements:\n' + str(answer)
    elif all(x == quotients[2][0] for x in quotients[2]) and quotients[2] is int:
        print 'd2'
        answer = quotients[0]
        answer.reverse()
        for i in range(len(numbers) - 1, 2499):
            if -2147483648 <= (answer[i] * (quotients[1][0] + quotients[2][0])) ** power <= 2147483648:
                answer.append(answer[i] * (quotients[1][0] + quotients[2][0]))
            else:
                break
        for i in range(len(answer)):
            answer[i] = answer[i] ** power
        send_no_answer_email(str(original))
        return 'These specific results are experimental, and as such may not be correct. The following is ' + str(len(answer)) + ' elements:\n' + str(answer)
    elif all(x == differences[3][0] for x in differences[3]):
        print 's3'
        answer = differences[0]
        answer.reverse()
        for i in range(len(numbers) - 1, 2499):
            if -2147483648 <= (answer[i] + differences[1][0] + differences[2][0] + differences[3][0]) ** power <= 2147483648:
                answer.append(answer[i] + differences[1][0] + differences[2][0] + differences[3][0])
            else:
                break
        for i in range(len(answer)):
            answer[i] = answer[i] ** power
        send_no_answer_email(str(original))
        return 'These specific results are experimental, and as such may not be correct. The following is ' + str(len(answer)) + ' elements:\n' + str(answer)
    
    send_no_answer_email(str(original))
    return 'The Pattern recognition algorithm is still under development.  It currently works with mathematical sequences such as, "0,1,4,9", or, "4,10,16,22".'